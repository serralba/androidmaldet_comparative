# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 11:16:31 2020

@author: Borja Molina Coronado
@email: borja.molina@ehu.eus
"""


import numpy as np
import collections
import sys

class FeatureEncoder:
    dictionary = None
    seq = 0
        
    
    def __init__(self, dictionary=collections.OrderedDict()):
        self.dictionary = dictionary
        self.seq = 0
    
    
# =============================================================================
# Read dictionary from file and load its entries
# If file read fails, auto_update is enabled
# =============================================================================
    def loadFromFile (self, fileName, separator=','):
        try:
            with open(fileName) as dFile:
                dict_lines = dFile.readlines()
            for line in dict_lines:
                line=line.replace("\n","") #Avoid \n at the end of the line
                lns = line.split(separator)
                self.dictionary[lns[0]] = lns[1]
            self.auto_update = False
            return 1
        except:
            print('Error reading dictionary object')
            self.auto_update = True
            return -1


# =============================================================================
# Adds a encoded value to element
# =============================================================================
    def addElement (self, element):
        """
        Encode an element

        Parameters
        ----------
        element : hasheable type

        Returns
        -------
        value : int
            Id of the encoded element

        """
        try:
            value = self.dictionary[element]
        except KeyError:
            value = self.seq
            self.seq += 1
            self.dictionary[element] = value
        except TypeError:
            print('TypeError: unhashable type'+str(type(element))+" with value "+str(element), file=sys.stderr)
            raise
        return value
    

# =============================================================================
# Encode elements in list
# =============================================================================
    def addList (self, _list):
        """
        Encode elements of a list

        Parameters
        ----------
        _list : [], ndarray
            List of elements

        Returns
        -------
        _out : ndarray
            List with ids of the elements of list encoded

        """
        _out = np.zeros(len(_list), dtype=np.int)
        n = 0
        for element in _list:
            _out[n] = self.addElement(element)
            n += 1
        return _out
        
        
# =============================================================================
# Get the dictionary entry for an element. Never auto_updates the dictionary
# Used only for internal methods where we need a invariable dictionary
# see OneHotEncoding or FrequencyEncoding methods
# =============================================================================
    def __getUnalterableTranslate (self, element):
        try:
            value = self.dictionary[element]
        except:
            value = None
        return value


# =============================================================================
# Set the auto_update attribute
# =============================================================================
    def setAutoUpdatable (self, auto_update):
        self.auto_update = auto_update


# =============================================================================
# Get the auto_update attribute
# =============================================================================
    def isAutoUpdatable (self):
        return self.auto_update


# =============================================================================
# Get the learnt dictionary
# =============================================================================
    def getDictionary (self):
        return self.dictionary
    

# =============================================================================
# Get the length of the dictionary
# =============================================================================
    def getFeatureNames (self):
        return list(self.dictionary.keys())


# =============================================================================
# Get the length of the dictionary
# =============================================================================
    def getLength (self):
        return len(list(self.dictionary.keys()))
    
    
# =============================================================================
# Filters items of the dictionary for feature selection
# =============================================================================
    def filterFeatures ( self, indexes ):
        """
        Removes features from dictionary. Usefull when FSS is used

        Parameters
        ----------
        indexes : List
            List of elements to keep in the dictionary. Elements in vector
            are the indexes of the keys of the dictionary

        Returns
        -------
        None.

        """
        keys = list(self.dictionary.keys())
        new_dict = collections.OrderedDict()
        new_seq = 0
        for idx in indexes:
            new_dict[keys[idx]] = new_seq
            new_seq += 1
        self.dictionary = new_dict
        self.seq = new_seq
        

# =============================================================================
# Get the translation of itmes in vector. It does not adds items that are not
# in the dictionary, i.e. does an unalterable translate
# =============================================================================
    def translate (self, vector):
        V = []
        for v in vector:
            try:
                idx = self.dictionary[v]
                V.append(idx)
            except:
                continue
        return np.array(V, dtype=np.int32)



# =============================================================================
# Get one-hot encoding of a vector according to the dictionary items
# =============================================================================
    def getOneHotEncode (self, vector):
        V = np.zeros(len(self.dictionary), dtype=np.int8)
        max_n = len(vector)
        for n in range(max_n):
            try:
                idx = self.dictionary[vector[n]]
            except:
                continue
            V[idx] = 1
        return V


# =============================================================================
# Get frequency encoding of a sequence according to the dictionary items
# If values is provided, vector contains unique items and values are their 
# correspondent frequency
# =============================================================================
    def getFrequencyEncoding (self, vector, values=None):
        V = np.zeros(len(list(self.dictionary.keys())))
        i = 0
        max_n = len(vector)
        for n in range(max_n):
            idx = self.__getUnalterableTranslate(vector[n])
            if idx:  # The element does not exist in the dict - Skip it
                if values:  # Set to the number of values
                    V[idx] = values[i] 
                else:  # It is a sequence with repetitions
                    V[idx] += 1
            i += 1
        return V

    