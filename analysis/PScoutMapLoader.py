#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 24 12:45:47 2020

@author: borja
"""

import os


class PScoutMapLoader:
    
    apis_perms = {}
    _use_descriptors = False
    fname = None
    
    def __init__ (self, fname, use_descriptors = True):
        """
        Constructor of the class.

        Parameters
        ----------
        fname : String
            path to the PScout file containing the permission to api mappings.
        use_descriptors : boolean
            Use method descriptors

        Returns
        -------
        None.

        """
        self.fname = fname
        self._use_descriptors = use_descriptors
        self.__loadAPIs()
        
    
    # https://github.com/JesusFreke/smali/wiki/TypesMethodsAndFields
    def __translateDType (self, dtype ):
        array = ''
        for n in range(0,dtype.count('[]')):
            array += '['
        dtype = dtype.replace('[]','')
        if dtype == 'void':
            return 'V'
        if dtype == 'boolean':
            return array+'Z'
        if dtype == 'byte':
            return array+'B'
        if dtype == 'short':
            return array+'S'
        if dtype == 'char':
            return array+'C'
        if dtype == 'int':
            return array+'I'
        if dtype == 'long':
            return array+'J'
        if dtype == 'float':
            return array+'F'
        if dtype == 'double':
            return array+'D'
        return array+'L'+dtype.replace('.','/')+';'
    
    
    def __methodToSmali (self, line ):
        call = line.split(' ')  # Format is  "<Class_name: return_type method_name(arg1,arg2)> ()"
        class_name = 'L'+call[0][1:-1].replace('.', '/')+';'
        # Return type
        return_type = self.__translateDType(call[1])
        # Parse method stuff
        method = call[2][:-2]  # Remove ')>'
        method = method.split('(')
        method_name = method[0] 
        if not self._use_descriptors:
            return "{}->{}".format(class_name, method_name)
        # Descriptors
        args = ""
        if len(method[1])>1:
            descriptors = method[1].split(',')
            for arg in descriptors:
                args += self.__translateDType(arg)
        # Build all
        return "{}->{}({}){}".format(class_name, method_name, args, return_type)
    
    
    # Loads file contents into dict, parsing classes and methods to smali
    def __loadAPIs (self):
        try:
            with open(self.fname, "r") as f:
                  content = f.readlines()
        except:
            print('Error reading', self.fname, 'working dir:', os.getcwd())
            content = None
            
        if not content: 
            return
        # Parse content
        for line in content:
            if line.startswith("Permission:"):
                perm = line.split(':')[1][:-1]  # Remove \n
            if line.startswith('<'):
                method = self.__methodToSmali(line)
                try:
                    self.apis_perms[method].append(perm)
                except:
                    self.apis_perms[method] = [perm]
                
                
    def checkAPI (self, API):
        """
        Check and returns the permissions needed by an API call

        Parameters
        ----------
        API : String
            String with the API call in smali format.
            If omit_descriptors is False:
                Lpackage/name/ObjectName;->MethodName(III)Z
            Else:
                Lpackage/name/ObjectName;->MethodName

        Returns
        -------
        []
            List containing the set of permissions needed by that API call.

        """
        try:
            return self.apis_perms[API]
        except:
            return []