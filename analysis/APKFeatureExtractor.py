#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 18 10:32:05 2019

@author: Borja Molina Coronado
@email: borja.molina@ehu.eus
"""

import sys
import zipfile
import os
import glob
import traceback
import subprocess
import numpy as np
from zipfile import ZipFile
import networkx as nx
import itertools
import multiprocessing as mp 
import analysis.PScoutMapLoader as PScoutLoader
import re
import lief
import shutil
import tempfile
import string
from androguard.misc import AnalyzeAPK, get_default_session



# This class analyzes an APK file and returns the opcode sequences of all its functions
class APKFeatureExtractor:
    
    APK = None
    pkg = None
    __EXTRACT_PATH = "./DEXs/"
    __thisPATH = './'

    
    # Analysis objects
    __platformCode = None
    __dex_sequences = []
    __dex_analysis = None
    __VMAnalysis = None
    __APKinfo = None
    
    
    # Collected information
    __entry_points = None  # Classes detected as entry points in the manifest
    __found_entry_p = None  # Elements found in the code that pertain to entry point classes
    __found_isolated = None  # Isolated node elements in the function call graph
    __api_usages = None # Dictionary of API classes in use in the apk. Each API class contains a dictionary with the methods and its usage count
    

    
    # Clases and methods
    class_h = None  # Class hierarchy dictionary
    
    # Library directories
    __platformJarDir = None
    __external_lib_jars = None
    __api2perm_mapping = None
    
    # Library lists
    __APIclasses = None  # Loaded API classes
    __external_libs = None  # List with class names of external libraries
    
    
    # Flag
    __with_descriptors = False  # To generate the graph with method descriptors and avoid repeating the generation process
    
    # Analysis flags
    __ignore_sdk_version = None  #

    
    # Graph attributes
    _G = None  # Methods call Graph
    _CG = None  # Classes call graph
    _CFG = None  # The CFG of the APK
    _API_CFG = None  # The API CFG of the APK
    
    
    #CFG auxiliar attributes
    _method_CFGs = {}
    _CFGs_edges = {}  # edges between CFGs used to build de APK CFG
    
    _PScoutMapper = None
    
    _android_callbacks = {'Activity': ['onActivityResult(IILandroid/content/Intent;)V','onCreate(Landroid/os/Bundle;)V','onCreateDialog(I)Landroid/app/Dialog;','onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;','onChildTitleChanged(Landroid/app/Activity;','onDestroy()V','onNewIntent(Landroid/content/Intent;)V','onPause()V','onPostCreate(Landroid/os/Bundle;)V','onPostResume()V','onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V','onPrepareDialog(ILandroid/app/Dialog;)V','onRestart()V','onRestoreInstanceState(Landroid/os/Bundle;)V','onResume()V','onSaveInstanceState(Landroid/os/Bundle;)V','onStart()V','onStop()V','onTitleChanged(','onUserLeaveHint()V'],
                          'Service': ['onBind(Landroid/content/Intent;)Landroid/os/IBinder;','onConfigurationChanged(Landroid/content/res/Configuration;)V','onCreate()V','onDestroy()V','onLowMemory()V','onRebind(Landroid/content/Intent;)V','onStart(Landroid/content/Intent;I)V','onStartCommand(Landroid/content/Intent;II)I','onTaskRemoved(Landroid/content/Intent;)V','onTrimMemory(I)V','onUnbind(Landroid/content/Intent;)B'],
                          'BroadcastReceiver': ['onReceive(Landroid/content/Context;Landroid/content/Intent;)V']}
    
    
    
    def __init__ (self, platform_jar_dir="./jars", external_jar_dir="./ad-libraries", api2perm_mapping="./mappings/PScoutMappings_DAPASA-Drebin.txt", use_method_descriptors=True, ignore_skd_version=True):
        """
        Creates an object of the class

        An APKSequenceExtractor object is initialized according to user params.

        
        :param platform_jar_dir: directory where jar files are. The directory must be organized in subdirectories containing the actual android.jar files and named by as sdk version that contain (jar_root/18/android.jar)
        :param external_jar_dir: directory containing the jar files of external libraries
        :param api2perm_mapping: file containing the PScout mappings between Permissions and API calls
        :param use_method_descriptors: difference between methods equally named but with different arguments
        :param ignore_skd_version: if True, loads the API classes of all versions, if False only libraries of the declared sdk version are loaded

        :rtype: APKSequenceExtractor
        """
        sys.setrecursionlimit(1000000)
        self.__APKinfo = None
        self.__VMAnalysis = None
        self.__dex_analysis = None
        self.__external = None
        self.__internal = None
        self.__external_libs = []
        self.__APIclasses = []
        
        self.__with_descriptors = use_method_descriptors
        self.__ignore_sdk_version = ignore_skd_version
        thispath = "/".join(__file__.split('/')[:-2])+'/'
        self.__thisPATH = thispath 
        if platform_jar_dir.endswith('/'):
            self.__platformJarDir = thispath + platform_jar_dir
        else:
            self.__platformJarDir = thispath + platform_jar_dir+'/'
        if external_jar_dir.endswith('/'):
            self.__external_lib_jars = thispath + external_jar_dir
        else:
            self.__external_lib_jars = thispath + external_jar_dir+'/'
        
        self.__api2perm_mapping = thispath + api2perm_mapping
        if self.__api2perm_mapping:
            self._PScoutMapper = PScoutLoader.PScoutMapLoader(self.__api2perm_mapping, use_descriptors=use_method_descriptors)
        
        if self.__ignore_sdk_version:
            self.__readPlatformJar()
        self.__readExternalJars()


##########################
# Internal methods
##########################

# File (APK, jar) management and processing methods
    
    # List the content of a zip file. Used to read classes from jar files
    def __listZipContent (self, file):
        zipF = ZipFile(file)
        listaZip = zipF.namelist()
        zipF.close()
        return listaZip
    
    
    # Read the android.jar correspondent to the sdk version of the app
    # If the platform code is not detected, all jar files are read
    # Parallelize this part
    def __readPlatformJar (self):
        if not self.__platformJarDir:
            return
        if self.__ignore_sdk_version or not self.__platformCode or int(self.__platformCode)<10: # Read all jars
            listaZip = []
            lista_extend = listaZip.extend
            for n in range(8,30):
                lista_extend(self.__listZipContent(self.__platformJarDir+'android-'+str(n)+"/android.jar"))
            listaZip = list(set(listaZip))  # Get unique APIs
        else:
            listaZip = self.__listZipContent(self.__platformJarDir+'android-'+self.__platformCode+"/android.jar")
        # Read support libraries
        files = glob.glob(self.__platformJarDir+"/android-support/"+"*.jar")
        lista_extend = listaZip.extend
        for f in files:
            lista_extend(self.__listZipContent(f))
        # Format classes
        self.__APIclasses = ["L"+file[:-6]+";" for file in listaZip if file.endswith(".class") and not self.__is_obfuscated(file)]
        #print("Number of API classes loaded:", len(self.__APIclasses))
        #print(self.__APIclasses)
    
    
    def __is_obfuscated (self, class_name):
        split_class = class_name.split('.')[:-1]
        obfuscated = 0
        for split in split_class:
            if len(split)<3:
                obfuscated += 1
        if obfuscated >= len(split_class)/2: return True
        return False
    
    
    def __run_java_component(self, jar, args, timeout=None):
        """Wrapper for calling Java processes used for extraction and injection."""
        cmd = ['java', '-jar', jar, *args]

        try:
            print('Executing java command', cmd)
            out = subprocess.run(' '.join(cmd), timeout=timeout, check=True, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            #out = subprocess.check_output(cmd, stderr=sys.stdout, timeout=timeout)
            return str(out.stderr, 'utf-8')
        except subprocess.TimeoutExpired:
            print('Execution timed out')
        except subprocess.CalledProcessError as e:
            exception = "\nexit code :{0} \nSTDOUT :{1} \nSTDERROR : {2} ".format(
                e.returncode,
                e.output.decode(sys.getfilesystemencoding()),
                e.stderr.decode(sys.getfilesystemencoding()))
            print(exception)
        return ''
    
    
    def __get_strings_from_bytes (self, bytestr):
        printable = set(string.printable)
        strs = []
        palabra = ''
        for b in bytestr:
            if chr(b) in printable: palabra += chr(b)
            elif len(palabra)>0: 
                strs.append(palabra)
                palabra = ''
        return strs
    
    
    
    # Read the jar files of ad libraries 
    def __readExternalJars (self):
        if not self.__external_lib_jars:
            return
        listaZip = []
        files = glob.glob(self.__external_lib_jars+"*.jar")
        lista_extend = listaZip.extend
        for f in files:
            lista_extend(self.__listZipContent(f))
        listaZip = list(set(listaZip))  # Get unique APIs
        self.__external_libs = ["L"+file[:-6]+";" for file in listaZip if file.endswith(".class") and not self.__is_obfuscated(file) ]
        #print("Number of external classes loaded:", len(self.__external_libs))
        #print(self.__APIclasses)
    
    # Analyze Dex detected files 
    def __analyzeDexFiles (self, dex_files):
        # Create session and add analysis of all dex files
        session = get_default_session()
        dx = None
        # Read all dex files
        for dexF in dex_files:
            with open(dexF, "rb") as fd:
                data = fd.read()
            digest, self.__VMAnalysis, dx = session.addDEX(dexF, data, dx=dx)
        if self.__dex_analysis is None:
           self.__dex_analysis = dx

    
    # Search for dex files in the extracted directory
    def __analyzeDexDirectory (self):
        dex_files = glob.glob(self.EXTRACT_PATH+"**/*.dex", recursive=True)
        return dex_files
            
    
    # Unzip the dex files of an APK
    def __unzip_APK (self, apk_name):
        zip_ref = zipfile.ZipFile(apk_name)
        for f in zip_ref.namelist():
            if f.endswith('.dex'):
                zip_ref.extract(f, path=self.EXTRACT_PATH)
        zip_ref.close()
    
    
    # Remove dex files of the previous APK
    def __cleanDexDirectory (self, dex_files):
        for dexF in dex_files:
            os.remove(dexF)




# Feature extraction internal methods

    # Build intruction sequences of methods reaching an API method at most 
    # depth distance (number of calls)
    # TODO:
    #  Only for static methods? (get_access_flags()==ACC_STATIC==0x8)
    def __extractDepthNeighbors (self, depth):
        self.__api_usages = {}
        sequences = []
        sequences_append = sequences.append
        __callers_in_list = []
        __callers_in_list_append = __callers_in_list.append
        metodos = []  # List that contains the attributes of methods to analyze
        metodos_append = metodos.append
        for apiClass in self.__APIclasses:
            try:
                #print(apiClass)
                for meth in self.__dex_analysis.classes[apiClass].get_methods():
                    for _, call, _ in meth.get_xref_from():
                        # Check if the caller is not another API method or an ad library
                        if call.class_name not in self.__APIclasses and call.class_name not in self.__external_libs:
                            # Count the number of callers to the API
                            self._add_api_usage(call)
                            
                            # Add the methods to be analyzed
                            metodos_append((call.class_name, call.name, call.get_descriptor()))
            except KeyError:
                continue
        print("Found:", len(self.__api_usages), "APIs")
        # Aanalyze API uses with a depth of n
        k = 1
        while k<=depth:
            aux_metodos = []
            aux_metodos_append = aux_metodos.append
            for (clase, metodo, descriptor) in metodos:
                # Do not re-analyze methods (avoid repeated)
                if clase+metodo+descriptor in __callers_in_list:
                    continue
                # Add as analyzed
                __callers_in_list_append(clase+metodo+descriptor)
                meth = self.__dex_analysis.get_method_by_name(clase, metodo, descriptor )
                # Get analysis of the method
                meth_ana = self.__dex_analysis.get_method_analysis(meth)
                # Analyze the method
                if meth_ana and not meth_ana.is_external():
                    # Get instructions
                    inst = meth.get_instructions()
                    if inst:
                        method_sequences = []
                        method_sequences_append = method_sequences.append
                        for ins in inst:
                            method_sequences_append(ins.get_op_value())
                        # Add sequences of the method to seqs, avoid duplicates
                        if method_sequences not in sequences:
                            sequences_append(method_sequences)
                    # Get method callers
                    for _, call, _ in meth_ana.get_xref_from():
                        # Check if the caller is not another API method or an ad library
                        if call.class_name not in self.__APIclasses and call.class_name not in self.__external_libs:
                            aux_metodos_append((call.class_name, call.name, call.get_descriptor()))
            # Copy aux to list in order to analyze new discovered methods
            metodos = aux_metodos
            k += 1
        print("Found", len(sequences), "callers of", len(list(self.__dex_analysis.get_methods())), "total methods")
        return sequences, __callers_in_list  # Returns the list of sequences and the list of analyzed methods
    
        

    # Gets opcode sequences of all internal methods in the APK
    def __extractAllMethodSequences (self):
        # Analyze methods of the selected dex file
        sequences = []
        for method in self.__dex_analysis.get_methods():
            if method.is_external():
                continue
            # Need to get the EncodedMethod from the MethodClassAnalysis object
            m = method.get_method()
            method_sequence = []
            # Get the list of instruction codes of the method
            if m.get_instructions():
                for ins in m.get_instructions():
                    method_sequence.append(ins.get_op_value())
                # Add new sequences (those that have not been added yet)
                if method_sequence not in sequences:
                    #print(method_sequence)
                    sequences.append(method_sequence)
        return sequences


    # Gets the signature of the BasicBlocks that conform a method
    # It checks the instructions of the block to create the signature
    def __get_bb_signature(self, analysis_method) :
        """
        Given a method analysis object, it analyzes its code
        to create the signature of the method, based on the vocabulary of
        https://dl.acm.org/doi/10.5555/1862294.1862301
        
        This method is based on the impelemtation code of Androguard 1.5.
        It has been ported due to the lack of offsets for Strings and Fields
        xrefs on new Androguard versions

        Parameters
        ----------
        analysis_method : MethodAnalysis
            Method for which the signature is calculated

        Returns
        -------
        bbs : String
            Signature of the method

        """
        """
        #########################################################################
        ##### Meaning of the signature ########
        https://dl.acm.org/doi/10.5555/1862294.1862301
        
        Procedure ::= StatementList
        StatementList ::= Statement | Statement StatementList
        Statement ::= BasicBlock | Return | Goto | If | Field | Package | String
        Return ::= 'R'
        Goto ::= 'G'
        If ::= 'I'
        BasicBlock ::= 'B'
        Field ::= 'F'0 | 'F'1
        Package ::= 'P' PackageNew | 'P' PackageCall
        PackageNew ::= '0'
        PackageCall ::= '1'
        PackageName ::= Epsilon | Id
        String ::= 'S' Number | 'S' Id
        Number ::= \d+
        Id ::= [a-zA-Z]\w+
        #########################################################################
        """
        bbs = []
        for b in analysis_method.basic_blocks.get() :
            l = []
            l.append( (b.start, "B") )
            l.append( (b.start, "[") )
            
            
            internal = []

            # get_last() bug in Androguard 3.3.5 (solved in this way)
            op_value = list(b.get_instructions())[-1].get_op_value()

            # return
            if op_value >= 0x0e and op_value <= 0x11 :
                internal.append( (b.end-1, "R") )

            # if
            elif op_value >= 0x32 and op_value <= 0x3d :
                internal.append( (b.end-1, "I") )

            # goto
            elif op_value >= 0x28 and op_value <= 0x2a :
                internal.append( (b.end-1, "G") )

            # sparse or packed switch
            elif op_value >= 0x2b and op_value <= 0x2c :
                internal.append( (b.end-1, "G") )

            ####
            ###
            ###
            offset = b.start
                
            for i in b.get_instructions(): # iterate over instructions
                op_value = i.get_op_value()
                # field access
                if (op_value >= 0x52 and op_value <= 0x6d):
                    if (0x52 <= op_value <= 0x58) or (0x60 <= op_value <= 0x66):
                        # read access to a field
                        internal.append( (offset, "F0" ) )
                    else:
                        # write access to a field
                        internal.append( (offset, "F1" ) )

                # invoke
                elif (op_value >= 0x6e and op_value <= 0x72) or (op_value >= 0x74 and op_value <= 0x78):
                    internal.append( (offset, "P1" ) )
    
                # new_instance
                elif op_value == 0x22:
                    internal.append( (offset, "P0" ))
    
                # const-string
                elif (op_value >= 0x1a and op_value <= 0x1b):
                    internal.append( (offset, "S" ) )
                    
                
                offset += i.get_length()
            #####
            #####

            internal.sort()
            
            for i in internal :
                if i[0] >= b.start and i[0] < b.end :
                    l.append( i )

            del internal

            l.append( (b.end, "]") )

            bbs.append( ''.join(i[1] for i in l) )
            
        return bbs


    def __L0_method_signature( self, method, join=True):
        """
            Return the default L0_0 signature of the method
            
            method : androguard.core.analysis.MethodClassAnalysis
                The method to which extract BB
            join : boolean
                If True, Basic Blocks are joined in a string, otherwise, a list
                of strings is returned, each representing a BB
        """
        enc_method = method.get_method()
        bbs = self.__get_bb_signature ( self.__dex_analysis.get_method(enc_method) )
        if join:
            return ''.join(z for z in bbs)
        return bbs
    
    
    
    
    def __collectOpcodeStats (self):
        """
        Counts the instructions in the code

        Returns
        -------
        None.

        """
        opcode_stats = {}
        for method in self.__dex_analysis.get_methods():
            if method.is_external():
                continue  # Skip methods
            
            m = method.get_method()
            # Get the list of instruction codes of the method
            if m.get_instructions():
                for ins in m.get_instructions():
                    # Update dictionary
                    try:
                        opcode_stats[ins.get_op_value()] += 1
                    except:
                        opcode_stats[ins.get_op_value()] = 1
        return opcode_stats
    
    
    def __extractOpcodesRecursively (self, target_method, depth):
        if depth<0 or target_method.is_external():
            return []
        else:
            own_seq = []
            own_seq_append = own_seq.append
            m = target_method.get_method()
            inst = m.get_instructions()
            if inst:
                for ins in inst:
                    operands = ins.get_operands()
                    op_value = ins.get_op_value()
                    own_seq_append(op_value)
                    
                    # Invoke
                    if (op_value >= 0x6e and op_value <= 0x72) or (op_value >= 0x74 and op_value <= 0x78):
                        new_target = None
                        call_to = operands[-1][-1].split('->')
                        new_target_class_name = call_to[0]
                        new_target_method_raw = call_to[1].split('(')
                        new_target_method_name = new_target_method_raw[0]
                        new_target_method_desc = '('+new_target_method_raw[1]
                        #print(new_target_class_name, new_target_method_name, new_target_method_desc)
                        new_target = self.__dex_analysis.get_method_analysis_by_name(new_target_class_name, new_target_method_name, new_target_method_desc)
                        
                        # If not method found, search on super classes
                        if not new_target:
                            new_target_class = self.__dex_analysis.get_class_analysis(new_target_class_name)
                            super_class_name = new_target_class.extends
                            interfaces = new_target_class.implements
                            
                            super_class = self.__dex_analysis.get_class_analysis(super_class_name)
                            for m in super_class.get_methods():
                                if m.name == new_target_method_name and m.descriptor == new_target_method_desc:
                                    new_target = m
                                    new_target_class = super_class
                                    break
                            
                            if not new_target:
                                for i in interfaces:
                                    interface = self.__dex_analysis.get_class_analysis(i)
                                    if interface is None: continue
                                    for m in interface.get_methods():
                                        if m.name == 'onCreate':
                                            new_target = m
                                            new_target_class = interface
                                            break
                                        
                        # Get opcodes of new target method
                        if new_target:
                            #print('Analyzing', new_target_class_name, new_target_method_name, new_target_method_desc, "iterations", depth)
                            opcode_sequence = self.__extractOpcodesRecursively(new_target, depth-1)
                            own_seq.extend(opcode_sequence)
            
                        
            return own_seq
    


    def __parseDescriptor (self, desc):
        """
        Transforms something of this form:
            '(IILjava/lang/String)V'
        To androguard format:
            '(I I Ljava/lang/String)V'
                
        Parameters
        ----------
        desc : String
            DESCRIPTION of the method.

        Returns
        -------
        parsed : String
            DESCRIPTION of the method.

        """
        n = 0
        parsed = ''
        posClosePar = desc.index(')')
        while n<len(desc):
            if n>1 and n<posClosePar:
                parsed += ' '
            if desc[n]=='L':
                endObjArg = desc[n:].index(';')+1
                parsed += desc[n:n+endObjArg]
                n += endObjArg
            elif desc[n]=='[':  # array of something
                if desc[n+1]=='L':
                    endObjArg = desc[n:].index(';')+1
                    parsed += desc[n:n+endObjArg]
                    n += endObjArg
                else:
                    parsed += '['+desc[n+1]
                    n += 2
            else:  # Rest of argument types and ")" and return
                parsed += desc[n]
                n += 1
        return parsed


    def __APIwalkMethodRecursive (self, method_analysis, APIlist, exclude_list, visited):
        """
        Recursive function that follows invokes to get API calls.
        
        APIlist and exclude_list are filter parameters
        
        """
        if not method_analysis:
            return 
        
        m = method_analysis.get_method()
        try:
            # Dummy load to check if the node has already been visited
            _ = visited[m]
            return []
        except KeyError:
            if m.class_name in self.__APIclasses:
                m_name = m.class_name+'->'+m.name+m.get_descriptor().replace(' ', '')
                if m_name not in APIlist and exclude_list or m_name in APIlist and not exclude_list:
                    return [m_name]
                return []
            
            visited[m] = 1
            
            apis = []
            callsto = list(method_analysis.get_xref_to())
            callsto.sort(key=lambda tup: tup[2])  # sorts in place by offset
            #print(callsto)
            for other_class, callee, offset in callsto:
                apis.extend(self.__APIwalkMethodRecursive(self.__dex_analysis.get_method_analysis(callee), visited))
            return apis
    
    
    def __APIwalkMethodIterative (self, method_analysis, APIlist, exclude_list):
        """
        Iterative version to walk the code. It follows invokes to get API calls.
        
        """
        seq = []
        found = []
        visited = {}
        current_method = method_analysis
        
        while True:
            if current_method:
                m = current_method.get_method()
                
                if m.class_name in self.__APIclasses:
                    m_name = m.class_name+'->'+m.name+m.get_descriptor().replace(' ', '')
                    if m_name not in APIlist and exclude_list or m_name in APIlist and not exclude_list:
                        seq.append(m_name)
                else:  # Add nodes to visit if not are already visited
                    try:
                        # Dummy assignation to cause an exception if not visited
                        _ = visited[current_method]
                    except KeyError:
                        callsto = list(current_method.get_xref_to())
                        callsto.sort(key=lambda tup: tup[2], reverse=True)  # sorts in place by offset
                        # Reverse sort because last in the list will be first
                        found.extend(callsto)
                        visited[current_method] = 1
            #End when no methods to analyze
            if len(found)<=0:
                break
            else:
                # Move to first call of the method
                current_method = self.__dex_analysis.get_method_analysis(found.pop()[1])
        return seq

    

    def __walkAPKforAPIs (self, only_isolated, APIlist, exclude_list):
        """
        Format is Lpackage_name/class_name;->method_name(arg1;arg2)return_type;

        Parameters
        ----------
        only_isolated : boolean
            If True, only isolated nodes of the graph are used as entry points.
            If False, methods of declared entry points and isolated nodes are 
            used.
        APIlist : List [] or String
            List of API classes that are either included or omited from the 
            analysis. Format is smali:
                Lpackage_name/class_name;
        exclude_list : boolean
            If True, the apis in APIlist are excluded, if False, only the apis
            in the list are included.

        Returns
        -------
        None.

        """
        sequences = []
        if only_isolated:
            init_points = self.__found_isolated
        else:
            init_points = list(set(self.__found_entry_p + self.__found_isolated))
        
        for init_method in init_points:
            #print(init_method)
            i_meth = init_method.split('->')
            # 'Ljava/lang/Object;'
            clase = i_meth[0]
            metodo = i_meth[1].split('(')
            n_method = metodo[0]
            # '(I I Ljava/lang/String)V'
            descriptor = self.__parseDescriptor('('+metodo[1])
            # Get the method
            #print(clase, n_method, descriptor)
            m = self.__dex_analysis.get_method_analysis_by_name(clase, n_method, descriptor)
            #sequences.append( self.__APIwalkMethodRecursive(m, APIlist, exclude_list, {}) )
            sequences.append( self.__APIwalkMethodIterative(m, APIlist, exclude_list) )
        return sequences
            
    
    
    
    def __buildApiCFGRecursive (self, CFG, G, node_name, previous_api, APIlist, exclude_list, visited):
        """
        **This method crash Python on large graphs!!!!
        
        Recursive function that follows the CFG of the APK to get the API control flow graph.
        
        APIlist and exclude_list are filter parameters
        
        visited : dict
            A dictionary with the edges visited
 
        
        """
        #print("Walking through: ", previous_api, node_name)
        # Avoid checking the same path
        try:
            # Dummy load to check if the node has already been visited
            _ = visited[(previous_api, node_name)]
            return  # Path already analyzed
        except KeyError:
            visited[(previous_api, node_name)] = 1
        
        
        class_name = node_name.split('->')[0]
        
        # Is API?
        if class_name in self.__APIclasses:
            # If not excluded, add it
            if node_name not in APIlist and exclude_list or node_name in APIlist and not exclude_list:
                if not self.__with_descriptors:
                    api_name = node_name.split('(')[0]
                else:
                    api_name = node_name
                # Add node
                if api_name  not in G:
                    G.add_node( api_name )
                # Add link
                if previous_api and not G.has_edge(previous_api, api_name):
                    G.add_edge(previous_api, api_name )
                previous_api = api_name
        

        # Follow successors
        for neighbor in CFG.neighbors(node_name):
            #print("Neigh: ", neighbor, "OF ", node_name)
            self.__buildApiCFGRecursive(CFG, G, neighbor, previous_api, APIlist, exclude_list, visited )
                
        #print("OUT: ", node_name)
        return
    
    
    def __buildApiCFGIterative (self, CFG, G, node_name, previous_api, APIlist, exclude_list ):
        """
        Iterative function that follows the CFG of the APK to get the API control flow graph.
        
        APIlist and exclude_list are filter parameters
        
        """
        visited = {}  # Dict containing the edges already visited
        to_visit = []  # List with edges to visit
        to_visit_extend = to_visit.extend
        to_visit_pop = to_visit.pop
                
        while True:
        
            try:
                # Dummy load to check if the edge has already been checked
                _ = visited[(previous_api,node_name)]
            except KeyError:
                # Not visited
                visited[(previous_api,node_name)] = 1
                
                class_name = node_name.split('->')[0]
                
                if class_name in self.__APIclasses or node_name=='ROOT':
                    # If not excluded, add it
                    if node_name not in APIlist and exclude_list or node_name in APIlist and not exclude_list:
                        if not self.__with_descriptors:
                            api_name = node_name.split('(')[0]
                        else:
                            api_name = node_name
                        # Add node
                        if api_name not in G:
                            G.add_node( api_name )
                        # Add link
                        if previous_api and not G.has_edge(previous_api, api_name):
                            G.add_edge(previous_api, api_name )
                        previous_api = api_name

                # Get successors
                vecinos = [(previous_api, n) for n in CFG.neighbors(node_name)]
                if vecinos:
                    to_visit_extend(vecinos)
                
            if not to_visit:
                break
            # Choose the next node
            previous_api, node_name = to_visit_pop()
            
    
    def _buildApiCFGIterative_par (self, cfgs, tid, use_descriptors, API_classes, API_list, is_exclude_list, initial_fnodes, output_q, lock ):
        """
        Parallel version of __buildApiCFGIterative
        
        cfgs : dict
            List with tuples method_name,CFG of the methods 
        with_descriptors: bool
            True for using API descriptors
        APIclasses : List []
            List of API classes
        APIlist : List []
            List containint valid (exclude_list=False) APIs or invalid (exclude_list=True)
        exclude_list : boolean
            True if APIlist is a exclusion list or False otherwise
        output_q : mp.Queue
            Output queue, where graphs are returned
        omit_descriptors : boolean
            If True API method descriptors are omitted and nodes are grouped by 
            method name, i.e., method overloads are grouped. Default is True
        
        """
        
        #lock.acquire()
        #print(tid, "in with ", len(cfgs))
        #lock.release()
        
        for method_name, mcfg in cfgs:
            #lock.acquire()
            #print(tid, method_name )
            #lock.release()
            # Get initial node
            if len(mcfg.nodes)==0:
                continue
                        
            # Initialize output CFG 
            G = nx.DiGraph()
            G.add_node('ROOT')
            visited = {}  # Dict containing the edges already visited
            to_visit = []
            
            in_weights = sorted(nx.in_degree_centrality(mcfg).items(), key=lambda item: item[1])
            initial = list(dict(filter(lambda item: item[1]==0, in_weights)).keys())
            for init in initial:
                to_visit.append((None, init))  # List with edges to visit
            
            # Add the first node if not already added
            first = list(mcfg.nodes)[0]
            if first not in initial: to_visit.append((None, first))
            
            # Get API CFG from that graph
            while len(to_visit)>0:
                # Choose the next node
                previous_api, node_name = to_visit.pop()
                
                if previous_api==None and node_name==None:
                    continue
                
                try:
                    # Dummy load to check if the edge has already been checked
                    _ = visited[(previous_api,node_name)]
                except KeyError:
                    # Not visited
                    visited[(previous_api,node_name)] = 1
                    
                    class_name = node_name.split('->')[0]
                    
                    if class_name in API_classes:
                        # If not excluded, add it
                        if node_name not in API_list and is_exclude_list or node_name in API_list and not is_exclude_list:
                            # If descriptors are omitted
                            if not use_descriptors:
                                api_name = node_name.split('(')[0]
                            else:
                                api_name = node_name
                            # Add node
                            if api_name  not in G:
                                G.add_node( api_name )
                            # Add link
                            if previous_api and not G.has_edge(previous_api, api_name):
                                G.add_edge(previous_api, api_name )
                            elif not previous_api and method_name in initial_fnodes:  # It is an entry point
                                G.add_edge('ROOT', api_name)
                            previous_api = api_name
    
                    # Get successors
                    vecinos = [(previous_api, neigh) for neigh in mcfg.neighbors(node_name)]
                    if vecinos:
                        to_visit.extend(vecinos)
                        to_visit = list(set(to_visit))
                
            # Put graph in the list
            # Put list of graphs as output
            lock.acquire()
            output_q.append(G)
            lock.release()
        
        #lock.acquire()
        #print(tid, "finished")
        #lock.release()
    

    
    def __build_method_CFG (self, encoded_method, is_api = False):
        """ 
        Builds the API Control Flow Graph of a method by analyzing its basic blocks
        
        Parameters
        ----------
        encoded_method : androguard.core.bytecodes.dvm.EncodedMethod
            Method for which the Basic Blocks are analyzed to build the CFG
        
        is_api : boolean
            Default: False. If True obtain API CFG of method
        
        Returns
        -------
        networkx.DiGraph
            The Control Flow Graph of the method, with basic blocks being nodes 
            and edges representing the connections between them
        
        """
    
        def __add_node (G, node_name):
            if node_name not in G:
                G.add_node(node_name)
        
        G = nx.DiGraph()
        
        key = encoded_method.class_name+'->'+encoded_method.name+encoded_method.get_descriptor().replace(' ', '')
        
        
        mx = self.__dex_analysis.get_method(encoded_method)
        
        #print("Getting CFG for", key, "with", len(mx.basic_blocks.gets()) )

        for DVMBasicMethodBlock in mx.basic_blocks.gets():
            
            ins_idx = DVMBasicMethodBlock.start
            
            if ins_idx==0:
                node_id = key
            else:
                node_id = key + '_$'+str(ins_idx)+'$'
            
            #print("Analyzing", node_id)
            # Create node
            
            __add_node(G, node_id)
    
            for DVMBasicMethodBlockInstruction in DVMBasicMethodBlock.get_instructions():
                
                op_value = DVMBasicMethodBlockInstruction.get_op_value()
                    
                # invokes or new_instances [0x1c,0x22]
                if (op_value >= 0x6e and op_value <= 0x72) or (op_value >= 0x74 and op_value <= 0x78) or op_value in [0x1c, 0x22]:
                    # Get method name
                    idx_meth = DVMBasicMethodBlockInstruction.get_ref_kind()
                    method_info = DVMBasicMethodBlockInstruction.cm.vm.get_cm_method(idx_meth)
                    if method_info:
                        class_name = method_info[0]
                        method_name = method_info[1]
                        descriptor = ''.join(method_info[2])
        
                        node_to = class_name+'->'+method_name+descriptor.replace(' ', '')
                        
                        ## Add node
                        __add_node(G, node_to)
                        
                        if not G.has_edge(node_id, node_to ):
                            G.add_edge(node_id, node_to )
                            #print("Edge: ", node_id, node_to)
                            node_id = node_to
                                                
    
                
# =============================================================================
#                 elif op_value in [0x1c, 0x22]:
# 
#                     # Get method name
#                     idx_type = DVMBasicMethodBlockInstruction.get_ref_kind()
#                     # type_info is the string like 'Ljava/lang/Object;'
#                     class_name = DVMBasicMethodBlockInstruction.cm.vm.get_cm_type(idx_type)
#                     # TODO analyze how to treat this cases. <init> methods have descriptors?
#                     node_to = class_name+'-><init>()'
#                     
#                     ## Add node
#                     __add_node(G, node_to)
#                         
#                     if not G.has_edge(node_id, node_to ):
#                         G.add_edge(node_id, node_to )
# =============================================================================
                    
                ins_idx += DVMBasicMethodBlockInstruction.get_length()
                
            # child basic blocks
            for DVMBasicMethodBlockChild in DVMBasicMethodBlock.childs:
                child_idx = DVMBasicMethodBlockChild[2].start 
                
                if child_idx==0:
                    node_to = key
                else:
                    node_to = key + '_$'+str(child_idx)+'$'
                    #print("Child", node_to)

                    
                ## Add node
                __add_node(G, node_to)
                    
                if not G.has_edge(node_id, node_to ):
                    G.add_edge(node_id, node_to )
                    #print("Edge: ", node_id, node_to)

        if is_api: return self.__filterAPIsFromGraph(G, key)
        
        return G
    
    
    def __filterAPIsFromGraph (self, G, key=None):
        # Filter graph to keep only API nodes
        nodes_in_G = list(G.nodes)
        for node in nodes_in_G:
            try:
                class_node = node.split('->')[0]
                if class_node not in self.__APIclasses and node!=key:
                    #print('Removing', node)
                    # Join predecessors with successors
                    pred = G.predecessors(node)
                    suc = G.successors(node)
                    for p in pred:
                        for s in suc:
                            G.add_edge(p,s)
                    # Remove the node
                    G.remove_node(node)
            except:
                pass
        return G

    
    def __getMethodsCFG (self, exclude_external_lib = False, is_api = False ):
        """
        Gets the Control Flow Graph of every method in the APK
        
        Parameters
        ----------
        exclude_external_lib : boolean
            Exclude methods from classes of external libs (ads)
        
        is_api : boolean
            Default: False. If True obtain API CFG of methods
        
        """
        self.__found_entry_p = []
        __found_entry_p_append = self.__found_entry_p.append
        self.__api_usages = {}
        
        # Get entry point classes
        entry_point_classes = []
        for n in list(self.__entry_points.values()):
            entry_point_classes += n 
        
        
        for meth in self.__dex_analysis.get_methods():
            if meth.is_external():
                continue
            enc_method = meth.get_method()
            key = enc_method.class_name+'->'+enc_method.name+enc_method.get_descriptor().replace(' ', '')
            # Analyze methods from external libs??
            if exclude_external_lib and enc_method.class_name in self.__external_libs:
                #print('Excluding -> ', key)
                continue
            # Is an entry point?
            if enc_method.class_name in entry_point_classes:
                __found_entry_p_append(key)
            # Is an API method?
            if enc_method.class_name in self.__APIclasses:
                self._add_api_usage(enc_method)
                continue  # Do not analyze calls between APIs
            
            self._method_CFGs[key] = self.__build_method_CFG(enc_method, is_api)
    
    
    def __searchEntryPoints (self):
        """
        Searchs for entry points in the manifiest of the APK, looking for declared Activities, Services, BroadcastReceivers and ContentProviders
        
        """
        #search_for = ['<activity>', '<intent-filter>', '<receiver>', '<provider>', '<service>']
        #attr = 'android:name'
        self.__entry_points = {}
        self.__entry_points['Activity'] = [("L"+clase.replace('.','/')+";").replace("//","/") for clase in self.__APKinfo.get_activities()]
        self.__entry_points['Service'] = [("L"+clase.replace('.','/')+";").replace("//","/") for clase in self.__APKinfo.get_services()]
        self.__entry_points['BroadcastReceiver'] = [("L"+clase.replace('.','/')+";").replace("//","/") for clase in self.__APKinfo.get_receivers()]
        self.__entry_points['Provider'] = [("L"+clase.replace('.','/')+";").replace("//","/") for clase in self.__APKinfo.get_providers()]

    
    def _add_api_usage (self, enc_method):
        """
        Wrapper to update API methods in use in the APK
        
        enc_method : androguard.core.bytecodes.dvm.EncodedMethod
        
        """
        try:
            if self.__with_descriptors:
                method_name = enc_method.name+enc_method.get_descriptor().replace(' ', '')
            else:
                method_name = enc_method.name
            apiDict = self.__api_usages[enc_method.class_name]
            try:
                apiDict[method_name] += 1 
            except KeyError:
                apiDict[method_name] = 1 
            self.__api_usages[enc_method.class_name] = apiDict
        except KeyError:
            self.__api_usages[enc_method.class_name] = {method_name : 1}
    
    
    # Modified version of androguard.core.analysis.get_call_graph
    def __build_call_graph(self, no_external=False, no_api_constructors=False, include_fields=False):
        """
        Generate a directed graph based on the methods found in the APK.
        
        A networkx.DiGraph is returned, containing all edges only once!
        that means, if a method calls some method twice or more often, there will
        only be a single connection.
        
        Additionally, two class objects are filled. __found_entry_p and __api_usages. The first contains the names
        that are found for the classes declared in the manifest as entry_points.
        The latter contains the API classes called by code of the APK.

        :param no_external: remove nodes corresponding to external libs
        :param no_api_constructors: remove <init> methods from the analysis graph
        :param include_fields: includes reads and writes of fields from methods. Every field is treaten as a method (node)
        

        """
        
        # 
        def _add_node(G, node_name, is_entry_point, is_sink, is_api, isolated):
            """
            Wrapper to add methods to a graph
            """

            # Add to the graph
            if node_name not in G:                
                G.add_node(node_name, api=is_api, entrypoint=is_entry_point, isolated=isolated)
            

        
        #CG = nx.MultiDiGraph()
        CG = nx.DiGraph()
        CG_has_edge = CG.has_edge
        CG_add_edge = CG.add_edge

        # Initialize variables
        self.__found_isolated = []
        __found_isolated_append = self.__found_isolated.append
        self.__api_usages = {}
        self.__found_entry_p = []
        __found_entry_p_append = self.__found_entry_p.append
        
        # Note: If you create the CG from many classes at the same time, the drawing
        # will be a total mess...
        for m in self.__dex_analysis.get_methods():
            orig_method = m.get_method()
            #print("Found Method --> {}".format(orig_method))
            
            isolated=False
            is_api = False
            is_entry_point = False
            is_sink = False
            
            # Do not add external libs (ads) code
            if no_external and orig_method.class_name in self.__external_libs:
                #print("Skipped {}, because if is a external lib method".format(orig_method))
                continue
            
            # is API?
            if orig_method.class_name in self.__APIclasses:
                is_api = True
                self._add_api_usage(orig_method)
                
            
            # Set the name of the node
            if self.__with_descriptors:
                orig_name = orig_method.class_name+'->'+orig_method.name+orig_method.get_descriptor().replace(' ', '')
            else:
                orig_name = orig_method.class_name+'->'+orig_method.name
            
            # is an entry point method?
            if orig_method.class_name in self.__entry_points['Activity']:
                for callback in self._android_callbacks['Activity']:
                    if orig_method.name in callback:
                        is_entry_point = True
                        if orig_name not in self.__found_entry_p:
                            __found_entry_p_append(orig_name)
                        break
            elif orig_method.class_name in self.__entry_points['Service']:
                for callback in self._android_callbacks['Service']:
                    if orig_method.name in callback:
                        is_entry_point = True
                        if orig_name not in self.__found_entry_p:
                            __found_entry_p_append(orig_name)
                        break
            elif orig_method.class_name in self.__entry_points['BroadcastReceiver']:
                for callback in self._android_callbacks['BroadcastReceiver']:
                    if orig_method.name in callback:
                        is_entry_point = True
                        if orig_name not in self.__found_entry_p:
                            __found_entry_p_append(orig_name)
                        break
            elif orig_method.class_name in self.__entry_points['Provider']:
                is_entry_point = True
                if orig_name not in self.__found_entry_p:
                    __found_entry_p_append(orig_name)
            

            # Is a sink? Has output edges?
            if len(m.get_xref_to()) == 0:
                is_sink = True
                
            # Has input edges?
            if len(m.get_xref_from()) == 0:
                isolated=True
                if orig_name not in self.__found_isolated:
                    __found_isolated_append(orig_name)
                
            
                    
            _add_node(CG, orig_name, is_entry_point, is_sink, is_api, isolated)
            
            
            # Get calls of that method
            for other_class, callee, offset in m.get_xref_to():
                               
                if no_external and callee.class_name in self.__external_libs:
                    #print("Skipped {}, because if is a external lib method".format(orig_method))
                    continue
                
                is_api = False
                is_sink=False
                is_entry_point = False
                
                # Has output edges?
                if len(other_class.get_method_analysis(callee).get_xref_to())==0:
                    is_sink=True
                    
                
                if self.__with_descriptors:
                    callee_name = callee.class_name+'->'+callee.name+callee.get_descriptor().replace(' ', '')
                else:
                    callee_name = callee.class_name+'->'+callee.name
                    
                #Is an API call?
                
                if callee.class_name in self.__APIclasses:
                    # Do not add constructors of APIs
                    if callee.name=="<init>" and no_api_constructors:
                        #print("Skipped ", callee.class_name, callee.name, ''.join(callee.descriptor))
                        continue
                    
                    is_api = True
                    # Update list of APIs in use
                    self._add_api_usage(callee)


                # Is from an entry point class?
                if callee.class_name in self.__entry_points['Activity']:
                    for callback in self._android_callbacks['Activity']:
                        if callee.name in callback:
                            is_entry_point = True
                            if callee_name not in self.__found_entry_p:
                                __found_entry_p_append(callee_name)
                            break
                elif callee.class_name in self.__entry_points['Service']:
                    for callback in self._android_callbacks['Service']:
                        if callee.name in callback:
                            is_entry_point = True
                            if callee_name not in self.__found_entry_p:
                                __found_entry_p_append(callee_name)
                            break
                elif callee.class_name in self.__entry_points['BroadcastReceiver']:
                    for callback in self._android_callbacks['BroadcastReceiver']:
                        if callee.name in callback:
                            is_entry_point = True
                            if callee_name not in self.__found_entry_p:
                                __found_entry_p_append(callee_name)
                            break
                elif callee.class_name in self.__entry_points['Provider']:
                    is_entry_point = True
                    if callee_name not in self.__found_entry_p:
                        __found_entry_p_append(callee_name)
                    
                
                _add_node(CG, callee_name, is_entry_point, is_sink, is_api, isolated = False)
                

                # As this is a DiGraph and we are not interested in duplicate edges,
                # check if the edge is already in the edge set.
                # If you need all calls, you probably want to check out MultiDiGraph
                if not CG_has_edge(orig_name, callee_name ):
                    CG_add_edge(orig_name, callee_name)
                
        if include_fields:
            
            for field in self.__dex_analysis.get_fields():
                f = field.get_field()
                
                if self.__with_descriptors:
                    orig_name = f.class_name+'->'+f.name+f.get_descriptor().replace(' ', '')
                else:
                    orig_name = f.class_name+'->'+f.name
                    
                # Add field to the graph
                _add_node(CG, orig_name, False, False, False, False)
                
                # Search for references to read the field
                for ref_class, ref_method  in field.get_xref_read():
                    if self.__with_descriptors:
                        callee_name = ref_method.class_name+'->'+ref_method.name+ref_method.get_descriptor().replace(' ', '')
                    else:
                        callee_name = ref_method.class_name+'->'+ref_method.name
                    
                    if not CG_has_edge(orig_name, callee_name ):
                        CG_add_edge(orig_name, callee_name)
                
                # Search for references to write the field
                for ref_class, ref_method  in field.get_xref_write():
                    if self.__with_descriptors:
                        callee_name = ref_method.class_name+'->'+ref_method.name+ref_method.get_descriptor().replace(' ', '')
                    else:
                        callee_name = ref_method.class_name+'->'+ref_method.name
                    
                    if not CG_has_edge(callee_name, orig_name ):
                        CG_add_edge(callee_name, orig_name)
                        
        
        # References to classes from methods of other classes (Explicit intents execute onCreate always)
        for clase in self.__dex_analysis.get_classes():
            for other_class in clase.get_xref_to():
                for ref_kind, ref_method, ref_offset in clase.xrefto[other_class]:
                    # Hardcoded for explicit intents
                    if self.__with_descriptors:
                        from_ref = ref_method.class_name+'->'+ref_method.name+ref_method.get_descriptor().replace(' ', '')
                        to = other_class.name + "->onCreate(Landroid/os/Bundle;)V"
                    else:
                        from_ref = ref_method.class_name+'->'+ref_method.name
                        to = other_class.name + "->onCreate"
                
                if not CG_has_edge(from_ref, to ):
                    CG_add_edge(from_ref, to)
            
                    
        self._G = CG

        
    
    # Modified version of androguard.core.analysis.get_call_graph
    def __build_classes_graph ( self, no_external=False ):
        """
        Generate a directed graph based on the class found in the apk.

        A networkx.DiGraph is returned, containing all edges only once!
        that means, if a method calls some method twice or more often, there will
        only be a single connection.

        :param no_external: remove nodes corresponding to external libs

        """
        
        # 
        def _add_node(G, clase, is_sink, is_api, isolated):
            """
            Wrapper to add methods to a graph
            """
            if clase.name not in G:
                if clase.is_external():
                    is_external = True
                else:
                    is_external = False
                
                    
                if clase.name in entry_point_classes:
                    is_entry_point = True
                    # Sinks cannot be entry_points
                    if clase.name not in self.__found_entry_p and not is_sink:
                        __found_entry_p_append(clase.name)
                else:
                    is_entry_point = False
                
                G.add_node(clase.name, api=is_api, external=is_external, entrypoint=is_entry_point, isolated=isolated)
            

        #CG = nx.MultiDiGraph()
        CG = nx.DiGraph()
        CG_has_edge = CG.has_edge
        CG_add_edge = CG.add_edge

        # Get entry point classes
        entry_point_classes = []
        for n in list(self.__entry_points.values()):
            entry_point_classes += n 
            
        # Initialize API usages list
        self.__api_usages = {}
        self.__found_entry_p = []
        __found_entry_p_append = self.__found_entry_p.append
        self.__found_isolated = []
        __found_isolated_append = self.__found_isolated.append
        
        # Note: If you create the CG from many classes at the same time, the drawing
        # will be a total mess...
        for clase in self.__dex_analysis.get_classes():
            class_name = clase.name
            #print("Found Method --> {}".format(orig_method))
            
            # Do not add external libs (ads) code
            if no_external and class_name in self.__external_libs:
                #print("Skipped {}, because if is a external lib".format(class_name))
                continue
            
            #Check if it is an API call
            if class_name in self.__APIclasses:
                is_api = True
                continue  # Do not analyze calls from APIs
            else:
                is_api = False
            

            if len(clase.get_xref_to()) == 0:
                is_sink = True
            else:
                is_sink = False
            
            # An alternative method to test for entry points is to check the 
            # input order of a node (isolated vertex)
            if len(clase.get_xref_from()) == 0:
                isolated = True
                if class_name not in self.__found_isolated:
                    __found_isolated_append(class_name)
            else:
                isolated = False
            
            
            
            _add_node(CG, clase, is_sink, is_api, isolated)
            
            
            
            for other_class in clase.get_xref_to():
                if other_class.name == class_name:  # Skip cycles
                    continue
                
                # Do not add external libs (ads) code
                if no_external and other_class.name in self.__external_libs:
                    #print("Skipped {}, because if is a external lib".format(class_name))
                    continue
                
                #Check if it is an API call and count callers that are from internal classes
                if other_class.name in self.__APIclasses:
                    is_api = True
                    # Update list of APIs in use
                    try:
                        self.__api_usages[other_class.name] += 1
                    except KeyError:
                        self.__api_usages[other_class.name] = 1
                else:
                    is_api = False
                    
                
                # Check if new class is a sink
                if len(other_class.get_xref_to())==0:
                    _add_node(CG, other_class, is_sink=True, is_api = is_api, isolated = False)
                else:
                    _add_node(CG, other_class, is_sink=False, is_api = is_api, isolated = False)

                # As this is a DiGraph and we are not interested in duplicate edges,
                # check if the edge is already in the edge set.
                # If you need all calls, you probably want to check out MultiDiGraph
                if not CG_has_edge(class_name, other_class.name):
                    CG_add_edge(class_name, other_class.name)
                
        self._CG = CG
    

    def __getRandomPath ( self, path_size, APIlist, exclude_list, only_isolated=False, same_walk_prob = .7, methods=True ):
        """
        Makes a random walk over the graph taking a random initial point as one of the entry points detected in the manifest

        Parameters
        ----------
        path_size : int
            Maximum size of the walk sequence obtained.
        APIlist : List []
            List of strings containing the names of the APIs to consider. 
            The format is (smali)
        exclude_list : boolean
            If True, the apis in APIlist are excluded, if False, only the apis
            in the list are included.
        only_isolated : boolean
            If True, only isolated nodes are taken as init points of the walk.
            If False, methods from declared entry points and isolated nodes are
            used.
        same_walk_prob : float
            Maximum probability of jumping on an already visited node when an API is found. Default is 0.7. It is not 
            recommended to select higher values to guarantee a trade-off between path and graph coverage.
        methods : boolean
            True if sequences have to be extracted from the methods call graph. false to utilize the classes call graph

        Returns
        -------
        walk : [] List
            List of graph API nodes visited.

        """
        if methods:
            G = self._G
        else:
            G = self._CG
        
        visited = []
        walk = []
        n_added = 0
        if same_walk_prob>0.80:
            print("same_walk_prob param cannot take values over 0.8 to guarantee graph coverage, setting it to 0.8")
            same_walk_prob=0.80
        
        # Select init_points
        if only_isolated:
            init_points = self.__found_isolated
        else:
            init_points = list(set(self.__found_entry_p + self.__found_isolated))
        
        # Choose and entry point
        if init_points:
            init = np.random.randint(0, len(init_points ))
            node_name = init_points[init]
        else:  # If entry points are not found, init is a random node of the graph
            init = np.random.randint(0, len(G.nodes))
            node_name = list(G.nodes())[init]
        
        # Random walk from initial
        while n_added<path_size:
            node = G.nodes[node_name]
            if node['api']:  # If it is an API call, add to the sequence
                # Apply filtering
                if node_name not in APIlist and exclude_list or node_name in APIlist and not exclude_list:
                    walk.append(node_name)
                    n_added += 1
            else:
                # Add node as visited if it is not an API
                visited.append(node_name)
            # Continue our walk to the next node
            neighbors = list(G.neighbors(node_name))
            if neighbors:  # Select a new path among the neighbors 
                going_to = np.random.randint(0, len(neighbors))
                node_name = neighbors[going_to]
            else:  
                prob = np.random.random()
                if prob>=1-same_walk_prob: # Return to any previous node with 0.7 prob
                    going_to = np.random.randint(0, len(visited))
                    node_name = visited[going_to]
                else:  # Visit any node of the graph with 0.3 prob
                    going_to = np.random.randint(0, len(G.nodes))
                    node_name = list(G.nodes())[going_to]
        return walk
    



##########################
# Public methods
##########################

    def getInstructionsSequences (self):
        """
        For all methods in the APK get the sequences of instructions in their code

        Returns
        -------
        None.

        """
        instructions = []
        for method in self.__dex_analysis.get_methods():
            if method.is_external():
                continue  # Skip methods
            
            m = method.get_method()
            if not m: continue
            # Get the list of instruction codes of the method
            if m.get_instructions():
                for ins in m.get_instructions():
                    # Update dictionary
                    instructions.append(ins.get_name())
        return instructions
    
    
    def getInstructionsOpcodeSequences (self):
        """
        For all methods in the APK get the sequences of instructions in their code

        Returns
        -------
        None.

        """
        instructions = []
        for method in self.__dex_analysis.get_methods():
            if method.is_external():
                continue  # Skip methods
            
            m = method.get_method()
            if not m: continue
            # Get the list of instruction codes of the method
            if m.get_instructions():
                for ins in m.get_instructions():
                    # Update dictionary
                    instructions.append(ins.get_op_value())
        return instructions
    
    

    def getMethodsOpcodes (self):
        """
        Method that returns an opcode sequence containing all the opcode sequences of the methods in the APK
        
        Returns
        -------
        sequences : list
            list of ints with the opcodes

        """
        seqs = self.getCallerOpcodeSequences(depth=0)
        seq = []
        return list(map(seq.extend, seqs))
    
    
    def getOpcodeFrequencies (self):
        """
        Method that returns an opcode frequencies considering all methods in the APK
        
        Returns
        -------
        sequences : dic
            opcode as key and count as value

        """
        freq = {}
        for l in self.getMethodsOpcodes():
            for opc in l:
                try:
                    freq[opc] += 1
                except:
                    freq[opc] = 1
        return freq

    
    def getOpcodeSequencesFromEntryPointActivities (self, depth=5):
        """
        
        Method based on original code by Canfora et al. in "An HMM and structural entropy based detector for Android malware: An empirical study"

        Parameters
        ----------
        depth : max recursion limit

        Returns
        -------
        sequences : list
            list of ints with the opcodes

        """
        main_act = None
        main_method = None
        main_class = None
        
        if self.__APKinfo.get_main_activity() != None:
            main_act_name = 'L'+self.__APKinfo.get_main_activity().replace('.', '/')+';'
            main_act = self.__dex_analysis.get_class_analysis(main_act_name)
            
        
        if main_act is None:
            activities = self.__APKinfo.get_activities()
            if len(activities)>0:
                main_act_name = 'L'+activities[0].replace('.', '/')+';'
                main_act = self.__dex_analysis.get_class_analysis(main_act_name)
            
        if main_act is None:
            return []
        
        super_class_name = main_act.extends
        interfaces = main_act.implements
        for m in main_act.get_methods():
            if m.name == 'onCreate':
                main_method = m
                main_class = main_act
        
        if main_method is None and super_class_name is not None:
            super_class = self.__dex_analysis.get_class_analysis(super_class_name)
            if super_class is not None:
                for m in super_class.get_methods():
                    if m.name == 'onCreate':
                        main_method = m
                        main_class = super_class
                        break
        if main_method is None:
            for i in interfaces:
                interface = self.__dex_analysis.get_class_analysis(i)
                if interface is not None:
                    for m in interface.get_methods():
                        if m.name == 'onCreate':
                            main_method = m
                            main_class = interface
                            break
        
        if main_method is not None:
            return self.__extractOpcodesRecursively(main_method, depth)
        
        return []
    
    

    def getRandomWalkClassSequences ( self, n_walks, APIlist=[], exclude_list = True, only_isolated=False, max_walk_size=-1, limit_attempts=50, no_external=False ):
        """
        Get a list of sequences as a result of randomly walking the class call graph

        Parameters
        ----------
        n_walks : int
            Number of walks that are returned.
        APIlist : List []
            List of strings containing the names of the API classes to consider in the analysis. 
            The format is (smali):
                Lclass_name;
        exclude_list : boolean
            If True, the classes in APIlist are excluded, if False, only the classes
            in the list are included.
        only_isolated : boolean
            If True, only isolated nodes are taken as init points of the walk.
            If False, methods from declared entry points and isolated nodes are
            used.
        max_walk_size : int, optional
            Number of API class nodes that every sequence contains. When default value is set (-1) the maximum size of
            is equivalent to the number of API classes found in the APK.
        limit_attempts : int, optional
            Number of repeated sequences found to end the search of random walks. Default is 50.

        Returns
        -------
        [[]] 
            List containing the unique sequences found during the random walks (=n_walk) of the graph. 

        """
        if not self._CG:
            self.__build_classes_graph(no_external)
            #print("Found ", len(self.__api_usages), "API classes in ", self.pkg)
            #print("Graph contains ", len(self._CG.nodes), "nodes")
            
        #print("Found entry points:")
        #pprint(self.__found_entry_p)
        #print("Found Isolated nodes:")
        #pprint(self.__found_isolated)
        
        if max_walk_size<0:
            max_walk_size = len(self.__api_usages)
        
        if (len(self.__api_usages)^2)<n_walks:
            n_walks = len(self.__api_usages)^2

        sequences = []
        n = 0
        f = 0
        while n<n_walks and f<limit_attempts:
            random_walk = self.__getRandomPath( max_walk_size, APIlist, exclude_list, only_isolated=only_isolated, methods = False )
            if random_walk not in sequences:
                sequences.append(random_walk)
                n += 1
                f = 0  # Reset attempts counter
            else: f += 1
        #print("Found", len(sequences), "Random Walks")
        return sequences
    

    def getRandomWalkSequences ( self, n_walks, APIlist=[], exclude_list = True, only_isolated=False, include_fields=False, max_walk_size=-1, limit_attempts=50, no_external=False ):
        """
        Get a list of sequences as a result of randomly walking the call graph

        Parameters
        ----------
        n_walks : int
            Number of walks that are returned.
        APIlist : List []
            List of strings containing the names of the API classes to consider in the analysis. 
            The format is (smali):
                Lclass_name;
        exclude_list : boolean
            If True, the classes in APIlist are excluded, if False, only the classes
            in the list are included.
        only_isolated : boolean
            If True, only isolated nodes are taken as init points of the walk.
            If False, methods from declared entry points and isolated nodes are
            used.
        include_fields : boolean
            If True, only fields are added as nodes and read/write operations are edges.
            If False, only methods are nodes.
        max_walk_size : int, optional
            Number of API class nodes that every sequence contains. When default value is set (-1) the maximum size of
            is equivalent to the number of API classes found in the APK.
        limit_attempts : int, optional
            Number of repeated sequences found to end the search of random walks. Default is 50.

        Returns
        -------
        [[]] 
            List containing the unique sequences found during the random walks (=n_walk) of the graph. 

        """
        # Return them
        if not self._G:
            self.__build_call_graph( no_external=no_external, include_fields=include_fields)
            #print("Found ", len(self.__api_usages), "API classes in ", self.pkg)
            #print("Graph contains ", len(self._G.nodes), "nodes")

        if max_walk_size<0:
            max_walk_size = len(self.__api_usages)
        
        if (len(self.__api_usages)*10)<n_walks:
            n_walks = len(self.__api_usages)*10

        sequences = []
        n = 0
        f = 0
        while n<n_walks and f<limit_attempts:
            random_walk = self.__getRandomPath( max_walk_size, APIlist, exclude_list, only_isolated )
            if random_walk not in sequences:
                sequences.append(random_walk)
                n += 1
                f = 0  # Reset attempts counter
            else: f += 1
        #print("Found", len(sequences), "Random Walks")
        return sequences
    
    
    def getWalkAPISequences (self, only_isolated=True, APIlist=[], exclude_list = True):
        """
        This method walks the code of the app from different entry poinst to 
        build API sequences found in the code. It recursively follows function 
        calls to find API sequences.
        
        
        Method used in MalDozer and Droidetec:
            https://www.sciencedirect.com/science/article/pii/S1742287618300392
            https://arxiv.org/abs/2002.03594
        
        Parameters
        ----------
        only_isolated : Boolean
            If True, entry points are isolated nodes of the graph (nodes 
            without input edges).
            If False, entry points declared in the Android manifest are also
            used
        APIlist : List [] or String
            List of API classes that are either included or omited from the 
            analysis. Format is smali:
                Lpackage_name/class_name;
        exclude_list : boolean
            If True, the apis in APIlist are excluded, if False, only the apis
            in the list are included.

        Returns
        -------
        List [[]]
            The list containing the different subsequences found by inspecting 
            all the entry points.

        """
        if not self._G:
            self.getCallGraph()
        return self.__walkAPKforAPIs(only_isolated, APIlist, exclude_list)
    
    
    
    def getClassesCallGraph (self, no_external=True ):
        """
        Get the classes call graph of the apk. Nodes represent classes and the 
        edges the calls between them.
        
        Parameters
        ----------
        
        no_external : boolean
            Omits external class nodes
        

        Returns
        -------
        nx.Digraph
            The graph classes call graph object

        """
        if not self._CG:
            self.__build_classes_graph(no_external)
        return self._CG
    
    
    def getCallGraph (self, no_external = False, include_fields = False ):
        """
        Get the call graph of the apk.
        
        Parameters
        ----------

        no_external : boolean
            Omits external class nodes
            
        include_fields : boolean
            Include class fields as nodes
        

        Returns
        -------
        nx.Digraph
            The call graph object

        """
        
        if not self._G:
            self.__build_call_graph(no_external=no_external, include_fields=include_fields)
        return self._G
    
    
    def getL0MethodSignatures (self, join=True):
        """
        Gets a list with the method signatures of the APK as described in
        
        http://phrack.org/issues/68/15.html#article
        
        and as used in 
        
        https://link.springer.com/article/10.1007/s10664-014-9352-6
        
        join : boolean
            If True, a string containing the full signature is returned for 
            each method. If False, for every method, a list of BB signatures 
            (strings) is returned.

        Returns
        -------
        List []
            List containing the signatures of methods

        """
        signatures = []
        signatures_append = signatures.append
        for metodo in self.__dex_analysis.get_methods():
            if metodo.is_external() or metodo.get_method().class_name in self.__APIclasses:
                continue
            sign = self.__L0_method_signature(metodo, join)
            if sign:
                signatures_append(sign)
            
        return signatures
    
    
    
    def getControlFlowGraph (self, exclude_external_lib=False):
        """
        Return the Control Flow Graph (CFG) of the APK.
        
        Returns
        -------
        networkx.DiGraph
            The CFG of the APK, with method basic blocks being nodes and edges
            being the connections in code between them.

        """
        if not self._CFG:
            self.__getMethodsCFG(exclude_external_lib=exclude_external_lib)
            nodes = []
            nodes_extend = nodes.extend
            edges = []
            edges_extend = edges.extend
            for method in self._method_CFGs:
                G = self._method_CFGs[method]
                nodes_extend(list(G.nodes))
                edges_extend(list(G.edges))
            nodes = list(set(nodes))
            edges = list(set(edges))
            self._CFG = nx.DiGraph()
            self._CFG.add_nodes_from(nodes)
            self._CFG.add_edges_from(edges)
                
        return self._CFG
    
    
    def getAPIControlFlowGraph (self, exclude_external_lib = True):
        """
        Computes the API CFG of the APK.
        
        Returns
        -------
        networkx.DiGraph
            The API control flow Graph of the APK, with API methods being nodes
            and edges being the connections in code between them.

        """
        if not self._CFG:
            self.__getMethodsCFG(exclude_external_lib=exclude_external_lib, is_api=True)
            nodes = []
            nodes_extend = nodes.extend
            edges = []
            edges_extend = edges.extend
            for method in self._method_CFGs:
                G = self._method_CFGs[method]
                nodes_extend(list(G.nodes))
                edges_extend(list(G.edges))
            nodes = list(set(nodes))+['ROOT']
            # Link initial nodes to ROOT virtual node
            initial_funct_nodes = list(set(self.getIsolatedNodes() + self.getEntryPoints()))
            additional_edges = [('ROOT', method) for method in initial_funct_nodes]
            edges = list(set(list(set(edges))+additional_edges))
            self.__API_CFG = nx.DiGraph()
            self.__API_CFG.add_nodes_from(nodes)
            self.__API_CFG.add_edges_from(edges)
        
        self.__API_CFG = self.__filterAPIsFromGraph(self.__API_CFG, key='ROOT')
        return self.__API_CFG
        
    
    def getAPICFG_FromGraph (self, G=None, APIlist=[], exclude_list = True, exclude_external_lib = True):
        """
        Compute the CFG and extract the API CFG of the APK.
        
         G : networkx.DiGraph
            Control flow graph. If None, the CFG of the apk is calculated
        APIlist : List [] or String
            List of API classes that are either included or omited from the 
            analysis. Format is smali:
                Lpackage_name/class_name;
        exclude_list : boolean
            If True, the apis in APIlist are excluded, if False, only the apis
            in the list are included.
        exclude_external_lib : boolean
            If True, the methods from external libs (ads) are excluded.
        omit_descriptors
            
        
        Returns
        -------
        networkx.DiGraph
            The API control flow Graph of the APK, with API methods being nodes
            and edges being the connections in code between them.

        """
        api_CFG = nx.DiGraph()
        self.getControlFlowGraph(exclude_external_lib)
        initial_funct_nodes = list(set(self.getIsolatedNodes() + self.getEntryPoints()))
        # Link initial nodes
        self._CFG.add_node('ROOT')
        for method in initial_funct_nodes:
            self._CFG.add_edge('ROOT', method)
                         
        # Get only API call nodes
        self.__buildApiCFGIterative(self._CFG, api_CFG, 'ROOT', None, APIlist, exclude_list)
        
        return api_CFG
    
    
    
    def getAPICFG_FromGraph_par (self, APIlist=[], exclude_list = True, exclude_external_lib = True):
        """
        Return the API call Graph build from the CFG of the APK.
        
       
        APIlist : List [] or String
            List of API classes that are either included or omited from the 
            analysis. Format is smali:
                Lpackage_name/class_name;
        exclude_list : boolean
            If True, the apis in APIlist are excluded, if False, only the apis
            in the list are included.
        exclude_external_lib : boolean
            If True, the methods from external libs (ads) are excluded.
        omit_descriptors
            
        
        Returns
        -------
        networkx.DiGraph
            The API control flow Graph of the APK, with API methods being nodes
            and edges being the connections in code between them.

        """
        # Initialize Return Graph
        api_CFG = nx.DiGraph()
        
        if not self._method_CFGs:
            self.__getMethodsCFG(exclude_external_lib)
        
        initial_funct_nodes = list(set(self.getIsolatedNodes() + self.getEntryPoints()))
        
        nodes = []
        nodes_extend = nodes.extend
        edges = []
        edges_extend = edges.extend
        
        #output_q = mp.Queue()
        output_q = []
        lock = mp.Lock()
        nproc = mp.cpu_count()-2
        processes = []
        total_graphs = len(self._method_CFGs.keys())
        #print("Total graphs: ", total_graphs)
        part_size = total_graphs//(nproc)
        #print("Partitions: ", nproc, "of size: ", part_size)
        # Partition data and run the processes
        methods = list(self._method_CFGs.items())
        from_idx = 0
        to_idx = 0
        for n in range(nproc-1):
            from_idx = n*part_size
            to_idx = (n+1)*part_size
            processes.append(mp.Process(target=self._buildApiCFGIterative_par, args=( methods[from_idx:to_idx].copy(), n, self.__with_descriptors, self.__APIclasses.copy(), APIlist.copy(), exclude_list, initial_funct_nodes.copy(), output_q, lock)))
            #print("Proc:", n, " from ", from_idx, " to ", to_idx)
        
        for n in range(nproc-1):
            processes[n].start()
        
        #Run the reamining methods
        #lock.acquire()
        #print("Last from ", to_idx, " to ", total_graphs)
        #lock.release()
        self._buildApiCFGIterative_par(methods[to_idx:].copy(), n+1, self.__with_descriptors, self.__APIclasses.copy(), APIlist.copy(), exclude_list, initial_funct_nodes.copy(), output_q, lock)
            
        # Get the results
        for proc in processes:
            proc.join()
        
        #print("All finished. Collecting results from Queue")
        for G in output_q:
            #new_edges = output_q.get_nowait()  # Get the graphs
            nodes_extend(list(G.nodes))
            edges_extend(list(G.edges))
            
                
        # Build the full API CFG
        nodes = list(set(nodes))
        edges = list(set(edges))
        api_CFG.add_nodes_from(nodes)
        api_CFG.add_edges_from(edges)
        
        return api_CFG
            
    
    
    def getMethodControlFlowGraph (self, method_name):
        """
        Returns the CFG containing the basic blocks and API calls of the method.

        Parameters
        ----------
        method_name : String
            The name of the method. If several matches are found, only the first
            is returned.

        Returns
        -------
        String
            The code.

        """
        for meth in self.__dex_analysis.get_methods():
            enc_meth = meth.get_method()
            key = enc_meth.class_name + '->' + enc_meth.name + enc_meth.get_descriptor().replace(' ', '')
            if method_name in key:
                return self.__build_method_CFG(enc_meth)
    
    
    def getMethodCode (self, method_name):
        """
        Searchs for a method with param:method_name in the apk and returns its
        code

        Parameters
        ----------
        method_name : String
            The name of the method. If several matches are found, only the first
            is returned.

        Returns
        -------
        String
            The code.

        """
        for meth in self.__dex_analysis.get_methods():
            enc_meth = meth.get_method()
            key = enc_meth.class_name + '->' + enc_meth.name + enc_meth.get_descriptor().replace(' ', '')
            if method_name in key:
                try:
                    return enc_meth.get_source()
                except:
                    return ''
            
    
    def getMethodCodeObject (self, method_name):
        for meth in self.__dex_analysis.get_methods():
            if meth.is_external():
                continue
            enc_meth = meth.get_method()
            if enc_meth.class_name in self.__APIclasses:
                continue
            key = enc_meth.class_name + '->' + enc_meth.name + enc_meth.get_descriptor().replace(' ', '')
            if method_name in key:
                try:
                    return enc_meth.get_code().get_bc()
                except:
                    pass
        return None
    
    
    def getIntentCallers (self, only_initializers = False):
        intents_in = []
        for method in self.__dex_analysis.get_methods():
            m1 = method.get_method()
            if (m1.class_name=='Landroid/content/Intent;' and m1.name=='<init>') or ( not only_initializers and ('Extra' not in m1.name and re.search('\)Landroid/content/Intent;', m1.get_descriptor().replace(' ', '')))):
                for c, m, o in method.get_xref_from():
                    name = m.class_name + "->" +m.name + m.get_descriptor().replace(' ', '')
                    # Only analyze internal intents
                    if m.class_name not in self.__APIclasses and name not in intents_in:
                        intents_in.append(name)
        return intents_in
    
    
    def getIntentFilterCallers (self, only_initializers = False):
        intentf_in = []
        for method in self.__dex_analysis.get_methods():
            m1 = method.get_method()
            if (m1.class_name=='Landroid/content/IntentFilter;' and m1.name=='<init>') or ( not only_initializers and re.search('\)Landroid/content/IntentFilter;', m1.get_descriptor().replace(' ', ''))):
                for c, m, o in method.get_xref_from():
                    name = m.class_name + "->" +m.name + m.get_descriptor().replace(' ', '')
                    # Only analyze internal intents
                    if m.class_name not in self.__APIclasses and name not in intentf_in:
                        intentf_in.append(name)
        return intentf_in
    
    
    
    #
    #  
    #
    def _taintIntentFilterCode (self, bytecode, return_finished = True, intentf_as_argument = 0, n_rec_calls=0):
        """
        Parses instructions and taints registers to collect intentFilter data

        Parameters
        ----------
        bytecode : androguard.core.bytecodes.dvm.DCode
            Bytecode of the method that initializes an intent
        return_finished : Boolean
            Only return intents with a call to the startActivity method. The default is True.
        intentf_as_argument : int
            Number of intents filters received as arguments by the method
        

        Returns
        -------
        called_intent_filters : List[Dictionary]
            A list with intent filter information (dictionary) for each intent filters.
        called_broadcast_receivers : List[Dictionary]
            A list with broadcast receivers information (dictionary) for each broadcast receiver.

        """
        if n_rec_calls>=100: return [], []
        
        instructions = bytecode.get_instructions()
        called_intentf = []
        broadcast_recv = []
        intentf_info = {}
        registers = {}
        objects = {}
        intentf_registers = []
        received_intentf = []
        last_reg = None
        last_return_type = ''
        # Search for Intent object
        for ins in instructions:
            inst_name = ins.get_name()
            out = ins.get_output()
            operands = out.split(', ')  # [v0, v1, 'kakaka']
            current_instruction = inst_name+' '+out
            if len(operands)==0: break  # Return instruction
            #print(operands)
            
            #print(inst_name, operands)
            # Move operation
                        
            #if 'move' in inst_name and 'exception' not in inst_name:  # move
            if re.search('^move(?:|\/from16|-wide(?:\/from16|\/16)|-object(?:|\/from16|\/16))? v([0-9]+), (v[0-9]+)$', current_instruction):  # move
                rematch = re.search('^move(?:|\/from16|-wide(?:\/from16|\/16)|-object(?:|\/from16|\/16))? v([0-9]+), (v[0-9]+)$', current_instruction)
                regto_num = rematch.groups()[0]
                regfrom_num = rematch.groups()[1]
                
                if regfrom_num in intentf_registers:
                    if regto_num not in intentf_registers: intentf_registers.append(regto_num)
                    intentf_info[regto_num] = intentf_info[regfrom_num]
                else:
                    if regto_num in intentf_registers: intentf_registers.remove(regto_num)
                    try:
                        registers[regto_num] = registers[regfrom_num]
                    except:
                        pass
            # MOVE_RESULT
            elif re.search('^move(?:-result(?:|-wide|-object)|-exception)? v([0-9]+)$', current_instruction):
                rematch = re.search('^move(?:-result(?:|-wide|-object)|-exception)? v([0-9]+)$', current_instruction)
                regto_num = rematch.groups()[0]
                #print('Returned',last_return_type)
                if 'Landroid/content/IntentFilter;' in last_return_type:
                    if last_reg in intentf_registers:
                        if regto_num not in intentf_registers: intentf_registers.append(regto_num)
                    try:
                        intentf_info[regto_num] = intentf_info[last_reg]
                    except:
                        intentf_info[regto_num] = {}
                else:
                    if regto_num in intentf_registers: intentf_registers.remove(regto_num)
                    registers[regto_num] = last_return_type
            
            # INVOKE no parameter
            elif re.search('^invoke-(?:static|virtual|direct|super|interface|interface-range|virtual-quick|super-quick) (L(?:.*);->.*)$', current_instruction):
                rematch = re.search('^invoke-(?:static|virtual|direct|super|interface|interface-range|virtual-quick|super-quick) (L(?:.*);->.*)$', current_instruction)
                call_to = rematch.groups()[0]
                last_return_type = call_to.split(')')[-1]
            
            # INVOKE 1 Parameter
            elif re.search('^invoke-(?:static|virtual|direct|super|interface|interface-range|virtual-quick|super-quick) v([0-9]+), (L(?:.*);->.*)$', current_instruction):
                
                rematch = re.search('^invoke-(?:static|virtual|direct|super|interface|interface-range|virtual-quick|super-quick) v([0-9]+), (L(?:.*);->.*)$', current_instruction)
                reg_num = rematch.groups()[0]
                call_to = rematch.groups()[1]
                last_return_type = call_to.split(')')[-1]
                
                # If the call return type is an IntentFilter and we came from
                # another method that passed an IntentFilter as argument, we know that the result 
                # is the IntentFilter we are tracing
                if intentf_as_argument>0 and 'Landroid/content/IntentFilter;' in last_return_type and reg_num not in intentf_registers: 
                    # Add the new register to the intent registers collection
                    if reg_num not in intentf_registers: intentf_registers.append(reg_num)
                    intentf_info[reg_num] = {}
                    intentf_as_argument -= 1   # This is valid only for n arguments
                    received_intentf.append(reg_num)
                    
                if 'Landroid/content/IntentFilter;-><init>(' in call_to:
                    intentf_info[reg_num] = {}
                    if reg_num not in intentf_registers: intentf_registers.append(reg_num)
                else:
                    if reg_num in intentf_registers: intentf_registers.remove(reg_num)
                    #registers[reg_num] = last_return_type
            
            # INVOKE MORE THAN 1 parameter
            elif re.search('^invoke-(?:static|virtual|direct|super|interface|interface-range|virtual-quick|super-quick) (v[0-9]+,){2,} (L(?:.*);->.*)$', current_instruction):
                rematch = re.search('^invoke-(?:static|virtual|direct|super|interface|interface-range|virtual-quick|super-quick) (v[0-9]+,){2,} (L(?:.*);->.*)$', current_instruction)
                regs = rematch.groups()[0]
                # parse all args of the call
                args = []
                for reg in re.search('v([0-9]+),').groups():
                    args.append(reg)
                
                call_to = rematch.groups()[1]
                last_return_type = call_to.split(')')[-1]
                
                # If the call return type is an IntentFilter and we came from
                # another method that passed an IntentFilter as argument, we know that the result 
                # is the IntentFilter we are tracing
                if intentf_as_argument>0 and 'Landroid/content/IntentFilter;' in last_return_type and args[0] not in intentf_registers: 
                    # Add the new register to the intent registers collection
                    if reg not in intentf_registers: intentf_registers.append(reg)
                    intentf_info[reg] = {}
                    intentf_as_argument -= 1   # This is valid only for n arguments
                    received_intentf.append(reg)
                
                
                # Finish intent -> OUT_POINT
                if '->registerReceiver(' in call_to:
                    try:
                        bcast_data = registers[args[0]]
                    except:
                        continue
                    if args[1] in received_intentf:
                        broadcast_recv.append(bcast_data)
                        intentf_info[args[1]]['registered'] = True
                        intentf_info[args[1]]['BroadcastRecv'] = bcast_data
                    elif args[1] in intentf_registers:
                        intentf_info[args[1]]['registered'] = True
                        intentf_info[args[1]]['BroadcastRecv'] = bcast_data
                        broadcast_recv.append(bcast_data)
                        called_intentf.append(intentf_info[args[1]])
                     
                
                # Parse IntentFilter-related instructions
                elif reg in intentf_registers and 'static' not in inst_name:  #Intent operation
                    operation = []
                    idx_init = call_to.index(';->') + 3
                    
                    # Initialization of intent with arguments. Parse possible arguments (properties)
                    if 'Landroid/content/IntentFilter;-><init>(' in call_to:
                        intentf_info[reg] = {}
                        
                        idx_init = call_to.index('(') + 1 
                        idx_fin = call_to.index(')')
                        # Get function arguments
                        function_arg_dtypes = call_to[idx_init+1:idx_fin].split(' ') 
                        n = 0
                        for arg_type in function_arg_dtypes:
                            if 'String' in arg_type and n==0:
                                operation.append('Action')
                            elif 'String' in  arg_type and n>0:
                                operation.append('DataType')
                            elif 'IntentFilter' in arg_type:  # Is a copy constructor
                                try:
                                    info = intentf_info[args[0]]
                                except:
                                    info = {}
                                    intentf_info[args[0]] = {}
                                    if args[0] not in intentf_registers: intentf_registers.append(args[0])
                                intentf_info[reg] = info
                                break
                    
                    # Set properties of intents: Extract the property that is being set or added
                    elif call_to[idx_init:idx_init+3] in ['add', 'set']:
                        idx_fin = call_to.index('(')
                        operation = [call_to[idx_init+3:idx_fin]]
                        
                    
                    try:
                        if len(operation)>0:
                            info = intentf_info[reg]
                            # Parse argument values
                            values = []
                            n = 0
                            for arg_reg in args:
                                try:
                                    #print(operation[n], arg_reg)
                                    values.append(registers[arg_reg])
                                except KeyError:
                                    idx_ini = call_to.index('(')
                                    new_fin = call_to.index(')')
                                    # Get function arguments
                                    function_arg_dtypes = call_to[idx_ini+1:new_fin].split(' ') 
                                    arg_type = function_arg_dtypes[n]
                                    values.append(arg_type)
                                n += 1
                            
                            if len(operation)==len(values):
                                n = 0
                                for op in operation:
                                    info[op] = values[n]
                                    n += 1
                            else:
                                operation = operation[0]
                                info[operation] = values
                                
                            #Update info of the intent
                            intentf_info[reg] = info
                    except TypeError:
                        #print(inst_name, operands)
                        #traceback.print_exc()
                        pass
                else:
                    # An intent is passed as argument to other function
                    # We count the number of intent filter registers that we have to trace
                    args_int = []
                    for arg in args:
                        # Is a call to a method that receives an intent-filter as argument?
                        if arg in intentf_registers:
                            args_int.append(arg)
                    if len(args_int)>0:
                        code = self.getMethodCodeObject(call_to.replace(' ', ''))
                        if code:
                            res, br = self._taintIntentFilterCode(code, return_finished=False, intentf_as_argument=len(args_int), n_rec_calls=n_rec_calls+1)
                            broadcast_recv.extend(br)
                            if len(res)>0:
                                for n in range(0,len(args_int)):  #Update intent-filter info with results
                                    try:    
                                        intentf_info[args_int[n]] =  {**intentf_info[args_int[n]], **res[n]}
                                        if intentf_info[args_int[n]]['registered'] == True:
                                            called_intentf.append(intentf_info[args_int[n]])
                                    except (IndexError,KeyError):
                                        #print(inst_name, operands)
                                        #traceback.print_exc()
                                        pass
                    
                    
            # NEW-INSTANCE instructions
            elif re.search('^new-instance v([0-9]+), (L(?:.*);)$', current_instruction):
                rematch = re.search('^new-instance v([0-9]+), (L(?:.*);)$', current_instruction)
                reg = rematch.groups()[0]
                dtype = rematch.groups()[1]
                last_return_type = ''
                if dtype=='Landroid/content/IntentFilter;':
                    if reg not in intentf_registers: intentf_registers.append(reg)
                    intentf_info[reg] = {}
                else:
                    # Remove the register if the op produces a new instance
                    if reg in intentf_registers: intentf_registers.remove(reg)
                    registers[reg] = dtype  # Get only the string or class name
            # CONST INSTRUCTION
            elif re.search('^const(?:\/4|\/16|\/high16|-wide(?:\/16|\/32)|-wide\/high16|)? v([0-9]+), \+?(-?[0-9]+(?:\.[0-9]+)?)$', current_instruction):
                rematch = re.search('^const(?:\/4|\/16|\/high16|-wide(?:\/16|\/32)|-wide\/high16|)? v([0-9]+), \+?(-?[0-9]+(?:\.[0-9]+)?)$', current_instruction)
                reg = rematch.groups()[0]
                value = rematch.groups()[1]
                last_return_type = ''
                registers[reg] = dtype
                if reg in intentf_registers: intentf_registers.remove(reg)
            
            # Array declaration
            elif 'new-array' in inst_name:
                last_return_type = ''
                info = []
                try:
                    size_reg = operands[1]  # size of the array
                    dtype = operands[-1]  # Type
                    size = registers[size_reg]
                except (KeyError, IndexError):
                    last_reg = None
                    continue
                if type(size) is not int: 
                    last_reg = None
                    continue
                if 'Landroid/content/IntentFilter;' in dtype:  #Is an intent
                    for e in range(size%100):
                        info.append({})
                    if reg not in intentf_registers: intentf_registers.append(reg)
                    intentf_info[reg] = info
                else:
                    for e in range(size%100):
                        info.append(None)
                    if reg in intentf_registers: intentf_registers.remove(reg)
                    registers[reg] = info  # Another dtype
            
            # Get op
            elif 'aget' in inst_name:  # array get
                rematch = re.search('^aget(?:-wide|-object|-boolean|-byte|-char|-short|) v([0-9]+), v([0-9]+), v([0-9]+)$', current_instruction)
                reg_dest = rematch.groups()[0]
                array_reg = rematch.groups()[1]
                index = rematch.groups()[2]
                last_return_type = ''
                if array_reg in intentf_registers:  # Copy from intent Array
                    try:
                        info = intentf_info[array_reg][registers[index]]
                        intentf_info[reg_dest] = info
                    except:
                        intentf_info[reg_dest] = {}
                    if reg_dest not in intentf_registers: intentf_registers.append(reg_dest)

                else:
                    if reg_dest in intentf_registers: intentf_registers.remove(reg_dest)
                    try:
                        info = registers[array_reg][registers[index]]
                        registers[reg_dest] = info
                    except:
                        last_reg = None
                        continue
            
            elif 'sget' in inst_name:
                last_return_type = ''
                rematch = re.search('^sget(?:-wide|-object|-boolean|-byte|-char|-short|) (v[0-9]+), (L(?:.*);)$', current_instruction)
                reg_dest = rematch.groups()[0]
                field = rematch.groups()[1].split(' ')
                field_name = field[0]
                field_dtype = field[1]
                
                if field_name in objects.keys():
                    info = objects[field_name]
                elif field_dtype == 'Landroid/content/IntentFilter;':
                    info = {}
                else:
                    info = field_dtype
                    objects[field_name] = field_dtype
                
                # If field is an Intent
                if field_dtype == 'Landroid/content/IntentFilter;':
                    if reg_dest not in intentf_registers: intentf_registers.append(reg_dest)
                    intentf_info[reg_dest] = info
                # Another object
                else: 
                    if reg_dest in intentf_registers: intentf_registers.remove(reg_dest)
                    registers[reg_dest] = info
                
            elif 'iget' in inst_name:
                last_return_type = ''
                rematch = re.search('^iget(?:-wide|-object|-boolean|-byte|-char|-short|) v([0-9]+), v([0-9]+), (L(?:.*);)$', current_instruction)
                reg_dest = rematch.groups()[0]
                obj_reg = rematch.groups()[1]
                field = rematch.groups()[2].split(' ')
                field_name = field[0]
                field_dtype = field[1]

                # Fill this method                

                if field_name in objects.keys():
                    info = objects[field_name]
                elif field_dtype == 'Landroid/content/IntentFilter;':
                    info = {}
                else:
                    info = field_dtype
                    objects[field_name] = field_dtype
                
                # If field is an Intent
                if field_dtype == 'Landroid/content/IntentFilter;':
                    if reg_dest not in intentf_registers: intentf_registers.append(reg_dest)
                    intentf_info[reg_dest] = info
                # Another object
                else: 
                    if reg_dest in intentf_registers: intentf_registers.remove(reg_dest)
                    registers[reg_dest] = info


                    
            # Put op
            elif 'aput' in inst_name:  # array put
                rematch = re.search('^aput(?:-wide|-object|-boolean|-byte|-char|-short|) v([0-9]+), v([0-9]+), v([0-9]+)$', current_instruction)
                reg_source = rematch.groups()[0]
                array_reg = rematch.groups()[1]
                index = rematch.groups()[2]
                last_return_type = ''
                if reg_source in intentf_registers:  # Intent field
                    try:
                        info = intentf_info[reg_source]
                        intentf_info[array_reg] = info
                    except:
                        intentf_info[array_reg] = {}
                    if array_reg not in intentf_registers: intentf_registers.append(array_reg)
                else:
                    try:
                        if array_reg in intentf_registers: intentf_registers.remove(array_reg)
                        registers[array_reg] = registers[reg_source]
                    except:
                        continue
                
            elif 'iput' in inst_name:  # Instance field put
                last_return_type = ''
                rematch = re.search('^iput(?:-wide|-object|-boolean|-byte|-char|-short|) v([0-9]+), v([0-9]+), (L(?:.*);)$', current_instruction)
                reg_source = rematch.groups()[0]
                obj_reg = rematch.groups()[1]
                field = rematch.groups()[2].split(' ')
                field_name = field[0]
                field_dtype = field[1]
                try:
                    if reg_source in intentf_registers:
                        info = intentf_info[reg_source]
                    elif field_dtype == 'Landroid/content/IntentFilter;':
                        info = {}
                    else:
                        info = registers[reg_source]
                except:
                    info = field_dtype

                objects[field_name] = info
            
            elif 'sput' in inst_name:
                last_return_type = ''
                rematch = re.search('^sget(?:-wide|-object|-boolean|-byte|-char|-short|) (v[0-9]+), (L(?:.*);)$', current_instruction)
                reg_source = rematch.groups()[0]
                field = rematch.groups()[1].split(' ')
                field_name = field[0]
                field_dtype = field[1]
                
                try:
                    if reg_source in intentf_registers:
                        info = intentf_info[reg_source]
                    elif field_dtype == 'Landroid/content/IntentFilter;':
                        info = {}
                    else:
                        info = registers[reg_source]
                except:
                    info = field_dtype

                objects[field_name] = info
                    
            elif 'add' in inst_name:
                b = operands[-1]
                if 'addr' in inst_name:
                    try:
                        registers[reg] += registers[b]
                    except:
                        registers[reg] = 0
                    
                elif 'lit' in inst_name:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] + b[1]
                    except:
                        registers[reg] = 0
                else:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] + registers[b]
                    except:
                        registers[reg] = 0
            elif 'sub' in inst_name:
                b = operands[-1]
                if 'addr' in inst_name:
                    try:
                        registers[reg] -= registers[b]
                    except:
                        registers[reg] = 0
                    
                elif 'lit' in inst_name:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] - b[1]
                    except:
                        registers[reg] = 0
                else:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] - registers[b]
                    except:
                        registers[reg] = 0
            
            elif 'mul' in inst_name:
                b = operands[-1]
                if 'addr' in inst_name:
                    try:
                        registers[reg] *= registers[b]
                    except:
                        registers[reg] = 0
                    
                elif 'lit' in inst_name:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] * b[1]
                    except:
                        registers[reg] = 0
                else:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] * registers[b]
                    except:
                        registers[reg] = 0
                        
            elif 'div' in inst_name:
                b = operands[-1]
                if 'addr' in inst_name:
                    try:
                        registers[reg] /= registers[b]
                    except:
                        registers[reg] = 0
                    
                elif 'lit' in inst_name:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] / b[1]
                    except:
                        registers[reg] = 0
                else:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] / registers[b]
                    except:
                        registers[reg] = 0
                        registers[b] = 0
            
            elif 'rem' in inst_name:
                b = operands[-1]
                if 'addr' in inst_name:
                    try:
                        registers[reg] %= registers[b]
                    except:
                        registers[reg] = 0
                    
                elif 'lit' in inst_name:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] % b
                    except:
                        registers[reg] = 0
                else:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] % registers[b]
                    except:
                        registers[reg] = 0
            
            elif 'and' in inst_name:
                b = operands[-1]
                if 'addr' in inst_name:
                    try:
                        registers[reg] &= registers[b]
                    except:
                        registers[reg] = 0
                    
                elif 'lit' in inst_name:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] & b
                    except:
                        registers[reg] = 0
                else:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] & registers[b]
                    except:
                        registers[reg] = 0
            
            elif 'or' in inst_name:
                b = operands[-1]
                if 'addr' in inst_name:
                    try:
                        registers[reg] |= registers[b]
                    except:
                        registers[reg] = 0
                    
                elif 'lit' in inst_name:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] | b
                    except:
                        registers[reg] = 0
                else:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] | registers[b]
                    except:
                        registers[reg] = 0
            
            elif 'xor' in inst_name:
                b = operands[-1]
                if 'addr' in inst_name:
                    try:
                        registers[reg] ^= registers[b]
                    except:
                        registers[reg] = 0
                    
                elif 'lit' in inst_name:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] ^ b
                    except:
                        registers[reg] = 0
                else:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] ^ registers[b]
                    except:
                        registers[reg] = 0
            elif 'shl' in inst_name:
                b = operands[-1]
                if 'addr' in inst_name:
                    try:
                        registers[reg] <<= registers[b]
                    except:
                        registers[reg] = 0
                    
                elif 'lit' in inst_name:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] << b
                    except:
                        registers[reg] = 0
                else:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] << registers[b]
                    except:
                        registers[reg] = 0
            
            elif 'shr' in inst_name:
                b = operands[-1]
                if 'addr' in inst_name:
                    try:
                        registers[reg] >>= registers[b]
                    except:
                        registers[reg] = 0
                    
                elif 'lit' in inst_name:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] >> b
                    except:
                        registers[reg] = 0
                else:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] >> registers[b]
                    except:
                        registers[reg] = 0
            elif 'ushr' in inst_name:
                b = operands[-1]
                if 'addr' in inst_name:
                    try:
                        registers[reg] =  abs(registers[reg] >> registers[b])
                    except:
                        registers[reg] = 0
                    
                elif 'lit' in inst_name:
                    try:
                        registers[reg] = abs(registers[operands[1:-1][0]] >> b)
                    except:
                        registers[reg] = 0
                else:
                    try:
                        registers[reg] = abs(registers[operands[1:-1][0]] >> registers[b])
                    except:
                        registers[reg] = 0   

            else:
                last_return_type = ''
                    
            last_reg = reg
        
        del instructions 
        del registers
        del objects
        del intentf_registers
        
        if return_finished:
            return called_intentf, broadcast_recv
        return [intentf_info[reg] for reg in received_intentf], broadcast_recv
    
    
    # TODO
    #
    # Intent objects put or get from and into fields of other objects
    # are not traced outside the present method
    def _taintIntentCode (self, bytecode, return_finished = True, intent_as_argument = 0, n_rec_calls=0, debug=False):
        """
        Parses instructions and taints registers to collect intent data

        Parameters
        ----------
        bytecode : androguard.core.bytecodes.dvm.DCode
            Bytecode of the method that initializes an intent
        return_finished : Boolean
            Only return intents with a call to the startActivity method. The default is True.
        intent_as_argument : int
            Number of intents received as arguments by the method
        

        Returns
        -------
        called_intents : List[Dictionary]
            A list with intents information (dictionary) for each intent.

        """
        if n_rec_calls>=100: return []
        
        instructions = bytecode.get_instructions()
        called_intents = []  # Stores the intents that are started
        intent_info = {}  # Contains the information of intent objects
        registers = {}  # Contains the values of general registers
        objects = {}  # Constains object field values
        intent_registers = []  # Contains the registers that contain Intent objects
        received_intents = []  # Stores the intents that are received by param
        last_reg = None
        last_return_type = '' 
        n_iter = 0
        # Search for Intent object
        for ins in instructions:
            inst_name = ins.get_name()
            out = ins.get_output()
            operands = out.split(', ')  # [v0, v1, 'kakaka']
            current_instruction = inst_name+' '+out
            if len(operands)==0: break  # Return instruction
            #print(inst_name, operands)
            reg = operands[0]
            
            if debug:
                n_iter += 1
                print(inst_name, operands)
                if n_iter%75==0:
                    print('----------')
                    print('Registers', len(registers))
                    print('IntentInfo', len(intent_info))
                    print('IntentRegisters', len(intent_registers))
                    print('Objects', len(objects))
            # Move result operation
            if re.search('^move(?:|\/from16|-wide(?:\/from16|\/16)|-object(?:|\/from16|\/16))? v([0-9]+), (v[0-9]+)$', current_instruction):  # move
                rematch = re.search('^move(?:|\/from16|-wide(?:\/from16|\/16)|-object(?:|\/from16|\/16))? v([0-9]+), (v[0-9]+)$', current_instruction)
                regto_num = rematch.groups()[0]
                regfrom_num = rematch.groups()[1]
                
                if regfrom_num in intent_registers:
                    if regto_num not in intent_registers: intent_registers.append(regto_num)
                    intent_info[regto_num] = intent_info[regfrom_num]
                else:
                    if regto_num in intent_registers: intent_registers.remove(regto_num)
                    try:
                        registers[regto_num] = registers[regfrom_num]
                    except:
                        pass
            # MOVE_RESULT
            elif re.search('^move(?:-result(?:|-wide|-object)|-exception)? v([0-9]+)$', current_instruction):
                rematch = re.search('^move(?:-result(?:|-wide|-object)|-exception)? v([0-9]+)$', current_instruction)
                regto_num = rematch.groups()[0]
                #print('Returned',last_return_type)
                if 'Landroid/content/Intent;' in last_return_type:
                    if last_reg in intent_registers:
                        if regto_num not in intent_registers: intent_registers.append(regto_num)
                    try:
                        intent_info[regto_num] = intent_info[last_reg]
                    except:
                        intent_info[regto_num] = {}
                else:
                    if regto_num in intent_registers: intent_registers.remove(regto_num)
                    registers[regto_num] = last_return_type
            # INVOKE no parameter
            elif re.search('^invoke-(?:static|virtual|direct|super|interface|interface-range|virtual-quick|super-quick) (L(?:.*);->.*)$', current_instruction):
                rematch = re.search('^invoke-(?:static|virtual|direct|super|interface|interface-range|virtual-quick|super-quick) (L(?:.*);->.*)$', current_instruction)
                call_to = rematch.groups()[0]
                last_return_type = call_to.split(')')[-1]
            # INVOKE 1 Parameter
            elif re.search('^invoke-(?:static|virtual|direct|super|interface|interface-range|virtual-quick|super-quick) v([0-9]+), (L(?:.*);->.*)$', current_instruction):
                
                rematch = re.search('^invoke-(?:static|virtual|direct|super|interface|interface-range|virtual-quick|super-quick) v([0-9]+), (L(?:.*);->.*)$', current_instruction)
                reg_num = rematch.groups()[0]
                call_to = rematch.groups()[1]
                last_return_type = call_to.split(')')[-1]
                
                # If the method return type is an Intent and we came from
                # another method that has received it from an argument
                if intent_as_argument>0 and ')Landroid/content/Intent;' in call_to and reg_num not in intent_registers: 
                    # Add the new register to the intent registers collection
                    #print('Received register', reg_num)
                    if reg_num not in intent_registers: intent_registers.append(reg_num)
                    intent_info[reg_num] = {}
                    intent_as_argument -= 1   # This is valid only for n arguments
                    received_intents.append(reg_num)
                
                if 'Landroid/content/Intent;-><init>(' in call_to:
                    intent_info[reg_num] = {}
                    if reg_num not in intent_registers: intent_registers.append(reg_num)
                else:
                    if reg_num in intent_registers: intent_registers.remove(reg_num)
                    #registers[reg_num] = last_return_type
            # INVOKE MORE THAN 1 parameter
            elif re.search('^invoke-(?:static|virtual|direct|super|interface|interface-range|virtual-quick|super-quick) (v[0-9]+,){2,} (L(?:.*);->.*)$', current_instruction):
                rematch = re.search('^invoke-(?:static|virtual|direct|super|interface|interface-range|virtual-quick|super-quick) (v[0-9]+,){2,} (L(?:.*);->.*)$', current_instruction)
                regs = rematch.groups()[0]
                # parse all args of the call
                args = []
                for reg in re.search('v([0-9]+),').groups():
                    args.append(reg)
                
                call_to = rematch.groups()[1]
                last_return_type = call_to.split(')')[-1]
                # If the method return type is an Intent and we came from
                # another method that has received it from an argument
                if intent_as_argument>0 and ')Landroid/content/Intent;' in call_to and reg_num not in intent_registers: 
                    # Add the new register to the intent registers collection
                    #print('Received register', reg_num)
                    if reg_num not in intent_registers: intent_registers.append(reg_num)
                    intent_info[reg_num] = {}
                    intent_as_argument -= 1   # This is valid only for n arguments
                    received_intents.append(reg_num)
                
                # Finish intent starting an Activity-> OUT_POINT
                if 'startActivity(Landroid/content/Intent;)V' in call_to or 'startActivityForResult(Landroid/content/Intent;' in call_to or '->startActivityFromChild(' in call_to or '->startActivityFromFragment(' in call_to or '->startIntentSender' in call_to:
                    if args[0] in received_intents:
                        intent_info[args[0]]['started'] = True
                        intent_info[args[0]]['Target'] = 'Activity'
                    elif args[0] in intent_registers:
                        intent_info[args[0]]['started'] = True
                        called_intents.append(intent_info[args[0]])
                        intent_info[args[0]]['Target'] = 'Activity'
                
                elif 'startActivities([Landroid/content/Intent;' in call_to:
                    if args[0] in received_intents:
                        for index in intent_info[args[0]].keys():
                            intent_info[args[0]][index]['started'] = True
                            intent_info[args[0]][index]['Target'] = 'Activity'
                    elif args[0] in intent_registers:
                        for index in intent_info[args[0]].keys():
                            intent_info[args[0]][index]['started'] = True
                            intent_info[args[0]][index]['Target'] = 'Activity'
                            called_intents.append(intent_info[args[0]][index])
                        
                elif ('start' in call_to or 'bind' in call_to) and 'Service(Landroid/content/Intent;' in call_to:
                    if args[0] in received_intents:
                        intent_info[args[0]]['started'] = True
                        intent_info[args[0]]['Target'] = 'Service'
                    elif args[0] in intent_registers:
                        intent_info[args[0]]['started'] = True
                        called_intents.append(intent_info[args[0]])
                        intent_info[args[0]]['Target'] = 'Service'
                
                # Parse the bytecode to trace register values
                elif reg in intent_registers and 'invoke-static' not in inst_name:  #Intent operation
                    operation = []
                    idx_init = call_to.index(';->') + 3
                    
                    # Initialization of intent with arguments. Parse possible arguments (properties)
                    if 'Landroid/content/Intent;-><init>(' in call_to:
                        intent_info[reg] = {}
                        
                        idx_init = call_to.index('(') + 1 
                        idx_fin = call_to.index(')')
                        # Get function arguments
                        function_arg_dtypes = call_to[idx_init+1:idx_fin].split(' ') 
                        
                        for arg_type in function_arg_dtypes:
                            if 'String' in arg_type:
                                operation.append('Action')
                            elif 'Context' in  arg_type:
                                operation.append('Package')
                            elif 'Class' in arg_type:
                                operation.append('Class')
                            elif 'Uri' in arg_type:
                                operation.append('Data')
                            elif len(arg_type)>0:  # Is a copy constructor
                                try:
                                    info = intent_info[args[0]]
                                except:
                                    info = {}
                                    intent_info[args[0]] = {}
                                    if args[0] not in intent_registers: intent_registers.append(args[0])
                                intent_info[reg] = info
                                break
                    
                    # Set properties of intents: Extract the property that is being set or added
                    elif call_to[idx_init:idx_init+3] in ['set', 'add']:
                        idx_fin = call_to.index('(')
                        operation = call_to[idx_init+3:idx_fin]
                        operation = operation.split('And')
                                                
                        if 'Class' in operation[0]:
                            operation = ['Package', 'Class']
                        
                    
                    # Update properties of intents based on arguments only if it is a valid operation
                    if len(operation)>0:
                        info = intent_info[reg]
                        values = []
                        
                        # Parse argument values
                        n = 0
                        for arg_reg in args:
                            try:
                                #print(operation[n], arg_reg)
                                values.append(registers[arg_reg])
                            except KeyError:
                                idx_ini = call_to.index('(')
                                new_fin = call_to.index(')')
                                # Get function arguments
                                function_arg_dtypes = call_to[idx_ini+1:new_fin].split(' ') 
                                arg_type = function_arg_dtypes[n]
                                values.append(arg_type)
                            n += 1
                        
                        # Set operation values
                        if len(operation)==len(values):
                            n = 0
                            for op in operation:
                                info[op] = values[n]
                                n += 1
                        else:
                            operation = 'And'.join(operation)
                            info[operation] = values
                            
                        #Update info of the intent
                        intent_info[reg] = info
                else:
                    # An intent is passed as argument to other function
                    # We count the number of intent registers that we have to trace
                    args_int = []
                    for arg in args:
                        # Is a call to a method that receives an intent as argument?
                        if arg in intent_registers:
                            args_int.append(arg)
                    if len(args_int)>0:
                        code = self.getMethodCodeObject(call_to.replace(' ', ''))
                        if code:
                            #print('Analyzing:', call_to)
                            res = self._taintIntentCode(code, return_finished=False, intent_as_argument=len(args_int), n_rec_calls=n_rec_calls+1)
                            if len(res)>0:
                                for n in range(0,len(args_int)):  #Update intents info with results
                                    try:
                                        intent_info[args_int[n]] =  {**intent_info[args_int[n]], **res[n]}
                                        # Add to finished intents
                                        if intent_info[args_int[n]]['started'] == True:
                                            called_intents.append(intent_info[args_int[n]])
                                    except (IndexError,KeyError):
                                        #print(inst_name, operands)
                                        #traceback.print_exc()
                                        pass
                            #print(intent_info)
                            
            # NEW-INSTANCE instructions
            elif re.search('^new-instance v([0-9]+), (L(?:.*);)$', current_instruction):
                rematch = re.search('^new-instance v([0-9]+), (L(?:.*);)$', current_instruction)
                reg = rematch.groups()[0]
                dtype = rematch.groups()[1]
                last_return_type = ''
                if dtype=='Landroid/content/Intent;':
                    if reg not in intent_registers: intent_registers.append(reg)
                    intent_info[reg] = {}
                else:
                    # Remove the register if the op produces a new instance
                    if reg in intent_registers: intent_registers.remove(reg)
                    registers[reg] = dtype  # Get only the string or class name
            # CONST INSTRUCTION
            elif re.search('^const(?:\/4|\/16|\/high16|-wide(?:\/16|\/32)|-wide\/high16|)? v([0-9]+), \+?(-?[0-9]+(?:\.[0-9]+)?)$', current_instruction):
                rematch = re.search('^const(?:\/4|\/16|\/high16|-wide(?:\/16|\/32)|-wide\/high16|)? v([0-9]+), \+?(-?[0-9]+(?:\.[0-9]+)?)$', current_instruction)
                reg = rematch.groups()[0]
                value = rematch.groups()[1]
                last_return_type = ''
                registers[reg] = dtype
                if reg in intent_registers: intent_registers.remove(reg)
            # Array declaration
            elif 'new-array' in inst_name:
                last_return_type = ''
                info = []
                try:
                    size_reg = operands[1:-1][0]  # size of the array
                    dtype = operands[-1][-1]  # Type
                    size = registers[size_reg]
                except (KeyError, IndexError):
                    last_reg = None
                    continue
                if type(size) is not int: 
                    last_reg = None
                    continue
                if 'Landroid/content/Intent;' in dtype:  #Is an intent
                    for e in range(size%100):
                        info.append({})
                    if reg not in intent_registers: intent_registers.append(reg)
                    intent_info[reg] = info
                else:
                    for e in range(size%100):
                        info.append(None)
                    if reg in intent_registers: intent_registers.remove(reg)
                    registers[reg] = info  # Another dtype
            
            # Get op
            elif 'aget' in inst_name:
                rematch = re.search('^aget(?:-wide|-object|-boolean|-byte|-char|-short|) v([0-9]+), v([0-9]+), v([0-9]+)$', current_instruction)
                reg_dest = rematch.groups()[0]
                array_reg = rematch.groups()[1]
                index = rematch.groups()[2]
                last_return_type = ''
                if array_reg in intent_registers:  # Copy from intent Array
                    try:
                        info = intent_info[array_reg][registers[index]]
                        intent_info[reg_dest] = info
                    except:
                        intent_info[reg_dest] = {}
                    if reg_dest not in intent_registers: intent_registers.append(reg_dest)

                else:
                    if reg_dest in intent_registers: intent_registers.remove(reg_dest)
                    try:
                        info = registers[array_reg][registers[index]]
                        registers[reg_dest] = info
                    except:
                        last_reg = None
                        continue
                
            elif 'sget' in inst_name:
                last_return_type = ''
                rematch = re.search('^sget(?:-wide|-object|-boolean|-byte|-char|-short|) (v[0-9]+), (L(?:.*);)$', current_instruction)
                reg_dest = rematch.groups()[0]
                field = rematch.groups()[1].split(' ')
                field_name = field[0]
                field_dtype = field[1]
                
                if field_name in objects.keys():
                    info = objects[field_name]
                elif field_dtype == 'Landroid/content/Intent;':
                    info = {}
                else:
                    info = field_dtype
                    objects[field_name] = field_dtype
                
                # If field is an Intent
                if field_dtype == 'Landroid/content/Intent;':
                    if reg_dest not in intent_registers: intent_registers.append(reg_dest)
                    intent_info[reg_dest] = info
                # Another object
                else: 
                    if reg_dest in intent_registers: intent_registers.remove(reg_dest)
                    registers[reg_dest] = info
                
            elif 'iget' in inst_name:
                last_return_type = ''
                rematch = re.search('^iget(?:-wide|-object|-boolean|-byte|-char|-short|) v([0-9]+), v([0-9]+), (L(?:.*);)$', current_instruction)
                reg_dest = rematch.groups()[0]
                obj_reg = rematch.groups()[1]
                field = rematch.groups()[2].split(' ')
                field_name = field[0]
                field_dtype = field[1]

                # Fill this method                

                if field_name in objects.keys():
                    info = objects[field_name]
                elif field_dtype == 'Landroid/content/Intent;':
                    info = {}
                else:
                    info = field_dtype
                    objects[field_name] = field_dtype
                
                # If field is an Intent
                if field_dtype == 'Landroid/content/Intent;':
                    if reg_dest not in intent_registers: intent_registers.append(reg_dest)
                    intent_info[reg_dest] = info
                # Another object
                else: 
                    if reg_dest in intent_registers: intent_registers.remove(reg_dest)
                    registers[reg_dest] = info

            # Put op
            elif 'sput' in inst_name:  # Static field put
                last_return_type = ''
                rematch = re.search('^sget(?:-wide|-object|-boolean|-byte|-char|-short|) (v[0-9]+), (L(?:.*);)$', current_instruction)
                reg_source = rematch.groups()[0]
                field = rematch.groups()[1].split(' ')
                field_name = field[0]
                field_dtype = field[1]
                
                try:
                    if reg_source in intent_registers:
                        info = intent_info[reg_source]
                    elif field_dtype == 'Landroid/content/Intent;':
                        info = {}
                    else:
                        info = registers[reg_source]
                except:
                    info = field_dtype

                objects[field_name] = info
                            
            elif 'aput' in inst_name:  # Array put
                rematch = re.search('^aput(?:-wide|-object|-boolean|-byte|-char|-short|) v([0-9]+), v([0-9]+), v([0-9]+)$', current_instruction)
                reg_source = rematch.groups()[0]
                array_reg = rematch.groups()[1]
                index = rematch.groups()[2]
                last_return_type = ''
                if reg_source in intent_registers:  # Intent field
                    try:
                        info = intent_info[reg_source]
                        intent_info[array_reg] = info
                    except:
                        intent_info[array_reg] = {}
                    if array_reg not in intent_registers: intent_registers.append(array_reg)
                else:
                    try:
                        if array_reg in intent_registers: intent_registers.remove(array_reg)
                        registers[array_reg] = registers[reg_source]
                    except:
                        continue
                
            elif 'iput' in inst_name:  # Instance field put
                last_return_type = ''
                rematch = re.search('^iput(?:-wide|-object|-boolean|-byte|-char|-short|) v([0-9]+), v([0-9]+), (L(?:.*);)$', current_instruction)
                reg_source = rematch.groups()[0]
                obj_reg = rematch.groups()[1]
                field = rematch.groups()[2].split(' ')
                field_name = field[0]
                field_dtype = field[1]
                try:
                    if reg_source in intent_registers:
                        info = intent_info[reg_source]
                    elif field_dtype == 'Landroid/content/Intent;':
                        info = {}
                    else:
                        info = registers[reg_source]
                except:
                    info = field_dtype

                objects[field_name] = info
            
            elif 'add' in inst_name:
                b = operands[-1]
                if 'addr' in inst_name:
                    try:
                        registers[reg] += registers[b]
                    except:
                        registers[reg] = 0
                    
                elif 'lit' in inst_name:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] + b[1]
                    except:
                        registers[reg] = 0
                else:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] + registers[b]
                    except:
                        registers[reg] = 0
            elif 'sub' in inst_name:
                b = operands[-1]
                if 'addr' in inst_name:
                    try:
                        registers[reg] -= registers[b]
                    except:
                        registers[reg] = 0
                    
                elif 'lit' in inst_name:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] - b[1]
                    except:
                        registers[reg] = 0
                else:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] - registers[b]
                    except:
                        registers[reg] = 0
            
            elif 'mul' in inst_name:
                b = operands[-1]
                if 'addr' in inst_name:
                    try:
                        registers[reg] *= registers[b]
                    except:
                        registers[reg] = 0
                    
                elif 'lit' in inst_name:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] * b[1]
                    except:
                        registers[reg] = 0
                else:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] * registers[b]
                    except:
                        registers[reg] = 0
                        
            elif 'div' in inst_name:
                b = operands[-1]
                if 'addr' in inst_name:
                    try:
                        registers[reg] /= registers[b]
                    except:
                        registers[reg] = 0
                    
                elif 'lit' in inst_name:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] / b[1]
                    except:
                        registers[reg] = 0
                else:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] / registers[b]
                    except:
                        registers[reg] = 0
                        registers[b] = 0
            
            elif 'rem' in inst_name:
                b = operands[-1]
                if 'addr' in inst_name:
                    try:
                        registers[reg] %= registers[b]
                    except:
                        registers[reg] = 0
                    
                elif 'lit' in inst_name:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] % b
                    except:
                        registers[reg] = 0
                else:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] % registers[b]
                    except:
                        registers[reg] = 0
            
            elif 'and' in inst_name:
                b = operands[-1]
                if 'addr' in inst_name:
                    try:
                        registers[reg] &= registers[b]
                    except:
                        registers[reg] = 0
                    
                elif 'lit' in inst_name:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] & b
                    except:
                        registers[reg] = 0
                else:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] & registers[b]
                    except:
                        registers[reg] = 0
            
            elif 'or' in inst_name:
                b = operands[-1]
                if 'addr' in inst_name:
                    try:
                        registers[reg] |= registers[b]
                    except:
                        registers[reg] = 0
                    
                elif 'lit' in inst_name:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] | b
                    except:
                        registers[reg] = 0
                else:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] | registers[b]
                    except:
                        registers[reg] = 0
            
            elif 'xor' in inst_name:
                b = operands[-1]
                if 'addr' in inst_name:
                    try:
                        registers[reg] ^= registers[b]
                    except:
                        registers[reg] = 0
                    
                elif 'lit' in inst_name:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] ^ b
                    except:
                        registers[reg] = 0
                else:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] ^ registers[b]
                    except:
                        registers[reg] = 0
            elif 'shl' in inst_name:
                b = operands[-1]
                if 'addr' in inst_name:
                    try:
                        registers[reg] <<= registers[b]
                    except:
                        registers[reg] = 0
                    
                elif 'lit' in inst_name:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] << b
                    except:
                        registers[reg] = 0
                else:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] << registers[b]
                    except:
                        registers[reg] = 0
            
            elif 'shr' in inst_name:
                b = operands[-1]
                if 'addr' in inst_name:
                    try:
                        registers[reg] >>= registers[b]
                    except:
                        registers[reg] = 0
                    
                elif 'lit' in inst_name:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] >> b
                    except:
                        registers[reg] = 0
                else:
                    try:
                        registers[reg] = registers[operands[1:-1][0]] >> registers[b]
                    except:
                        registers[reg] = 0
            elif 'ushr' in inst_name:
                b = operands[-1]
                if 'addr' in inst_name:
                    try:
                        registers[reg] =  abs(registers[reg] >> registers[b])
                    except:
                        registers[reg] = 0
                    
                elif 'lit' in inst_name:
                    try:
                        registers[reg] = abs(registers[operands[1:-1][0]] >> b)
                    except:
                        registers[reg] = 0
                else:
                    try:
                        registers[reg] = abs(registers[operands[1:-1][0]] >> registers[b])
                    except:
                        registers[reg] = 0
            
            else:  # Unimplemented opcodes
                #print('Unimplemented', inst_name, operands)
                last_return_type = ''
                        

            last_reg = reg
        
        
        del instructions 
        del registers
        del objects
        del intent_registers

        
        if return_finished:
            return called_intents
        return [intent_info[reg] for reg in received_intents]
    
    
    def _getCallersToMethodReturnType (self, returntype, callclass='.*'):
        """
        Finds all the functions that have a call to a method returning returntype in their code

        Parameters
        ----------
        returntype : string
            Return type can be a regex expression
        callclass : string
            The name of the class for the method returning returntype. It can be a regex
        Returns
        -------
        cremethods : List
            List of tuples with (callername, returningmethodname)

        """
        cremethods = []
        for method in self.__dex_analysis.get_methods():
            m1 = method.get_method()
            if (re.search('\)'+returntype, m1.get_descriptor().replace(' ', ''))):
                for c, m, o in method.get_xref_from():
                    name = m.class_name + "->" +m.name + m.get_descriptor().replace(' ', '')
                    # Only analyze internal intents
                    if m.class_name not in self.__APIclasses and name not in cremethods and re.search(callclass, m1.class_name):
                        cremethods.append((name,m1.class_name+'->'+m1.name))
        return cremethods
    
    
    def _getCallersForMethod (self, classname, methodname):
        """
        Finds all the functions that have a call to a method

        Parameters
        ----------
        classname : string
            Can be a regex expression
        methodname : string
            Can be a regex expression
        Returns
        -------
        invekers : List
            List of tuples for callers and offset of the call. The caller is in format:
                Lpackage/class;->method(descriptortypes)Lreturnpackage/returnclass;
                
                
        
	Cryptographic API:
		javax.crypto.*
		java.security.*

	Reflection API:
		java.lang.reflect.*

	Dynamic Code loading API:
		java.lang.ClassLoader
		java.security.SecureClassLoader
		java.net.URLClassLoader
		junit.util.TestSuiteLoader
		java.util.ServiceLoader
		dalvik.system.*

	Command Execution API:
		java.lang.ProcessBuilder
		java.lang.Runtime
	
        
        """
        invokers = []
        for method in self.__dex_analysis.get_methods():
            m1 = method.get_method()
            if (re.search(classname, m1.class_name) and re.search(methodname, m1.name)):
                for c, m, offset in method.get_xref_from():
                    name = m.class_name + "->" +m.name + m.get_descriptor().replace(' ', '')
                    # Only analyze internal intents
                    if m.class_name not in self.__APIclasses and name not in invokers:
                        invokers.append((name, offset))
        return invokers    
    
    
    def _getClassForDynamicLoad (self, method, reg, nline, called=None, recursion=3 ):
        if recursion<=0: return ['unresolved_loaded_class']
        code = self.getMethodCode(method)
        codelines = code.split('\n')
        # search where the method is called
        if called and nline<0:
            nline = len(codelines)-1
            while nline>=0:
                line = codelines[nline]
                #print(line)
                if called in line:
                    break
                nline -= 1
        
            if nline<0: 
                #print("not found call to ", method)
                return ['unresolved_loaded_class']
        
        # Recursive search to get the method name
        while nline>=0:
            line = codelines[nline]
            # returns a reflect type method
            line_assign = re.search(''+reg+' = \"([\w\_\.\$\d]+)\"', line)
            if line_assign!=None:
                #print(line)
                classname = line_assign.groups()[0]
                return [classname]
            elif re.search(''+reg+' = ([\w\_\.\$\d]+)$', line):
                #print(line)
                line_assign = re.search(''+reg+' = ([\w\_\.\$\d]+)$', line)
                classname = line_assign.groups()[0]
                return [classname]
            elif re.search(''+reg+' = (p[0-9]+|v[0-9]+\_[0-9]+)\.([\w\_\.\$\d]+)', line):
                #print(line)
                line_assign = re.search(''+reg+' = (p[0-9]+|v[0-9]+\_[0-9]+)\.([\w\_\.\$\d]+)', line)
                reg = line_assign.groups()[0]
                #print('Resolving register', reg)
                #return [classname]
            nline -= 1
        if nline<0:
            out = []
            try:
                for caller in self._G.predecessors(method):
                    #if caller.split('(')[1].split(')')[0]
                    #method_reg = 
                    out += self._getClassForDynamicLoad(caller, 'v[0-9]+\_[0-9]+', nline, called = method, recursion=recursion-1)
                return out
            except:
                #print('Exception (not found predecessors)', method)
                pass
        return ['unresolved_loaded_class']            
    
    
    def getDynamicLoads (self):
        """
        This method tries to resolve Dynamic Code Loads. All loads return a java.lang.Class
        
        Returns
        -------
        dict 
            A dictionary containing the class;->method_name solved, class solved and unresolved apis, value is the count that an api is loaded

        """
        calls = {}
        if not self._G: self.getCallGraph()
        invokers = self._getCallersForMethod('(Class|Service|TestSuite)Loader;', '(load|resolve|define)')
        #invokers = list(set(invokers))
        #print('Found', len(invokers), 'invokers')
        for call,_ in invokers:
            code = self.getMethodCode(call)
            #print(call)
            codelines = code.split('\n')
            nmaxline = 0
            for line in codelines:
                if re.search('(load|define|resolve)',line):  # Call found
                    #print(line)
                    # Check if name is directly provided
                    class_name = re.search('(load|define|resolve)[\w\_\$\d]+\(\"([\w\_\.\$\d]+)\"', line)
                    if class_name!=None:
                        #print('Found classname', class_name.group(), line)
                        classname = class_name.groups()[1]
                        try:
                            calls[classname] += 1
                        except:
                            calls[classname] = 1                    
                    else:
                        reg = re.search('(load|define|resolve)[\w\_\$\d]+\(([\w\_\.\$\d]+)', line)
                        if reg!=None:
                            reg = reg.groups()[1]
                            #print('Found register', reg)
                            nmaxline -= 1
                            if 'class' in reg:
                                try:
                                    calls[reg] += 1
                                except:
                                    calls[reg] = 1
                            else: break
                    
                nmaxline += 1
            if nmaxline>=len(codelines) or nmaxline<0: 
                continue  # invoke from register not found
            else:  # Find the register value
                classcalls = self._getClassForDynamicLoad(call, reg, nmaxline)
                for clcall in classcalls:
                    try:
                        calls[clcall] += 1
                    except:
                        calls[clcall] = 1           
        #print(calls)
        return calls
    
    
    def _resolveClassForReflectiveCall (self, method, method_reg, mname, nline=-1, called=None, recursion=3):
        if recursion<=0: return ['unresolved_class;->'+mname]
        code = self.getMethodCode(method)
        codelines = code.split('\n')
        
        # search where the method is called
        if called and nline<0:
            nline = len(codelines)-1
            while nline>=0:
                line = codelines[nline]
                #print(line)
                if called in line:
                    break
                nline -= 1
        
            if nline<0: 
                #print("not found call to ", method)
                return ['unresolved_class;->'+mname]
        
        classname = ""
        while nline>=0:
            line = codelines[nline]
            #print(code)
            line_assign = re.search('Class '+method_reg+' = ', line)
            if line_assign!=None:
                #print(line)
                classname = re.search('([\w\_\.\$\d]+)\.class', line)
                #method_name = re.search('\(\".*\"', line)
                if classname!=None:
                    classname = 'L'+classname.groups()[0].replace('.', '/')
                    call = classname + ';->' + mname
                    #print('Found:', call)
                    return [call]
                else:
                    class_name = re.search('\(\".*\"', line)
                    if class_name!=None:
                        #print(method_name.group())
                        classname = class_name.group().split('"')[1]  # get the name by removing the call
                        call = classname + ';->' + mname
                        #print('Found:', call)
                        return [call]
            nline -= 1
        if nline<0:
            out = []
            try:
                for caller in self._G.predecessors(method):
                    #if caller.split('(')[1].split(')')[0]
                    #method_reg = 
                    out += self._resolveClassForReflectiveCall(caller, 'v[0-9]+\_[0-9]+', mname, called = method, recursion=recursion-1)
                return out
            except:
                #print('Exception (not found predecessors)', method)
                pass
        return ['unresolved_class;->'+mname]
    
    
    def _resolveMethodForReflectiveCall (self, method, method_reg, nline = -1, called=None, recursion=3):
        if recursion<=0: return ['unresolved_class;->unresolved_method']
        code = self.getMethodCode(method)
        codelines = code.split('\n')
        
        # search where the method is called
        if called and nline<0:
            nline = len(codelines)-1
            while nline>=0:
                line = codelines[nline]
                #print(line)
                if called in line:
                    break
                nline -= 1
            if nline<0: 
                #print("not found call to ", method)
                return ['unresolved_class;->unresolved_method']
        
        # Backtrace the code to search the name of the method
        mname = ''
        found = False
        while nline>=0:
            line = codelines[nline]
            # returns a reflect type method
            line_assign = re.search('reflect.[\w\_\.\$\d]+ '+method_reg+' = ', line)
            if line_assign!=None:
                method_name = re.search('\(\".*\"', line)
                if method_name!=None:  # Is a string
                    #print(method_name.group())
                    mname = method_name.group().split('"')[1]  # get the name by removing the called
                    method_reg = re.search(' ([\w\_\.\$\d]+){1}(\.[\w\_\.\$\d]+\()', line)
                    if method_reg!=None:
                        method_reg = method_reg.groups()[0]
                        #print(method_reg, type(method_reg), 'in line ', nmaxline, line)
                        nline -= 1
                        found = True
                else:  # is a type
                    #print(line)
                    mname = re.search('= ([\w\_\.\$\d]+)\.(([\w\_\$\d]+));', line)
                    if mname!=None and '= this.' not in mname.group():
                        #print(mname)
                        method_reg = mname.groups()[0]
                        mname = mname.groups()[1]
                        #print('Found:', method_reg, mname)
                        nline -= 1
                        found  = True
            
            if found:
                if '.' in method_reg:  # is a class name already
                    method_reg = method_reg.replace('.', '/')
                    call = 'L'+ method_reg + ';->' + mname
                    return [call]
                else:
                    return self._resolveClassForReflectiveCall(method, method_reg, mname, nline)
            nline -= 1
        if nline<0:
            out = []
            try:
                for caller in self._G.predecessors(method):
                    out += self._resolveMethodForReflectiveCall(caller, method_reg='v[0-9]+\_[0-9]+', nline=-1, called=method, recursion = recursion-1)
                return out
            except:
                #print('Exception (not found predecessors)', method)
                return ['unresolved_class;->unresolved_method']
        return [mname]
    
    
    def getReflectiveInvocations (self):
        """
        This method follows the approach of 
        
        Garcia et al. 2016. Obfuscation-Resilient Detection and Family Identification of Android Malware, ACM Trans on Soft Eng
        
        to resolve the relfective calls that may be present in the code of the app.
        It classifies the invocations into 3 categories:
            -Fully resolved: class and method name are obtained
            -Partially resolved: class name is obtained
            -Unresolved: neither class or method name are obtained

        Returns
        -------
        dict 
            A dictionary containing the class;->method_name solved, class solved and unresolved apis, value is the invocation count

        """
        calls = {}
        if not self._G: self.getCallGraph()
        invokers = self._getCallersForMethod('Ljava/lang/reflect/Method;', 'invoke')
        #invokers = list(set(invokers))
        #print('Found: ', len(invokers), 'invokers to method')
        mnames = []
        for call,_ in invokers:
            code = self.getMethodCode(call)
            #print(call)
            codelines = code.split('\n')
            nmaxline = 0
            method_reg = ""
            for line in codelines:
                method_reg = re.search('(v\d\_\d*){1}(\.invoke)', line)
                if method_reg!=None:
                    #print(line)
                    method_reg = method_reg.groups()[0]
                    #print(method_reg, type(method_reg), 'in line ', nmaxline, line)
                    nmaxline -= 1
                    break
                nmaxline += 1
            if nmaxline>=len(codelines) or nmaxline<0: 
                continue  # invoke from register not found
            # Recursive search to get the method name
            mnames += self._resolveMethodForReflectiveCall(call, method_reg, nmaxline)
            
        #print(calls)
        for mname in mnames:
            try:
                calls[mname] += 1
            except:
                calls[mname] = 1
        #print(calls)
        return calls
    
    
    def getIntentFiltersInCode (self):
        """
        Performs taint analysis of methods that instantiate Intent objects to 
        extract implicit and explicit intent calls and their attributes.

        Returns
        -------
        intents : List []
            A list of dictionary items each with an intent description
        
        broadcast : List []
            A list of broadcast receiver items related to intent filters
        """
        intents = []
        broadcast = []
        cfg = self.getCallGraph()
        callers = self.getIntentFilterCallers(only_initializers=True)
        for call in callers:
            if cfg.nodes[call]['isolated']==False or cfg.nodes[call]['entrypoint']==True:
                try:
                    intn, broad = self._taintIntentFilterCode(self.getMethodCodeObject(call))
                    intents.extend(intn)
                    broadcast.extend(broad)
                except:
                    #traceback.print_exc()
                    continue
        del cfg, callers
        return intents, broadcast
        
            
    
    def getIntentsInCode (self):
        """
        Performs taint analysis of methods that instantiate Intent objects to 
        extract implicit and explicit intent calls and their attributes.

        Returns
        -------
        intents : List []
            A list of dictionary items each with an intent description

        """
        intents = []
        cfg = self.getCallGraph()
        _, broadcast = self.getIntentFiltersInCode()
        callers = self.getIntentCallers(only_initializers=True)
        for call in callers:
            class_name = call.split('->')[0]
            # Check only if the function has an entry point (Broadcast, entrypoint of the app, another function...)
            if cfg.nodes[call]['isolated']==False or cfg.nodes[call]['entrypoint']==True or class_name in broadcast:
                try:
                    intents.extend(self._taintIntentCode(self.getMethodCodeObject(call)))
                except:
                    #traceback.print_exc()
                    continue
        del cfg, callers, broadcast
        return intents
    
    
    def getIntentActionsInCode (self):
        """
        Returns the ACTION values found for explicit and implicit intents in the code

        Returns
        -------
        found : List []
            Intent.ACTION strings.

        """
    
        intents = self.getIntentsInCode()
        found = []              
        for intent in intents:
            try:
                s = intent['Action']
            except KeyError:
                continue
            found.append(s)
        del intents
        return found
    
    
    def filterStringsByMethod (self, class_filters=[".*"], method_filters=[".*"]):
        """
        Returns a dictionary with the strings used in the methods that comply the 
        regex filter
        
        Parameters
        ----------
        class_filters : List []
            List containing the regular expresions used to filter the class of methods in the APK
            
        method_filters : List []
            List containing the regular expresions used to filter the methods in the APK
            
        :rtype: dict
            Returns a dictionary with strings as keys and the count of references
            as the value
        """
        found = []              
        for string in self.__dex_analysis.get_strings():
            s = string.get_value()
            for _, ref_method  in string.get_xref_from():
                for c_filt in class_filters:
                    if re.search(c_filt, ref_method.class_name):
                        for m_filt in method_filters:
                            if re.search(m_filt, ref_method.name):
                                found.append(s)
            
        return found
    
    
    def getSourcesAndSinks (self):
        """
        Uses flowdroid to trace flows between sources and sinks in the code.
        
        
        Returns
        -------
        sourcesinks : dict
            keys are the sinks found and the value is a list with the different sources that reach each sink

        """
        out = self.__run_java_component(self.__thisPATH+'analysis/soot-infoflow-cmd-jar-with-dependencies.jar', ['-a', self.APK, '-p', self.__platformJarDir, '-s', self.__thisPATH+'analysis/sources_sinks.txt'], timeout=None)
        out = out.split('\n')
        n = 0
        sourcesinks = {}
        #print(out)
        while n<len(out):
            line = out[n]
            if '[main] INFO soot.jimple.infoflow.android.SetupApplication$InPlaceInfoflow - The sink ' in line:
                # get sink
                try:
                    splitted = line.split('<')
                    sink_class = splitted[1].split(' ')[0][:-1]
                    sink_ret = splitted[1].split(' ')[1]
                    sink_method = (splitted[1].split(' ')[2]).split('>')[0]
                    sink = sink_class + ';->' + sink_method + sink_ret
                except:
                    print('ERROR', line)
                    continue
                k = n+1
                sources = []
                while k<len(out):
                    line = out[k]
                    if '[main] INFO soot.jimple.infoflow.android.SetupApplication$InPlaceInfoflow - - ' in line:
                        # parse source
                        try:
                            splitted = line.split('<')
                            source_class = splitted[1].split(' ')[0][:-1]
                            source_ret = splitted[1].split(' ')[1]
                            source_method = (splitted[1].split(' ')[2]).split('>')[0]
                            source = source_class + ';->' + source_method + source_ret
                            sources.append(source)
                        except:
                            print('ERROR', line)
                            break
                        k += 1
                    else: break 
                # Add source and sinks
                try:
                    sourcesinks[sink] += sources
                except:
                    sourcesinks[sink] = sources
                n = k -1
            n += 1
        return sourcesinks
    
    
    def getCyclomaticComplexity (self):
        """
        Computes the cyclomatic complexity of the code. It is based on the CFG of the app

        Returns
        -------
        int - The cyclomatic complexity value

        """
        self.getControlFlowGraph(exclude_external_lib=True)
        cycomp = len(self._CFG.edges) - len(self._CFG.nodes) + 2*nx.number_connected_components(self._CFG)
        return cycomp
     
    
    def getInternalMethodCount (self):
        """
        Gets a dictionary with the internal classes found in the apk and their
        number of methods as value

        Returns
        -------
        internal : dict {}
            Dictionary containing as keys the name of internal classes and as
            values the number of methods of each class.

        """
        internal = {}
        for clase in self.__dex_analysis.get_internal_classes():
            metodos = clase.get_methods()
            internal[clase.name] = len(metodos)
        #print("Internal count: ", len(internal))
        #print("Total internal methods: ", sum(internal.values()))
        return internal
        
    
    def getExternalMethodCount (self):
        """
        Gets a dictionary with the external classes found in the apk and their
        number of methods as value

        Returns
        -------
        external : dict {}
            Dictionary containing as keys the name of external classes and as
            values the number of methods of each class.

        """
        external = {}
        for clase in self.__dex_analysis.get_external_classes():
            metodos = clase.get_methods()
            external[clase.name] = len(metodos)
        #print("External count: ", len(external))
        #print("Total external methods: ", sum(external.values()))
        return external
        
    
    
    def getStrings (self, regex_filter=[".*"], only_dex=True):
        """
        Returns a dictionary with the strings found in the APK that comply the 
        regex filter
        
        Parameters
        ----------
        regex_filter : List []
            List containing the regular expresions used to filter the Strings
            Example to filter http[s] urls:
                "^http[s]{0,1}:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)
        
        only_dex : boolean
            Default value is True. If set to True, do not extract the strings from other files inside the APK.
        
        :rtype: dict
            Returns a dictionary with strings as keys and the count of references
            as the value
        """
        # Strings inside the dex file
        strs = self.__dex_analysis.get_strings_analysis()
        found = {}
        #print('Found:', strs)
        for cadena in strs.keys():
            for regex in regex_filter:
                #print('Regex', regex)
                if re.search(regex, cadena):
                    #print('Match', cadena)
                    found[cadena] = len(strs[cadena].get_xref_from())
        #return found
        # The rest of files inside the APK
        if not only_dex:
            files = self.getFilesInformation()
            files = [(fn,ft.split(' ')[0])  for fn,ft,_ in files]
            dirtemp = tempfile.mkdtemp(dir='./') + '/'
            for fn,ft in files:
                if fn=='classes.dex': continue  # We have already analyzed this
                with zipfile.ZipFile(self.__APKinfo.filename,"r") as zipref:
                    zipref.extract(fn,dirtemp)
                if os.path.isdir(dirtemp + fn): continue
                with open(dirtemp + fn, 'rb') as fr:
                    for regex in regex_filter:
                        #print('Regex', regex)
                        pattern = re.compile(regex)
                        while True:
                            data = fr.read(1024*4)
                            if not data: break
                            strs = self.__get_strings_from_bytes(data)
                            #print('Found:', strs)
                            for cadena in strs:
                                for match in pattern.findall(cadena):
                                    #print('Match', cadena)
                                    try:
                                        found[match] += 1
                                    except:
                                        found[match] = 1
                            
                try:
                    shutil.rmtree(dirtemp)
                except:
                    pass
        return found


    def getOpcodeStatistics (self):
        """
        Returns the set of opcodes used and their frequency in the
        APK
                
        Returns
        -------
        dict {}
            A dictionary containing as keys the opcode identifiers found and 
            their quantity as the values.
        """
        return self.__collectOpcodeStats()


   
    def getCallerOpcodeSequences (self, depth=4):
        """
        Generate a set of opcode sequences for the methods that call to APIs.
        
        A list of opcode sequences (list) is returned [[]].
        
        
        Parameters
        ----------
        depth : int 
            indicates the depth (the maximum number of calls ending to an API) at which a method is added to the analysis. If <=0, unlimited depth, so the sequences of all methods are returned
        
        :rtype: [[]]
        """
        if depth>0:
            return self.__extractDepthNeighbors(depth)[0]
        return self.__extractAllMethodSequences()
    
    
    def getHwFeatures (self):
        """
        Return a list of all android:names found for the tag uses-feature
        in the AndroidManifest.xml

        :returns: list
        """
        return self.__APKinfo.get_features()
    
    
    def getFilesInformation (self):
        """
        Returns the name of the files and their type (identified using python-magic) inside the APK

        Returns
        -------
        :returns: list of (filename,file_magic_type,crc)

        """
        return self.__APKinfo.get_files_information()
    
    
    def getBinaryFilesAnalysis (self):
        """
        Analyzes the ELF binaries inside the APK and returns their imported syscalls

        Returns
        -------
        dict
            A dictionary with syscalls imported as keys and the number of imports as values

        """
        files = self.getFilesInformation()
        files = [(fn,ft.split(' ')[0])  for fn,ft,_ in files]
        calls = []
        dirtemp = tempfile.mkdtemp(dir='./') + '/'
        calls = []
        symbols = {}
        for fn,ft in files:
            if ft=='ELF':
                with zipfile.ZipFile(self.__APKinfo.filename,"r") as zipref:
                    zipref.extract(fn,dirtemp)
                    binary = lief.parse(dirtemp + fn)
                    calls += [symbol.name for symbol in binary.dynamic_symbols if symbol.is_function and symbol.imported]
            # TODO Should we analyze jar files?
        try:
            shutil.rmtree(dirtemp)
        except:
            pass
        # Count symbols
        for call in calls:
            try:
                symbols[call] += 1
            except:
                symbols[call] = 1
        
        return symbols
    
    
    def getDexSize (self):
        """
        Returns the size (in bytes) of the DEX file of the APK (smali file)

        Returns
        -------
        int:  The size in bytes of the dex file

        """
        return sum([len(dex) for dex in self.__APKinfo.get_all_dex()])
    
    
    def getIntentFiltersInManifest (self):
        """
        Return a dictionary of all intents found for activity, service and broadcast
        receivers present in the AndroidManifest.xml

        :returns: dict
        """
        intents = {}
        # Androguard bug here with some apks
        try:
            intents['Activity'] = []
            for parent in self.__APKinfo.get_activities():
                intents['Activity'].append( self.__APKinfo.get_intent_filters("activity", parent))
        except:
            pass
        try:
            intents['Service'] = []
            for parent in self.__APKinfo.get_services():
                intents['Service'].append(self.__APKinfo.get_intent_filters("service", parent))
        except:
            pass
        try:
            intents['BroadcastReceiver'] = []
            for parent in self.__APKinfo.get_receivers():
                intents['BroadcastReceiver'].append(self.__APKinfo.get_intent_filters("receiver", parent))
        except:
            pass
        return intents
    
    
    def getIntentFilterActionsInManifest (self):
        """
        Return a list of all intents actions found for activity, service and 
        broadcast receivers present in the AndroidManifest.xml

        :returns: List []
        """
        intents = self.getIntentFiltersInManifest()
        actions = []
        for key in intents.keys():
            for el in intents[key]:
                try:
                    actions.extend(el['action'])
                except KeyError:
                    pass
        return actions
    
    
    def getAppComponents (self):
        """
        Returns entry points found in the manifiest of the APK, including
        activities, services, broadcast receivers and content providers
        
        :rtype: Dict {} 
            A dictionary containing Activity, Service, BroadcastReceiver and 
            Provider elements found
        """
        if not self.__entry_points:
            self.__searchEntryPoints()
        return self.__entry_points
    

    def getRestrictedAPImethodsUsage (self, APIlist=[], exclude_list = True ):
        """
        Get the filtered set of API methods found during the analysis of the APK and their usage.
        
        Parameters
        ----------
        APIlist : List []
            Default=None. List of strings containing the names of the APIs to consider in the analysis. The format
            of the list changes depending on the value of use_method_descriptors.
            The format is smali:
                Lpackage/name/ObjectName;->MethodName(III)Z
            If method descriptors flag is not set, format is:
                Lpackage/name/ObjectName;->MethodName
            Elements of the list can be regular expressions, to match smali format.
        exclude_list : boolean
            Sets the behavior of the filtering of APIs. If True, APIlist elements are excluded. If False, only elements
            in APIlist are included in the analysis
        
        Returns
        -------
        dict {}
            A dictionary containing as keys the names of the API methods found (in smali format) and the number of callers as values.
            If an APIlist has been provided, the filtered list of API usages is returned.
            Otherwise, the full list of API methods is returned
            
            Smali format:
                Lpackage/name/ObjectName;->MethodName(III)Z
        """
        restricted_apis = {}
        if not self.__api_usages:
            self.getAPImethodsUsage()
        for api in self.__api_usages:
            if not exclude_list:
                for filt_api in APIlist:
                    if re.search(filt_api,api):
                        restricted_apis[api] = self.__api_usages[api]
        return restricted_apis

        
    def getAPImethodsUsage (self, no_external = True):
        """
        Get the set of API methods found during the analysis of the APK and their usage.
        
        Parameters
        ----------
        no_external : boolean
            If True, does not follow external lib nodes

        Returns
        -------
        dict {}
            A dictionary containing as keys the names of API classes found (in smali format) and as value a dictionary with method name as key and number of callers as value.
            
            Smali format:
                Lpackage/name/ObjectName;->MethodName(III)Z

        """
        # We need the call graph
        self.getCallGraph( no_external)
        return self.__api_usages
    
    
    def getAPIs (self, no_external=True):
        """
        Get the set of API methods found during the analysis of the APK and their usage.
        
        Parameters
        ----------
        no_external : boolean
            If True, does not follow external lib nodes

        Returns
        -------
        dict {}
            A dictionary containing as keys the names of API methods found (in smali format) and as value the number of callers.
            
            Smali format:
                Lpackage/name/ObjectName;->MethodName(III)Z

        """
        if not self.__api_usages:
            self.getCallGraph( no_external)
        apis = {}
        for apiClass in self.__api_usages:
            for apiMethod in self.__api_usages[apiClass]:
                apis[apiClass+'->'+apiMethod] = self.__api_usages[apiClass][apiMethod]
        return apis
    
    
    def getIsolatedNodes (self):
        """
        Returns the list of isolated nodes found in the call graph (do not have
        input edges).
        
        Returns
        -------
        List []
            A list containing the methods found.

        """
        if not self.__found_isolated:
            self.getCallGraph()
        return self.__found_isolated
            
        
        
    def getEntryPoints (self):
        """
        Get the list of entry point methods found during the graph construction.
        An entry point is a callback method pertaining to any of the classes (Activities, Services
        and Broadcast Receivers) declared in the manifest which is isolated (does not have
        entry edges).
        
        Returns
        -------
        List []
            A list containing the methods found.

        """
        if not self.__found_entry_p:
            self.getCallGraph()
        return self.__found_entry_p
    
    
    def getLibraries (self):
        """
        Returns the list of libraries found in the manifest.xml
        
        Returns
        -------
        List []
            A list containing the elements found.

        """
        return self.__APKinfo.get_libraries()

    
    def getActivities (self):
        """
        Returns the list of Activities found in the manifest.xml
        
        Returns
        -------
        List []
            A list containing the elements found.

        """
        aux = self.__APKinfo.get_activities()
        return [("L"+clase.replace('.','/')+";").replace("//","/") for clase in aux]


    def getServices (self):
        """
        Returns the list of Services found in the manifest.xml
        
        Returns
        -------
        List []
            A list containing the elements found.

        """
        aux = self.__APKinfo.get_services()
        return [("L"+clase.replace('.','/')+";").replace("//","/") for clase in aux]
    
    
    def getBroadcastReceivers (self):
        """
        Returns the list of Broadcast Receivers found in the manifest.xml
        
        Returns
        -------
        List []
            A list containing the elements found.

        """
        aux = self.__APKinfo.get_receivers()
        return [("L"+clase.replace('.','/')+";").replace("//","/") for clase in aux]
    
    
    def getContentProviders (self):
        """
        Returns the list of Content Providers found in the manifest.xml
        
        Returns
        -------
        List []
            A list containing the methods found.

        """
        aux = self.__APKinfo.get_providers()
        return [("L"+clase.replace('.','/')+";").replace("//","/") for clase in aux]
    
    
    def getDeclaredPermissions (self):
        """
        Get the list of <uses-permission> tags in the androidmanifest.xml file
        
        Returns
        -------
        List []
            A list containing the permissions found.

        """
        return list(set(self.__APKinfo.get_permissions()))
    
    
    def getManifestFeatures (self):
        """
        Get the list of:
            Hardware components <uses-feature>
            Permissions <uses-permission>
            App components <service>, <activity>, <provider>, <receiver>
            Intents actions of <intent-filter>
            

        Returns
        -------
        List []

        """
        app_components = [item for l in list(self.getAppComponents().values()) for item in l]
        return self.getHwFeatures() + self.getDeclaredPermissions() + app_components + self.getIntentFilterActionsInManifest()
        
    
    def getAPIclasses (self):
        return self.__APIclasses
    
    
    def getAPKinfo (self):
        self.__APKinfo.get
    
    
    def getRequestedPermissionsWithPScoutMappings (self):
        """
        Get the list of requested permissions according to the api-permission
        mappings of PScout
                
        Returns
        -------
        List []
            A list containing the permissions dependencies found.
        Dict {}
            A dict containing as keys the API_calls that require any permission
            and their number of ocurrences as value.

        """
        if not self._PScoutMapper:
            return None, None
        req_perms = []
        apis = {}
        if not self.__api_usages:
            self.getAPImethodsUsage()            
        for apiClass in self.__api_usages:
            for apiMethod in self.__api_usages[apiClass]:
                perms = self._PScoutMapper.checkAPI(apiClass+'->'+apiMethod)
                if len(perms)>0:
                    apis[apiClass+'->'+apiMethod] = self.__api_usages[apiClass][apiMethod]
                    req_perms.extend(perms)
                
        return list(set(req_perms)), apis              
    
    
    def getRequestedPermissions (self):
        """
        Get the list of requested permissions according to the api-permission
        mappings of Androguard
        
        Unavailable until the new release of Androguard: (>3.3.5)
        
        Returns
        -------
        List []
            A list containing the permissions found.

        """
        perms = []
        for meth, perm in self.__dex_analysis.get_permissions(self.__platformCode):
            if perm not in perms:
                perms.append(perm)
        return perms
    
    
    def getCertificatesInfo (self):
        certificates = []
        cert = {}
        for x509cert in self.__APKinfo.get_certificates():
            cert['issuer'] = x509cert.issuer_serial.decode('utf-8', errors='ignore')
            cert['not_valid_before'] = str(x509cert.not_valid_before)
            cert['subject'] = x509cert.subject.human_friendly
            certificates.append(cert)
            cert = {}
        return certificates
        
    
   
    def getAnalysis (self):
        return self.__APKinfo, self.__VMAnalysis, self.__dex_analysis


    def _resetAnalysis (self):
              
        # Androguard variables
        self.__APKinfo = None
        self.__VMAnalysis = None
        self.__dex_analysis = None
        self.pkg = None
        self.APK = None
        self.__platformCode = None
        
        # Graph variables
        self._G = None  # Methods call Graph
        self._CG = None  # Classes call graph
        self._CFG = None  # The CFG of the APK
        self._API_CFG = None  # The API CFG of the APK
        self._method_CFGs = {}
        self._CFGs_edges = {}
        
        self.__api_usages = None
        self.__entry_points = {}  # Classes detected as entry points in the manifest
        self.__found_entry_p = None  # Elements found in the code that pertain to entry point classes
        self.__found_isolated = None  # Isolated node elements
                

    def processAPK (self, apkName):
        """
        

        Parameters
        ----------
        apkName : string
            Path to the apk to analyze

        Returns
        -------
        string
            The package name of the apk.

        """
        self._resetAnalysis()
        self.APK = apkName
        
        self.__APKinfo,self.__VMAnalysis,self.__dex_analysis = AnalyzeAPK(apkName)
        
        self.pkg = self.__APKinfo.get_package().replace('.', '/')
        self.__platformCode = self.__APKinfo.get_target_sdk_version()
        #print("Processing: ", self.pkg, "- SDK version:", self.__platformCode)
        if not self.__ignore_sdk_version:
            self.__APIclasses = []
            self.__readPlatformJar()
        self.__searchEntryPoints()
        return self.pkg
    

    # Process an APK in a more manual way
    def processAPK_alternative (self, apkName):
        """
        Alternative method for analysis. Contrary to Androguard, it examines every .dex file inside the APK

        Returns
        -------
        string
            The package name of the APK
        """
        # Get args
        self._resetAnalysis()
        self.APK = apkName
        self.__APKinfo,_,_ = AnalyzeAPK(self.APK)
        self.__unzip_APK(self.APK)
        dex_files = self.__analyzeDexDirectory()
        self.__analyzeDexFiles(dex_files)
        self.__cleanDexDirectory(dex_files)
        ## Usual analysis
        self.pkg = self.__APKinfo.get_package().replace('.', '/')
        self.__platformCode = self.__APKinfo.get_target_sdk_version()
        #print("Processing: ", self.pkg, "- SDK version:", self.__platformCode)
        if not self.__ignore_sdk_version:
            self.__APIclasses = []
            self.__readPlatformJar()
        self.__searchEntryPoints()
        return self.pkg
        


    
    

