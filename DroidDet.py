#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 24 11:24:48 2020

@author: Borja Molina Coronado
@email: borja.molina@ehu.eus
"""

from analysis.APKFeatureExtractor import APKFeatureExtractor
from analysis.FeatureEncoder import FeatureEncoder
import numpy as np
from rotation_forest import RotationForestClassifier
from sklearn.metrics import cohen_kappa_score, make_scorer
from sklearn.model_selection import ParameterGrid
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.model_selection import TimeSeriesSplit
from sklearn import metrics
import multiprocessing as mp
import traceback
import operator
import glob
import pickle
import time
import os


class DroidDet:
    """
    Class implementing the method by Zhu et al. in 
    "DroidDet: Effective and robust detection of android malware using
    static analysis along with rotation forest model"
    
    APK Information and features:
        Manifest -> permissions and Intent Actions
        Code -> Intent Actions (td-idf)
        Sensitive APIs -> those associated to perms
        Permission Ratio = n_perms/size_smali_file
          
    Features:
        Not-specified
    Model:
        Rotation Forest
        
        *Note: The number of event features selected is not reported
        Also, the criterion to consider an API as sensitive is not described
    """
    

    _working_path = None
    _data_path = None

    _column_names = None
    
    _apkEvStatsBenign = {}
    _apkEvStatsMalware = {}
    _n_malw = 0
    _n_good = 0
    
    _analyzer = None
    _encoder = None
    
    _clf = None
    _best_params = None
    _model_file = None
    
    
    def __init__ (self, random_state=2020, run_id="default", data_path = '', working_path="./"):
        """
        
        Parameters
        ----------
        run_id : String
            String with run id to store results uniquely
        data_path : String
            Path to the directory containing the apk samples (dataset)
        working_path : String
            Path where files are stored

        Returns
        -------
        None.


        """

        self._encoder = FeatureEncoder()
        if not working_path.endswith('/'):
            working_path += '/'
        if data_path == '':
            raise Exception('No data path provided')
        self._data_path = data_path
        self._working_path = working_path + "DroidDet_Data/"
        if not os.path.exists(self._working_path):
            os.makedirs(self._working_path)
        self._random_state = random_state
        self._model_file = self._working_path + run_id + '_model.pkl'
        # Load model if exists
        try:
            with open(self._model_file, 'rb') as f:
                mod = pickle.load(f)
                self._clf = mod[0]
                self._encoder = mod[1]
        except FileNotFoundError:
            pass
        
    
    def _writeAPKfeatures (self, apk_list, parent_out_dir ):
        analyzer = APKFeatureExtractor()
        for idx in range(len(apk_list)):
            data = {}
            path = apk_list[idx]
            path = path.replace(self._data_path, '').replace('../', '')
            path = path.replace('/', '_')
            path = path[:-4]
            write_path = parent_out_dir + path + ".pkl"
            try:
                analyzer.processAPK(apk_list[idx])
                perms = analyzer.getDeclaredPermissions()
                int_act = analyzer.getIntentFilterActionsInManifest()
                int_act.extend(analyzer.getIntentActionsInCode())
                _, apis = analyzer.getRequestedPermissionsWithPScoutMappings()
                dex_size = analyzer.getDexSize()/(1024*1024)  # In MB
                ratio = len(perms)/dex_size
                data['perm_apis'] = np.array( perms + list(apis.keys()) )
                data['events'] = np.array(int_act)
                data['ratio'] = ratio
            except:
                traceback.print_exc()
                print("Corrupted file: ", apk_list[idx])
                #os.remove(apk_list[idx])
                continue  # Omit that corrupted apk
            
            with open(write_path, 'wb') as f:
                pickle.dump(data, f)
    
    
    
    # Remove already parsed files from parsing
    def _filterInputList (self, apk_list, parent_out_dir):
        new_list = []
        for idx in range(len(apk_list)):
            path = apk_list[idx]
            path = path.replace(self._data_path, '').replace('../', '')
            path = path.replace('/', '_')
            path = path[:-4]
            write_path = parent_out_dir + path + ".pkl"
            if os.path.isfile(write_path): continue
            new_list.append(apk_list[idx])
        return new_list
    
    
    def parseAPKs (self, njobs = -1, overwrite=True):
        """
        This method extract features from every apk found inside path and 
        stores the results in the working_path 
        

        Parameters
        ----------
            njobs : integer
                -1 use all cpus
                1 use only one thread
            overwrite : boolean
                True to overwrite existing files

        Returns
        -------
        None.

        """
        t1 = time.time()
        
        files = glob.glob(self._data_path+'/**/*.apk', recursive=True)
        n_files = len(files)
        print("Found", n_files, " in directory")
        
        if not overwrite:
            files = self._filterInputList(files, self._working_path)
            n_files = len(files)
            print("Remaining", n_files) 
        
        # Parallellized        
        processes = []
        if njobs<0:
            nproc = mp.cpu_count()-2
            part_size = n_files//nproc
        else:
            nproc = njobs
            part_size = n_files//nproc
        
        init_idx = 0
        fin_idx = 0
        for pn in range(nproc-1):
            init_idx = pn*part_size
            fin_idx = (pn+1)*part_size
            processes.append(mp.Process(target=self._writeAPKfeatures, args=(files[init_idx:fin_idx], self._working_path) ))
            processes[pn].start()
            
        # This process also works! Do the rest
        self._writeAPKfeatures(files[fin_idx:], self._working_path)
        
        # Wait for the rest
        for proc in processes:
            proc.join()
        
        t2 = time.time()
        print("Finished in: ", t2-t1, ", avg time: ", (t2-t1)/(1+n_files), "s/apk" )
        
        
    def _collectStatistics( self, events, label):
        """
        It seems that authors use frequency of EVENTS in malware and benign (i.e. 
        boolean TF, instead of raw count TF)
        
        events: List []

        """
        if label == 1:
            stats = self._apkEvStatsMalware
            self._n_malw += 1
        else:
            stats = self._apkEvStatsBenign
            self._n_good += 1
        
        for ev in events:
            try:
                stats[ev] += 1
            except KeyError:
                stats[ev] = 1
    
    
    def _getEventFeatures (self, n_feat = 150):
        """
        Return (n_feat) most frequent events in training set

        Parameters
        ----------
        n_feat : int, 
            The number of event features selected. The default is 100.

        Returns
        -------
        events: List[] 
            The names of the top selected events

        """        
        events = []
        sorted_malw = sorted(self._apkEvStatsMalware.items(), key=operator.itemgetter(1))
        sorted_good = sorted(self._apkEvStatsBenign.items(), key=operator.itemgetter(1))
        for key in sorted_good:
            events.append(key)
            if len(events)>=(n_feat/2):
                break
        for key in sorted_malw:
            if key not in events:
                events.append(key)
            if len(events)>=n_feat:
                break
        
        return events
    


    def _computeFeatureSet (self, APKs_train, Y_train):
        init = time.time()
        
        # Read training apks to get the global feature set
        n_files = len(APKs_train)
        train_feat = []
        feat_append = train_feat.append
        n = 0
        while n<n_files:
            path = APKs_train[n]
            path = path.replace(self._data_path, '').replace('../', '')
            path = path.replace('/', '_')
            path = path[:-4]
            name = self._working_path + path + ".pkl"
            with open(name, 'rb') as f:
                data = pickle.load(f)
                
            perm_apis = data['perm_apis']
            int_act = data['events'] 
            ratio = data['ratio']
            
            label = Y_train[n]
            self._collectStatistics(int_act, label)
            
            feat_append([self._encoder.addList(perm_apis), int_act, ratio])  # (index_apk, perm List [] )
            n += 1
        
        # Add selected top ranked events
        events = self._getEventFeatures()
        self._encoder.addList(events)
                
        n_features = self._encoder.getLength()+1  # + ratio
        
        X_train = np.zeros((len(APKs_train), n_features), dtype=np.float32)
        for n in range(len(APKs_train)):
            X_train[n,:-1] = self._encoder.getOneHotEncode(train_feat[n][1])  # Encode events of this apk
            X_train[n,train_feat[n][0]] = 1  # Add perm_apis
            X_train[n,-1] = train_feat[n][2]  # Add ratio
        
        
        del train_feat
        
        self._column_names = self._encoder.getFeatureNames() + ["Permisson-Rate"]
        self._column_names.append("Class")
        # Store data into csv files
        #pd.DataFrame(np.append(X_train, Y_train.reshape(Y_train.shape[0],1), axis=1), columns=self._column_names).to_csv(self._train_csv, index=False)
        
        fin = time.time()
        print("Processed: ", len(APKs_train), " in ", fin-init, " seconds")
        
        return X_train, Y_train
    
    
    def _extractFeaturesFor (self, APKs, Y=None):
        # Get testing permissions and Encode testing set
        n_features = self._encoder.getLength() + 1  # + Ratio
        n_apks = len(APKs)
        X = np.zeros((n_apks, n_features), dtype=np.float32)
        n = 0
        while n<n_apks:
            path = APKs[n]
            path = path.replace(self._data_path, '').replace('../', '')
            path = path.replace('/', '_')
            path = path[:-4]
            name = self._working_path + path + ".pkl"
            with open(name, 'rb') as f:
                data = pickle.load(f)
            
            perm_apis = data['perm_apis']
            int_act = data['events'] 
            ratio = data['ratio']
            
            x = self._encoder.getOneHotEncode(np.append(perm_apis,int_act))  
            X[n,:-1] = x
            X[n,-1] = ratio
            n += 1
            
        if type(Y)==np.ndarray:
            return X, Y
        else:
            return X
    
    
    def _GridSearchCV (self, X, Y, estimator, param_grid, kf_cv, scorer):
        results = []
        print("Finding best hiperparameters for the model")
        X = np.array(X)
        num_cvs = kf_cv.get_n_splits()
        for params in list(ParameterGrid(param_grid)):
            t1 = time.time()
            n_features = 0
            score = 0
            for train_index, test_index in kf_cv.split(X, Y):
                # Fit to k-1 folds
                X_train, Y_train = self._computeFeatureSet(X[train_index], Y[train_index])
                n_features += X_train.shape[1]
                model = estimator(**params)
                model.fit(X_train, Y_train)
                # Predict k fold
                X_test, Y_test = self._extractFeaturesFor(X[test_index], Y[test_index])
                Y_pred = model.predict(X_test)
                score += scorer(Y_test, Y_pred)
            results.append((params,score/num_cvs))
            t2 = time.time()
            print("Grid Search iteration", t2-t1, "seconds with", n_features/num_cvs, "mean features" )
        # Sort restuls and return the best
        results = sorted(results, key=lambda item: item[1], reverse=True)
        return results[0]
    

    
    def fit (self, X, Y, kfold_splits=5, n_repeats=1, time_dependent_folds=False):
        """
        Computes feature set of X and trains the Random Forest classifier. Returns the validation metrics.

        Parameters
        ----------
        X : List of Strings 
            List containing the apk names used for training
        Y : np.array
            Array containing the class labels of the apks in X
        kfold_splits : int, optional
            Number of cross validation splits used in training. The default is 5.
        n_repeats : int, optional
            Number of repetitions of cross validation in training. The default is 1.
        time_dependent_folds : bool, optional
            Use time-dependent-aware k-folds (sklearn TimeSeriesSplit). If True, n_repeats is
            ignored. The default is False.
            
        Returns
        -------
        (TPR, FPR, precision, F1, AUC, Kappa)
            TPR: average of the True Possitive Rate of the classifier in the cross-validation set
            FPR: average of the False Possitive Rate of the classifier in the cross-validation set
            precision: ratio of correct positive predictions
            F1: average of the F1 score of the classifier in the cross-validation set
            AUC: average Area Under the ROC Curve of the classifier in the cross-validation set
            Kappa: Cohen Kappa
            
        """
        if time_dependent_folds:
            kf = TimeSeriesSplit(max_train_size=None, n_splits=kfold_splits)
        else:
            kf = RepeatedStratifiedKFold(n_splits = kfold_splits, n_repeats=n_repeats, random_state=self._random_state)
        
        param_grid = {'n_estimators' : list(range(100,301,100)),
                      'criterion': ['gini', 'entropy'],
                      'min_samples_split': list(range(2,11,3)),
                      'min_samples_leaf': list(range(1,11,3)),
                      'max_features': [.1, .25, .5],
                      'max_samples': [.25, .5, .75, 1] }
        
        kappa_scorer = cohen_kappa_score
        
        rf = RotationForestClassifier
        
        best = self._GridSearchCV(X, Y, rf, param_grid, kf_cv=kf, scorer=kappa_scorer)
        
        self._best_params = best[0]
        print('Selected', self._best_params, 'with CV score', best[1])
        
        X, Y = self._computeFeatureSet(X, Y)
        
        self._clf = RotationForestClassifier(**self._best_params)
        self._clf.fit(X, Y)
        
        y_pred = self._clf.predict(X)
        
        auc = metrics.roc_auc_score(Y, y_pred)
        f1 = metrics.f1_score(Y, y_pred)
        tn, fp, fn, tp = metrics.confusion_matrix(Y, y_pred).ravel()
        fpr = fp/(fp+tn)
        tpr = tp/(tp+fn)
        precision = tp/(tp+fp)
        kappa = cohen_kappa_score(Y, y_pred)
        
        print("On train set (tpr, fpr, precision, f1, auc, kappa):", tpr, fpr, precision, f1, auc, kappa )
        
        print("Saving model to ", self._model_file)
        with open(self._model_file, 'wb') as f:
            pickle.dump([self._clf, self._encoder], f)
        
        return tpr, fpr, precision, f1, auc, kappa
        
        
    
    def evaluate (self, X, Y):
        """
        Evaluates the Classifier and returns the metrics computed.

        Parameters
        ----------
        X : List of Strings
            List with the APK names in the testing set.
        Y : np.array
            Array containing the class labels of the apks in X

        Returns
        -------
        (TPR, FPR, precision, F1, AUC, Kappa)
            TPR: True Possitive Rate of the classifier
            FPR: False Possitive Rate of the classifier
            precision: ratio of correct positive predictions
            F1: F1 score of the classifier
            AUC: Area Under the ROC Curve of the classifier
            Kappa: Cohen Kappa of the classifier

        """
        X, Y = self._extractFeaturesFor(X, Y)
        y_pred = self._clf.predict(X)
        auc = metrics.roc_auc_score(Y, y_pred)
        f1 = metrics.f1_score(Y, y_pred)
        tn, fp, fn, tp = metrics.confusion_matrix(Y, y_pred).ravel()
        fpr = fp/(fp+tn)
        tpr = tp/(tp+fn)
        precision = tp/(tp+fp)
        kappa = cohen_kappa_score(Y, y_pred)
        print("On test set (tpr, fpr, precision, f1, auc, kappa):", tpr, fpr, precision, f1, auc, kappa )
        return tpr, fpr, precision, f1, auc, kappa
    
    
    def predict (self, APKs):
        """
        

        Parameters
        ----------
        APKs : List of Strings
            List with the APK names

        Returns
        -------
        y_pred : np.array
            Predictions returned by the classifier

        """
        to_be_parsed = self._filterInputList(APKs, self._working_path)
        if len(to_be_parsed)>0:
            self._writeAPKfeatures(to_be_parsed, self._working_path)
        X = self._extractFeaturesFor(APKs)
        y_pred = self._clf.predict(X)
        return y_pred
        