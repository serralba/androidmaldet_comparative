#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 14 10:27:18 2020

@author: Borja Molina Coronado
@email: borja.molina@ehu.eus
"""

from analysis.APKFeatureExtractor import APKFeatureExtractor
from analysis.FeatureEncoder import FeatureEncoder
import numpy as np
from sklearn.feature_selection import mutual_info_classif
from sklearn.metrics import cohen_kappa_score, make_scorer
from sklearn.model_selection import ParameterGrid
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.model_selection import TimeSeriesSplit
from sklearn.svm import LinearSVC
from sklearn import metrics
import multiprocessing as mp
from collections.abc import Iterable
import traceback
import glob
import time
import pickle
import os


class ICCDetector:
    """
    Class implementing the method by Xu et al. in 
    "ICCDetector: ICC-Based Malware Detection on Android"
    
    
    APK Information and Features:
        #Components (Services, Activities, Broadcast Receivers, Provider):
            -component_name(activity/service/provider)
            -component_name(receiver_static)
            -component_name(receiver_dynamic)
            -num_of_activity/service/provider
            -num_of_receiver(static)
            -num_of_receiver(dynamic)
        #Explicit Intents (External if package name is not in the apk):
            -num_of_explicitintent
            -num_of_external_explicitintent
            -external_package_name(external_explicitintent)
        #Implicit Intents (if None of the IntentFilters process the action of an 
        implicit intent, it is external. Also, all system actions have the
        android.intent.action prefix):
            -num_of_implicitintent
            -num_of_internal_implicitintent
            -num_of_external_implicitintent(userdefined_action)
            -num_of_external_implicitintent(system_action)
            -action_string(internal)
            -action_string(external_userdefined_action)
            -action_string(external_system_action)
        #Intent Filters
            -num_of_intentfilter
            -num_of_intentfilter_for_activity/service
            -num_of_intentfilter_for_receiver(static)
            -action_string(for_receiver_dynamic)
            -num_of_intentfilter_for_receiver(dynamic)
            -action_string(for_activity/service)
            -action_string(for_receiver_static)
        
    F.Selection:
        CFS (best 5000 Components)
    Model:
        Linear SVM 
        
        *Note: model parameters are not reported
    
    """

    
    _column_names = None
    _analyzer = None
    _encoder = None
        
    _clf = None
    
    _working_path = None
    _data_path = None
    _model_file = None
    
    def __init__ (self, random_state=2020, run_id="default", data_path = '', working_path="./"):
        """
 

        Parameters
        ----------
        run_id : String
            String with run id to store results uniquely
        data_path : String
            Path to the directory containing the apk samples (dataset)
        working_path : String
            Path where files are stored

        Returns
        -------
        None.

        """
        self._encoder = FeatureEncoder()
        if not working_path.endswith('/'):
            working_path += '/'
        if data_path == '':
            raise Exception('No data path provided')
        self._data_path = data_path
        self._working_path = working_path + "ICCDetector_Data/"
        if not os.path.exists(self._working_path):
            os.makedirs(self._working_path)
        self._random_state = random_state
        self._model_file = self._working_path + run_id + '_model.pkl'
        # Load model if exists
        try:
            with open(self._model_file, 'rb') as f:
                mod = pickle.load(f)
                self._clf = mod[0]
                self._encoder = mod[1]
        except FileNotFoundError:
            pass
        
        
    def _writeAPKfeatures (self, apk_list, parent_out_dir ):
        analyzer = APKFeatureExtractor()
        for idx in range(len(apk_list)):
            path = apk_list[idx]
            path = path.replace(self._data_path, '').replace('../', '')
            path = path.replace('/', '_')
            path = path[:-4]
            write_path = parent_out_dir + path + ".pkl"
            try:
                pkg = analyzer.processAPK(apk_list[idx])
                app_comp = analyzer.getAppComponents()
                intf_code, broadcast_dyn = analyzer.getIntentFiltersInCode()
                intf_manifest = analyzer.getIntentFiltersInManifest()
                intents = analyzer.getIntentsInCode()

                data = {'pkg': pkg,
                        'app_comp': app_comp,
                        'intf_code': intf_code,
                        'broadcast_dyn': broadcast_dyn,
                        'intf_manifest': intf_manifest,
                        'intents': intents}
            except:
                traceback.print_exc()
                print("Corrupted file: ", apk_list[idx])
                #os.remove(apk_list[idx])
                continue  # Omit that corrupted apk
            
            with open(write_path, 'wb') as f:
                pickle.dump(data, f)
    
    
    # Remove already parsed files from parsing
    def _filterInputList (self, apk_list, parent_out_dir):
        new_list = []
        for idx in range(len(apk_list)):
            path = apk_list[idx]
            path = path.replace(self._data_path, '').replace('../', '')
            path = path.replace('/', '_')
            path = path[:-4]
            write_path = parent_out_dir + path + ".pkl"
            if os.path.isfile(write_path): continue
            new_list.append(apk_list[idx])
        return new_list
    
    
    def parseAPKs (self, njobs=-1, overwrite=True):
        """
        This method extract features from every apk found inside path and 
        stores the results in the working_path 
        

        Parameters
        ----------
            njobs : integer
                -1 use all cpus
                1 use only one thread
            overwrite : boolean
                True to overwrite existing files

        Returns
        -------
        None.

        """
        t1 = time.time()
        
        files = glob.glob(self._data_path+'/**/*.apk', recursive=True)
        n_files = len(files)
        print("Found", n_files, " in directory")
        
        if not overwrite:
            files = self._filterInputList(files, self._working_path)
            n_files = len(files)
            print("Remaining", n_files)
        
        # Parallellized        
        processes = []
        if njobs<0:
            nproc = mp.cpu_count()-2
            part_size = n_files//nproc
        else:
            nproc = njobs
            part_size = n_files//nproc
        
        init_idx = 0
        fin_idx = 0
        for pn in range(nproc-1):
            init_idx = pn*part_size
            fin_idx = (pn+1)*part_size
            processes.append(mp.Process(target=self._writeAPKfeatures, args=(files[init_idx:fin_idx], self._working_path) ))
            processes[pn].start()
            
        # This process also works! Do the rest
        self._writeAPKfeatures(files[fin_idx:], self._working_path)
        
        # Wait for the rest
        for proc in processes:
            proc.join()
        
        t2 = time.time()
        print("Finished in: ", t2-t1, ", avg time: ", (t2-t1)/(1+n_files), "s/apk" )
    
    
    #Extracts features from app info
    def compute_one_hot_strings_and_counts (self, pkg, app_comp, intf_code, broadcast_dyn, intf_manifest, intents ):
        feat = []
        counts = []
        for key in app_comp.keys():
            for comp in app_comp[key]:
                if key=='BroadcastReceiver':
                    feat.append(comp+'(receiver_static)')
                else:    
                    feat.append(comp+'('+key+')')
                    
        counts.append(len(app_comp['Activity']))
        counts.append(len(app_comp['Service']))
        counts.append(len(app_comp['Provider']))
        counts.append(len(app_comp['BroadcastReceiver']))
        
        num_dyn = 0
        for broad in broadcast_dyn:
            if broad not in app_comp['BroadcastReceiver']:
                feat.append(comp+'(receiver_dynamic)')
                num_dyn += 1
                
        counts.append(num_dyn)
        
        
        for_recv_dyn = 0
        for_recv_sta = 0
        for_service = 0
        for_activity = 0
        for int_fil in intf_code:
            try:
                actions = int_fil['Action']
                if type(actions)==list and len(actions)>1:
                    for action in actions:
                        if type(action)==str:
                            feat.append(action+'(for_receiver_dynamic)')
                            for_recv_dyn += 1
            except KeyError:
                pass
                    
        for key in intf_manifest.keys():
            for int_filt in intf_manifest[key]:
                try:
                    actions = int_filt['action']
                    for action in actions:
                        if key=='BroadcasReceivers':
                            feat.append(action+'(for_receiver_static)')
                            for_recv_sta += 1
                        elif key=='Activity':
                            for_activity += 1
                            feat.append(action+'(for_'+key+')')
                        elif key=='Service':
                            for_service += 1
                            feat.append(action+'(for_'+key+')')
                except KeyError:
                    pass
        
        # Num intent filters
        counts.append(len(intf_code)+for_activity+for_service+for_recv_sta+for_recv_dyn)
        
        counts.append(for_activity)
        counts.append(for_service)
        counts.append(for_recv_sta)
        counts.append(for_recv_dyn)

        """
        For explicit intents:
            
        external_package_name(external_explicitintent)
        
        For implicit intents:
            
        action_string(internal)
        action_string(external_userdefined_action)
        action_string(external_system_action)
        """
        num_explicit = 0
        num_implicit = 0
        user_def = 0
        external_sys = 0
        external_exp = 0
        internal_imp = 0
        for intent in intents:
            if 'Package' in list(intent.keys()):  # Explicit
                num_explicit += 1
                intent_pack = intent['Package']
                if type(intent_pack)==str and pkg not in intent_pack:  # External
                    feat.append(intent['Package']+'(external_explicitintent)')
                    external_exp += 1
            else:  # Implicit
                num_implicit += 1
                try:
                    action = intent['Action']
                    if pkg.replace('/', '.') in action:
                        feat.append(intent['Action']+'(internal)')
                        internal_imp += 1
                    elif 'android.intent' in action:
                        feat.append(intent['Action']+'(external_system_action)')
                        external_sys += 1
                    else:
                        feat.append(intent['Action']+'(external_userdefined_action)')
                        user_def += 1
                except:
                    pass
        
        counts.append(num_explicit)
        counts.append(external_exp)
        counts.append(num_implicit)
        counts.append(internal_imp)
        counts.append(user_def)
        counts.append(external_sys)
        # feat = string_feat (one-hot encoding), counts = counted values
        return feat, counts
    
    
    def _computeFeatureSet (self, APKs_train, Y_train):
        init = time.time()
        
        # Read training apks to get the global feature set
        n_files = len(APKs_train)
        apk_feat = []
        apk_feat_append = apk_feat.append
        n = 0

        count_names = ['num_of_activity', 'num_of_service', 'num_of_provider','num_of_receiver(static)','num_of_receiver(dynamic)','num_of_intentfilter','num_of_intentfilter_for_activity','num_of_intentfilter_for_service','num_of_intentfilter_for_receiver(static)','num_of_intentfilter_for_receiver(dynamic)','num_of_explicitintent','num_of_external_explicitintent','num_of_implicitintent','num_of_internal_implicitintent','num_of_external_implicitintent(userdefined_action)','num_of_external_implicitintent(system_action)']
        
        C_mat = np.zeros((n_files, len(count_names)), dtype=np.int32)
        
        while n<n_files:
            path = APKs_train[n]
            path = path.replace(self._data_path, '').replace('../', '')
            path = path.replace('/', '_')
            path = path[:-4]
            name = self._working_path + path + ".pkl"
            with open(name, 'rb') as f:
                data = pickle.load(f)
            # Compute features
            pkg = data['pkg']
            app_comp = data['app_comp']
            intf_code = data['intf_code']
            broadcast_dyn = data['broadcast_dyn']
            intf_manifest = data['intf_manifest']
            intents = data['intents']
            
            # For one hot strings
            string_feat, counts = self.compute_one_hot_strings_and_counts ( pkg, app_comp, intf_code, broadcast_dyn, intf_manifest, intents )
            
            # Store counts
            C_mat[n] = counts
            # End compute features
            apk_feat_append(self._encoder.addList(np.array(list(set(string_feat)))))
            n += 1
        
        # Encode training
        n_features = self._encoder.getLength()
        
        IComp = np.zeros((len(APKs_train), n_features), dtype=np.int8)
        
        for n in range(len(apk_feat)):
            IComp[n,apk_feat[n]] = 1
            
            
        del apk_feat
        
        idx = self._MutualInfo_FSS(IComp, Y_train)
        
        self._encoder.filterFeatures(idx)
        
        # Concatenate both feature sets
        X_train = np.append(IComp[:,idx], C_mat, axis=1)
        
        self._column_names = self._encoder.getFeatureNames() + count_names
        self._column_names.append("Class")
        
        fin = time.time()
        print("Processed: ", len(APKs_train), " in ", fin-init, " seconds")
        
        return X_train, Y_train
    
    
    def _extractFeaturesFor (self, APKs, Y=None):
        # Get testing permissions and Encode testing set
        count_names = ['num_of_activity', 'num_of_service', 'num_of_provider','num_of_receiver(static)','num_of_receiver(dynamic)','num_of_intentfilter','num_of_intentfilter_for_activity','num_of_intentfilter_for_service','num_of_intentfilter_for_receiver(static)','num_of_intentfilter_for_receiver(dynamic)','num_of_explicitintent','num_of_external_explicitintent','num_of_implicitintent','num_of_internal_implicitintent','num_of_external_implicitintent(userdefined_action)','num_of_external_implicitintent(system_action)']
        n_features = self._encoder.getLength() + len(count_names)
        n_apks = len(APKs)
        X = np.zeros((n_apks, n_features), dtype=np.int32)
        n = 0
        while n<n_apks:
            path = APKs[n]
            path = path.replace(self._data_path, '').replace('../', '')
            path = path.replace('/', '_')
            path = path[:-4]
            name = self._working_path + path + ".pkl"
            with open(name, 'rb') as f:
                data = pickle.load(f)
            
            # Compute features
            pkg = data['pkg']
            app_comp = data['app_comp']
            intf_code = data['intf_code']
            broadcast_dyn = data['broadcast_dyn']
            intf_manifest = data['intf_manifest']
            intents = data['intents']
            
            # For one hot strings
            string_feat, counts = self.compute_one_hot_strings_and_counts ( pkg, app_comp, intf_code, broadcast_dyn, intf_manifest, intents )
        
            # End compute features
            x = self._encoder.getOneHotEncode(string_feat)  # Avoid label
            X[n,:-len(count_names)] = x
            X[n,-len(count_names):] = counts
            n += 1
            
        if type(Y)==np.ndarray:
            return X, Y
        else:
            return X

    
    def _MutualInfo_FSS (self, X, Y, n_best=5000):
        """
        1000 best features are selected

        Parameters
        ----------
        X : np.array
            Data
        Y : np.array
            Class
        n_best : int, optional
            Number of features to select. The default is 5000.

        Returns
        -------
        np.array with the indexes of the n best features

        """
        mi = mutual_info_classif(X, Y)
        idx = np.argsort(mi)
        return idx[-n_best:]
        
    
    def _GridSearchCV (self, X, Y, estimator, param_grid, kf_cv, scorer):
        results = []
        print("Finding best hiperparameters for the model")
        X = np.array(X)
        num_cvs = kf_cv.get_n_splits()
        for params in list(ParameterGrid(param_grid)):
            t1 = time.time()
            n_features = 0
            score = 0
            for train_index, test_index in kf_cv.split(X, Y):
                # Fit to k-1 folds
                X_train, Y_train = self._computeFeatureSet(X[train_index], Y[train_index])
                
                n_features += X_train.shape[1]
                model = estimator(**params)
                model.fit(X_train, Y_train)
                # Predict k fold
                X_test, Y_test = self._extractFeaturesFor(X[test_index], Y[test_index])
                Y_pred = model.predict(X_test)
                score += scorer(Y_test, Y_pred)
            results.append((params,score/num_cvs))
            t2 = time.time()
            print("Grid Search iteration", t2-t1, "seconds with", n_features/num_cvs, "mean features" )
        # Sort restuls and return the best
        results = sorted(results, key=lambda item: item[1], reverse=True)
        return results[0]
    
    
    def fit (self, X, Y, kfold_splits=5, n_repeats=1, time_dependent_folds=False):
        """
        Computes feature set of X and trains the Random Forest classifier. Returns the validation metrics.

        Parameters
        ----------
        X : List of Strings 
            List containing the apk names used for training
        Y : np.array
            Array containing the class labels of the apks in X
        kfold_splits : int, optional
            Number of cross validation splits used in training. The default is 5.
        n_repeats : int, optional
            Number of repetitions of cross validation in training. The default is 1.
        time_dependent_folds : bool, optional
            Use time-dependent-aware k-folds (sklearn TimeSeriesSplit). If True, n_repeats is
            omited. The default is False.
            
        Returns
        -------
        (TPR, FPR, precision, F1, AUC, Kappa)
            TPR: average of the True Possitive Rate of the classifier in the cross-validation set
            FPR: average of the False Possitive Rate of the classifier in the cross-validation set
            precision: ratio of correct positive predictions
            F1: average of the F1 score of the classifier in the cross-validation set
            AUC: average Area Under the ROC Curve of the classifier in the cross-validation set
            Kappa: Cohen Kappa
            
        """        
        if time_dependent_folds:
            kf = TimeSeriesSplit(max_train_size=None, n_splits=kfold_splits)
        else:
            kf = RepeatedStratifiedKFold(n_splits = kfold_splits, n_repeats=n_repeats, random_state=self._random_state)
        
        param_grid = {'penalty': ['l2'],
                      'loss': ['hinge',  'squared_hinge'],
                      'tol': [0.0001,0.00025,0.0005, 0.001, 0.003],
                      'C': [0.01, 0.26, 0.51, 0.76, 1],
                      'max_iter': list(range(1000, 12000, 2500))}
        
        kappa_scorer = cohen_kappa_score
        
        svm = LinearSVC
        
        best = self._GridSearchCV(X, Y, svm, param_grid, kf_cv=kf, scorer=kappa_scorer)
        
        self._best_params = best[0]
        print('Selected', self._best_params, 'with CV score', best[1])
        
        X, Y = self._computeFeatureSet(X, Y)
        
        self._clf = LinearSVC(**self._best_params)
        self._clf.fit(X,Y) 
        y_pred = self._clf.predict(X)
        
        auc = metrics.roc_auc_score(Y, y_pred)
        f1 = metrics.f1_score(Y, y_pred)
        tn, fp, fn, tp = metrics.confusion_matrix(Y, y_pred).ravel()
        fpr = fp/(fp+tn)
        tpr = tp/(tp+fn)
        precision = tp/(tp+fp)
        kappa = cohen_kappa_score(Y, y_pred)
        
        print("On train set (tpr, fpr, precision, f1, auc, kappa):", tpr, fpr, precision, f1, auc, kappa )
        
        print("Saving model to ", self._model_file)
        with open(self._model_file, 'wb') as f:
            pickle.dump([self._clf, self._encoder], f)
        
        return tpr, fpr, precision, f1, auc, kappa
    
    
    def evaluate (self, X, Y):
        """
        Evaluates the Classifier and returns the metrics computed.

        Parameters
        ----------
        X : List of Strings
            List with the APK names in the testing set.
        Y : np.array
            Array containing the class labels of the apks in X

        Returns
        -------
        (TPR, FPR, precision, F1, AUC, Kappa)
            TPR: True Possitive Rate of the classifier
            FPR: False Possitive Rate of the classifier
            precision: ratio of correct positive predictions
            F1: F1 score of the classifier
            AUC: Area Under the ROC Curve of the classifier
            Kappa: Cohen Kappa of the classifier

        """
        X, Y = self._extractFeaturesFor(X, Y)
        
        y_pred = self._clf.predict(X)
        auc = metrics.roc_auc_score(Y, y_pred)
        f1 = metrics.f1_score(Y, y_pred)
        tn, fp, fn, tp = metrics.confusion_matrix(Y, y_pred).ravel()
        fpr = fp/(fp+tn)
        tpr = tp/(tp+fn)
        precision = tp/(tp+fp)
        kappa = cohen_kappa_score(Y, y_pred)
        print("On test set (tpr, fpr, precision, f1, auc, kappa):", tpr, fpr, precision, f1, auc, kappa )
        return tpr, fpr, precision, f1, auc, kappa
    
    
    def predict (self, APKs):
        """
        

        Parameters
        ----------
        APKs : List of Strings
            List with the APK names

        Returns
        -------
        y_pred : np.array
            Predictions returned by the classifier

        """
        to_be_parsed = self._filterInputList(APKs, self._working_path)
        if len(to_be_parsed)>0:
            self._writeAPKfeatures(to_be_parsed, self._working_path)
        X = self._extractFeaturesFor(APKs)
        y_pred = self._clf.predict(X)
        return y_pred
        