#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 11:20:30 2020

@author: Borja Molina Coronado
@email: borja.molina@ehu.eus
"""


from analysis.APKFeatureExtractor import APKFeatureExtractor
from analysis.FeatureEncoder import FeatureEncoder
import numpy as np
from sklearn.metrics import cohen_kappa_score, make_scorer
from sklearn.model_selection import ParameterGrid
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.model_selection import TimeSeriesSplit
from sklearn.svm import LinearSVC
import multiprocessing as mp
from sklearn import metrics
import traceback
import pickle
import glob
import time
import os

class Drebin:
    """
    Class implementing the method by Arp et al. in
    "Drebin: Effective and explainable detection of android malware in your pocket"
    
    APK Information:
        Manifest -> HW components (S1), permissions (S2), App Components (S3) (services, 
        activities, broadcast receivers and content providers), Intents (S4)
        (actions)
        Code -> restricted APIcalls (S5) requiring a permission (S6) - PScoutMappings,
        suspicious API calls (S7).
        Strings -> IP, hostnames and URLs (S8)
    Features:
        OneHot (binary)
    Model:
        SVM with linear kernel
    
    """
 
    #List of suspicious API calls
    S7_list = ['getDeviceId', 'getSubscriberId', 'setWifiEnabled', 'sendTextMessage', 'Runtime;->exec', 'Cipher;->getInstance']
    
    # IPregex, hostname regex, URL regex
    S8_filters = ['^[0-9]{1,3}(\.[0-9]{1,3}){3}$', '^[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$', '[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)' ]

    _column_names = None
    _encoder = None
    
    _clf = None
    _model_file = None
    _working_path = None
    _random_state = None
    
    _encoder2 = None
    
    def __init__ (self, random_state=2020, run_id="default", data_path = '', working_path="./"):
        """
        
        Parameters
        ----------
        run_id : String
            String with run id to store results uniquely
        data_path : String
            Path to the directory containing the apk samples (dataset)
        working_path : String
            Path where files are stored

        Returns
        -------
        None.


        """

        self._encoder = FeatureEncoder()
        if not working_path.endswith('/'):
            working_path += '/'
        if data_path == '':
            raise Exception('No data path provided')
        self._data_path = data_path
        self._working_path = working_path + "Drebin_Data/"
        if not os.path.exists(self._working_path):
            os.makedirs(self._working_path)
        self._random_state = random_state
        self._model_file = self._working_path + run_id + '_model.pkl'
        # Load model if exists
        try:
            with open(self._model_file, 'rb') as f:
                mod = pickle.load(f)
                self._clf = mod[0]
                self._encoder = mod[1]
        except FileNotFoundError:
            pass
        
        try:
            # Read Dictionary to decode basic blocks
            feat_dict = self._working_path + "Drebin_Dictionary.pkl"
            with open(feat_dict, 'rb') as f:
                self._encoder2 = pickle.load(f)
        except FileNotFoundError:
            self._encoder2 = FeatureEncoder()
            print('Decoder file not found')
            pass
    
    
    def _writeAPKfeatures (self, apk_list, parent_out_dir, lock ):
        analyzer = APKFeatureExtractor()
        for idx in range(len(apk_list)):
            path = apk_list[idx]
            path = path.replace(self._data_path, '').replace('../', '')
            path = path.replace('/', '_')
            path = path[:-4]
            write_path = parent_out_dir + path + ".pkl"
            try:
                analyzer.processAPK(apk_list[idx])
                S1_4 = analyzer.getManifestFeatures()
                S6, S5 = analyzer.getRequestedPermissionsWithPScoutMappings()
                S5 = list(S5.keys())
                S7 = list(analyzer.getRestrictedAPImethodsUsage(APIlist=self.S7_list, exclude_list=False).keys())
                S8 = list(analyzer.getStrings(self.S8_filters).keys())
                full_feat = np.array(S1_4 + S5 + S6 + S7 + S8)
                
                lock.acquire()
                data = self._encoder2.addList(full_feat)
                lock.release()
            except:
                traceback.print_exc()
                print("Corrupted file: ", apk_list[idx])
                #os.remove(apk_list[idx])
                continue  # Omit that corrupted apk
            
            with open(write_path, 'wb') as f:
                pickle.dump(data, f)
    
    
    # Remove already parsed files from parsing
    def _filterInputList (self, apk_list, parent_out_dir):
        new_list = []
        for idx in range(len(apk_list)):
            path = apk_list[idx]
            path = path.replace(self._data_path, '').replace('../', '')
            path = path.replace('/', '_')
            path = path[:-4]
            write_path = parent_out_dir + path + ".pkl"
            if os.path.isfile(write_path): continue
            new_list.append(apk_list[idx])
        return new_list
    
    
    def parseAPKs (self, njobs = -1, overwrite=True):
        """
        This method extract features from every apk found inside path and 
        stores the results in the working_path 
        

        Parameters
        ----------
            njobs : integer
                -1 use all cpus
                1 use only one thread
            overwrite : boolean
                True to overwrite existing files

        Returns
        -------
        None.

        """
        t1 = time.time()
        
        files = glob.glob(self._data_path+'/**/*.apk', recursive=True)
        n_files = len(files)
        print("Found", n_files, " in directory")
        
        if not overwrite:
            files = self._filterInputList(files, self._working_path)
            n_files = len(files)
            print("Remaining", n_files)
        
        # Parallellized        
        processes = []
        lock = mp.Lock()
        
        if njobs<0:
            nproc = mp.cpu_count()-2
            part_size = n_files//nproc
        else:
            nproc = njobs
            part_size = n_files//nproc
        
        init_idx = 0
        fin_idx = 0
        for pn in range(nproc-1):
            init_idx = pn*part_size
            fin_idx = (pn+1)*part_size
            processes.append(mp.Process(target=self._writeAPKfeatures, args=(files[init_idx:fin_idx], self._working_path, lock) ))
            processes[pn].start()
            
        # This process also works! Do the rest
        self._writeAPKfeatures(files[fin_idx:], self._working_path, lock)
        
        # Wait for the rest
        for proc in processes:
            proc.join()
        
        feat_dict = self._working_path + "Drebin_Dictionary.pkl"
        with open(feat_dict, 'wb') as f:
            pickle.dump(self._encoder2, f)
        
        t2 = time.time()
        print("Finished in: ", t2-t1, ", avg time: ", (t2-t1)/(1+n_files), "s/apk" )    
        

    def _computeFeatureSet (self, APKs_train, Y_train):
        init = time.time()
        
        # Read training apks to get the global feature set
        n_apks = len(APKs_train)
        apk_feat = []
        feat_append = apk_feat.append
        
        dic = self._encoder2.getDictionary()
        
        i = 0
        while i<n_apks:
            path = APKs_train[i]
            path = path.replace(self._data_path, '').replace('../', '')
            path = path.replace('/', '_')
            path = path[:-4]
            filename = self._working_path + path + ".pkl"
            with open(filename, 'rb') as f:
                data = pickle.load(f)
            
            # Do reverse to get feature names
            feats = [k[0] for k in dic.items() if k[1] in data]
            
            feat_append(self._encoder.addList(feats))
            i += 1
            
        n_features = self._encoder.getLength()
        
        X_train = np.zeros((len(APKs_train), n_features), dtype=np.int8)
        
        for n in range(len(apk_feat)):
            X_train[n,apk_feat[n]] = 1 
            
            
        del apk_feat
                
        self._column_names = self._encoder.getFeatureNames()
        self._column_names.append("Class")
        
        fin = time.time()
        print("Processed: ", len(APKs_train), " in ", fin-init, " seconds")
        return X_train, Y_train
    
    
    def _extractFeaturesFor ( self, APKs, Y=None):
        n_features = self._encoder.getLength()
        n_apks = len(APKs)
        X = np.zeros((n_apks, n_features), dtype=np.int8)
        
        dic = self._encoder2.getDictionary()
        
        n = 0
        while n<n_apks:
            path = APKs[n]
            path = path.replace(self._data_path, '').replace('../', '')
            path = path.replace('/', '_')
            path = path[:-4]
            filename = self._working_path + path + ".pkl"
            with open(filename, 'rb') as f:
                data = pickle.load(f)
                
            feats = [k[0] for k in dic.items() if k[1] in data]
            
            X[n] = self._encoder.getOneHotEncode(feats)
            n += 1
            
        if type(Y)==np.ndarray:
            print(Y.shape)
            return X, Y
        return X
    
    
    def _GridSearchCV (self, X, Y, estimator, param_grid, kf_cv, scorer):
        results = []
        print("Finding best hiperparameters for the model")
        X = np.array(X)
        num_cvs = kf_cv.get_n_splits()
        for params in list(ParameterGrid(param_grid)):
            t1 = time.time()
            n_features = 0
            score = 0
            for train_index, test_index in kf_cv.split(X, Y):
                # Fit to k-1 folds
                X_train, Y_train = self._computeFeatureSet(X[train_index], Y[train_index])
                n_features += X_train.shape[1]
                model = estimator(**params)
                model.fit(X_train, Y_train)
                # Predict k fold
                X_test, Y_test = self._extractFeaturesFor(X[test_index], Y[test_index])
                Y_pred = model.predict(X_test)
                score += scorer(Y_test, Y_pred)
            results.append((params,score/num_cvs))
            t2 = time.time()
            print("Grid Search iteration", t2-t1, "seconds with", n_features/num_cvs, "mean features" )
        # Sort restuls and return the best
        results = sorted(results, key=lambda item: item[1], reverse=True)
        return results[0]
    
    
    def fit (self, X, Y, kfold_splits=5, n_repeats=1, time_dependent_folds=False):
        """
        Computes feature set of X and trains the Random Forest classifier. Returns the validation metrics.

        Parameters
        ----------
        X : List of Strings 
            List containing the apk names used for training
        Y : np.array
            Array containing the class labels of the apks in X
        kfold_splits : int, optional
            Number of cross validation splits used in training. The default is 5.
        n_repeats : int, optional
            Number of repetitions of cross validation in training. The default is 1.
        time_dependent_folds : bool, optional
            Use time-dependent-aware k-folds (sklearn TimeSeriesSplit). If True, n_repeats is
            ignored. The default is False.
        
        Returns
        -------
        (TPR, FPR, precision, F1, AUC, Kappa)
            TPR: average of the True Possitive Rate of the classifier in the cross-validation set
            FPR: average of the False Possitive Rate of the classifier in the cross-validation set
            precision: ratio of correct positive predictions
            F1: average of the F1 score of the classifier in the cross-validation set
            AUC: average Area Under the ROC Curve of the classifier in the cross-validation set
            Kappa: Cohen Kappa
        """
        
        if time_dependent_folds:
            kf = TimeSeriesSplit(max_train_size=None, n_splits=kfold_splits)
        else:
            kf = RepeatedStratifiedKFold(n_splits = kfold_splits, n_repeats=n_repeats, random_state=self._random_state)
        
        param_grid = {'penalty': ['l2'],
                      'loss': ['hinge',  'squared_hinge'],
                      'tol': [0.0001,0.00025,0.0005, 0.001, 0.003],
                      'C': [0.01, 0.26, 0.51, 0.76, 1],
                      'max_iter': list(range(1000, 12000, 2500))}
        
        kappa_scorer = cohen_kappa_score
        
        svm = LinearSVC
        
        best = self._GridSearchCV(X, Y, svm, param_grid, kf_cv=kf, scorer=kappa_scorer)
        
        self._best_params = best[0]
        print('Selected', self._best_params, 'with CV score', best[1])
        
        X, Y = self._computeFeatureSet(X, Y)
        
        self._clf = LinearSVC(**self._best_params)
        self._clf.fit(X,Y)
        y_pred = self._clf.predict(X)
        
        auc = metrics.roc_auc_score(Y, y_pred)
        f1 = metrics.f1_score(Y, y_pred)
        tn, fp, fn, tp = metrics.confusion_matrix(Y, y_pred).ravel()
        fpr = fp/(fp+tn)
        tpr = tp/(tp+fn)
        precision = tp/(tp+fp)
        kappa = cohen_kappa_score(Y, y_pred)
        
        print("On train set (tpr, fpr, precision, f1, auc, kappa):", tpr, fpr, precision, f1, auc, kappa )
        
        print("Saving model to ", self._model_file)
        with open(self._model_file, 'wb') as f:
            pickle.dump([self._clf, self._encoder], f)
        
        return tpr, fpr, precision, f1, auc, kappa
    
    
    def evaluate (self, X, Y):
        """
        Evaluates the Classifier and returns the metrics computed.

        Parameters
        ----------
        X : List of Strings
            List with the APK names in the testing set.
        Y : np.array
            Array containing the class labels of the apks in X

        Returns
        -------
        (TPR, FPR, precision, F1, AUC, Kappa)
            TPR: True Possitive Rate of the classifier
            FPR: False Possitive Rate of the classifier
            precision: ratio of correct positive predictions
            F1: F1 score of the classifier
            AUC: Area Under the ROC Curve of the classifier
            Kappa: Cohen Kappa of the classifier

        """
        X, Y = self._extractFeaturesFor(X, Y)
        
        y_pred = self._clf.predict(X)
        auc = metrics.roc_auc_score(Y, y_pred)
        f1 = metrics.f1_score(Y, y_pred)
        tn, fp, fn, tp = metrics.confusion_matrix(Y, y_pred).ravel()
        fpr = fp/(fp+tn)
        tpr = tp/(tp+fn)
        precision = tp/(tp+fp)
        kappa = cohen_kappa_score(Y, y_pred)
        print("On test set (tpr, fpr, precision, f1, auc, kappa):", tpr, fpr, precision, f1, auc, kappa )
        return tpr, fpr, precision, f1, auc, kappa
    
    
    def predict (self, APKs):
        """
        

        Parameters
        ----------
        APKs : List of Strings
            List with the APK names

        Returns
        -------
        y_pred : np.array
            Predictions returned by the classifier

        """
        to_be_parsed = self._filterInputList(APKs, self._working_path)
        if len(to_be_parsed)>0:
            self._writeAPKfeatures(to_be_parsed, self._working_path)
        X = self._extractFeaturesFor(APKs)
        y_pred = self._clf.predict(X)
        return y_pred
        
