#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  6 12:43:18 2020

@author: Borja Molina Coronado
@email: borja.molina@ehu.eus
"""

from analysis.APKFeatureExtractor import APKFeatureExtractor
from analysis.FeatureEncoder import FeatureEncoder
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import cohen_kappa_score, make_scorer
from sklearn.model_selection import ParameterGrid
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.model_selection import TimeSeriesSplit
from sklearn import metrics
import multiprocessing as mp
import traceback
import glob
import pickle
import itertools
import time 
import os


class MaMaDroid:
    """
    Class implementing the method by Mariconti et. al in
    "MAMADROID: Detecting Android Malware byBuilding Markov Chains of Behavioral Models"
    
    APK Information:
        -API Call graph of the application
        
        -Nodes with no incoming edges are entrypoints.
        -Extract all possible sequences from entrypoints.
        
        -Abstract API methods to family and package level. Example:
            java.lang.Throwable;getMessage() 
            
            package: java.lang
            family: java
        
        
        
        -methods/classes with the form f.a.a.b as "obfuscated" or custom methods/classes to "selfdefined"
            X = method_desc.split('.')
            those with T = {|x|<3} - identifiers of length <3
            if |T|>=|X|/2 --> obfuscated
            else --> selfdefined
        
        -Build markov chains (package level is the best performing)
        
        
        
        
    Features:
        Adjacency vector of every API
            
    
    Model:
        Random Forests:
            101 trees and maximum depth 64
        
    """
    
    
    _column_names = None
    _encoder = None

    _clf = None
    _model_file = None
    _random_state = None
    _working_path = None
    _data_path = None
    
    MC = None
    
    packages = ["android","android.accessibilityservice","android.accounts","android.animation","android.annotation","android.app","android.app.admin","android.app.assist","android.app.backup","android.app.job","android.app.slice","android.app.usage","android.appwidget","android.bluetooth","android.bluetooth.le","android.companion","android.content","android.content.pm","android.content.res","android.database","android.database.sqlite","android.drm","android.gesture","android.graphics","android.graphics.drawable","android.graphics.drawable.shapes","android.graphics.fonts","android.graphics.pdf","android.hardware","android.hardware.biometrics","android.hardware.camera2","android.hardware.camera2.params","android.hardware.display","android.hardware.fingerprint","android.hardware.input","android.hardware.usb","android.icu.lang","android.icu.math","android.icu.text","android.icu.util","android.inputmethodservice","android.location","android.media","android.media.audiofx","android.media.browse","android.media.effect","android.media.midi","android.media.projection","android.media.session","android.media.tv","android.mtp","android.net","android.net.http","android.net.nsd","android.net.rtp","android.net.sip","android.net.wifi","android.net.wifi.aware","android.net.wifi.hotspot2","android.net.wifi.hotspot2.omadm","android.net.wifi.hotspot2.pps","android.net.wifi.p2p","android.net.wifi.p2p.nsd","android.net.wifi.rtt","android.nfc","android.nfc.cardemulation","android.nfc.tech","android.opengl","android.os","android.os.health","android.os.storage","android.os.strictmode","android.preference","android.print","android.print.pdf","android.printservice","android.provider","android.renderscript","android.sax","android.se.omapi","android.security","android.security.keystore","android.service.autofill","android.service.carrier","android.service.chooser","android.service.dreams","android.service.media","android.service.notification","android.service.quicksettings","android.service.restrictions","android.service.textservice","android.service.voice","android.service.vr","android.service.wallpaper","android.speech","android.speech.tts","android.support.animation","android.support.annotation","android.support.app.recommendation","android.support.asynclayoutinflater","android.support.compat","android.support.content","android.support.coordinatorlayout","android.support.coreui","android.support.coreutils","android.support.cursoradapter","android.support.customtabs","android.support.customview","android.support.design.widget","android.support.documentfile","android.support.drawerlayout","android.support.dynamicanimation","android.support.exifinterface","android.support.fragment","android.support.graphics.drawable","android.support.graphics.drawable.animated","android.support.interpolator","android.support.loader","android.support.localbroadcastmanager","android.support.media","android.support.media.tv","android.support.mediacompat","android.support.percent","android.support.print","android.support.recommendation","android.support.slidingpanelayout","android.support.swiperefreshlayout","android.support.text.emoji","android.support.text.emoji.appcompat","android.support.text.emoji.bundled","android.support.text.emoji.widget","android.support.transition","android.support.v13","android.support.v13.app","android.support.v13.view","android.support.v13.view.inputmethod","android.support.v14.preference","android.support.v17.leanback","android.support.v17.leanback.app","android.support.v17.leanback.database","android.support.v17.leanback.graphics","android.support.v17.leanback.media","android.support.v17.leanback.system","android.support.v17.leanback.widget","android.support.v17.leanback.widget.picker","android.support.v17.preference","android.support.v4","android.support.v4.accessibilityservice","android.support.v4.app","android.support.v4.content","android.support.v4.content.pm","android.support.v4.content.res","android.support.v4.database","android.support.v4.graphics","android.support.v4.graphics.drawable","android.support.v4.hardware.display","android.support.v4.hardware.fingerprint","android.support.v4.math","android.support.v4.media","android.support.v4.media.app","android.support.v4.media.session","android.support.v4.net","android.support.v4.os","android.support.v4.print","android.support.v4.provider","android.support.v4.text","android.support.v4.text.util","android.support.v4.util","android.support.v4.view","android.support.v4.view.accessibility","android.support.v4.view.animation","android.support.v4.widget","android.support.v7.app","android.support.v7.appcompat","android.support.v7.cardview","android.support.v7.content.res","android.support.v7.graphics","android.support.v7.graphics.drawable","android.support.v7.gridlayout","android.support.v7.media","android.support.v7.mediarouter","android.support.v7.palette","android.support.v7.preference","android.support.v7.recyclerview","android.support.v7.recyclerview.extensions","android.support.v7.util","android.support.v7.view","android.support.v7.widget","android.support.v7.widget.helper","android.support.v7.widget.util","android.support.viewpager","android.support.wear","android.support.wear.ambient","android.support.wear.utils","android.support.wear.widget","android.support.wear.widget.drawer","android.system","android.telecom","android.telephony","android.telephony.cdma","android.telephony.data","android.telephony.euicc","android.telephony.gsm","android.telephony.mbms","android.test","android.test.mock","android.test.suitebuilder","android.test.suitebuilder.annotation","android.text","android.text.format","android.text.method","android.text.style","android.text.util","android.transition","android.util","android.view","android.view.accessibility","android.view.animation","android.view.autofill","android.view.inputmethod","android.view.textclassifier","android.view.textservice","android.webkit","android.widget","com.google.android.gms.ads","com.google.android.gms.ads.doubleclick","com.google.android.gms.ads.formats","com.google.android.gms.ads.mediation","com.google.android.gms.ads.mediation.admob","com.google.android.gms.ads.mediation.customevent","com.google.android.gms.ads.reward","com.google.android.gms.ads.reward.mediation","com.google.android.gms.ads.search","com.google.android.gms.ads.identifier","com.google.android.gms.analytics","com.google.android.gms.analytics.ecommerce","com.google.android.gms.analytics","com.google.android.gms.appindexing","com.google.android.gms.search","com.google.firebase.appindexing","com.google.firebase.appindexing.builders","com.google.android.gms.appinvite","com.google.firebase.appinvite","com.google.android.gms.auth","com.google.android.gms.auth.account","com.google.android.gms.auth.api.accounttransfer","com.google.android.gms.auth.api","com.google.android.gms.auth.api.credentials","com.google.android.gms.auth.api.signin","com.google.android.gms.auth.api.phone","com.google.android.gms.awareness","com.google.android.gms.awareness.fence","com.google.android.gms.awareness.snapshot","com.google.android.gms.awareness.state","com.google.android.gms.auth.api.signin","com.google.android.gms.common","com.google.android.gms.common.api","com.google.android.gms.common.data","com.google.android.gms.common.images","com.google.android.gms.actions","com.google.android.gms.common","com.google.android.gms.common.api","com.google.android.gms.security","com.google.firebase","com.google.android.gms.cast","com.google.android.gms.cast.games","com.google.android.gms.cast.framework","com.google.android.gms.cast.framework.media","com.google.android.gms.cast.framework.media.uicontroller","com.google.android.gms.cast.framework.media.widget","com.google.android.gms.drive","com.google.android.gms.drive.events","com.google.android.gms.drive.metadata","com.google.android.gms.drive.query","com.google.android.gms.drive.widget","com.google.android.gms.fido","com.google.android.gms.fido.common","com.google.android.gms.fido.fido2","com.google.android.gms.fido.fido2.api.common","com.google.android.gms.fido.u2f","com.google.android.gms.fido.u2f.api.common","com.google.android.gms.fido.u2f.api.messagebased","com.google.android.gms.fitness","com.google.android.gms.fitness.data","com.google.android.gms.fitness.request","com.google.android.gms.fitness.result","com.google.android.gms.fitness.service","com.google.android.gms.games","com.google.android.gms.games.achievement","com.google.android.gms.games.event","com.google.android.gms.games.leaderboard","com.google.android.gms.games.multiplayer","com.google.android.gms.games.multiplayer.realtime","com.google.android.gms.games.multiplayer.turnbased","com.google.android.gms.games.quest","com.google.android.gms.games.request","com.google.android.gms.games.snapshot","com.google.android.gms.games.stats","com.google.android.gms.games.video","com.google.android.gms.gcm","com.google.android.gms.identity.intents","com.google.android.gms.identity.intents.model","com.google.android.gms.iid","com.google.android.gms.instantapps","com.google.android.gms.location","com.google.android.gms.maps","com.google.android.gms.maps.model","com.google.android.gms.measurement","com.google.firebase.analytics","com.google.android.gms.measurement","com.google.android.gms.nearby","com.google.android.gms.nearby.connection","com.google.android.gms.nearby.messages","com.google.android.gms.nearby.messages.audio","com.google.android.gms.oss.licenses","com.google.android.gms.panorama","com.google.android.gms.location.places","com.google.android.gms.location.places.ui","com.google.android.gms.location.places","com.google.android.gms.plus","com.google.android.gms.plus.model.people","com.google.android.gms.safetynet","com.google.android.vending.verifier","com.google.android.gms.stats","com.google.android.gms.tagmanager","com.google.android.gms.tagmanager","com.google.android.gms.tasks","com.google.android.gms.vision.barcode","com.google.android.gms.vision.face","com.google.android.gms.vision.text","com.google.android.gms.vision","com.google.android.gms.wallet","com.google.android.gms.wallet.fragment","com.google.android.gms.wallet.wobs","com.google.android.gms.wearable","com.google.firebase","com.google.firebase.auth","com.google.firebase.provider","com.google.firebase.auth","com.google.firebase.crash","com.google.firebase.database","com.google.firebase.dynamiclinks","com.google.firebase","com.google.firebase.firestore","com.google.firebase.iid","com.google.firebase.messaging","com.google.firebase.ml.common","com.google.firebase.ml.custom","com.google.firebase.ml.custom.model","com.google.firebase.ml.vision","com.google.firebase.ml.vision.barcode","com.google.firebase.ml.vision.cloud","com.google.firebase.ml.vision.cloud.label","com.google.firebase.ml.vision.cloud.landmark","com.google.firebase.ml.vision.common","com.google.firebase.ml.vision.document","com.google.firebase.ml.vision.face","com.google.firebase.ml.vision.label","com.google.firebase.ml.vision.text","com.google.firebase.perf","com.google.firebase.perf.metrics","com.google.firebase.storage","com.google.firebase.remoteconfig","dalvik.annotation","dalvik.bytecode","dalvik.system","java.awt.font","java.beans","java.io","java.lang","java.lang.annotation","java.lang.invoke","java.lang.ref","java.lang.reflect","java.math","java.net","java.nio","java.nio.channels","java.nio.channels.spi","java.nio.charset","java.nio.charset.spi","java.nio.file","java.nio.file.attribute","java.nio.file.spi","java.security","java.security.acl","java.security.cert","java.security.interfaces","java.security.spec","java.sql","java.text","java.time","java.time.chrono","java.time.format","java.time.temporal","java.time.zone","java.util","java.util.concurrent","java.util.concurrent.atomic","java.util.concurrent.locks","java.util.function","java.util.jar","java.util.logging","java.util.prefs","java.util.regex","java.util.stream","java.util.zip","javax.crypto","javax.crypto.interfaces","javax.crypto.spec","javax.microedition.khronos.egl","javax.microedition.khronos.opengles","javax.net","javax.net.ssl","javax.security.auth","javax.security.auth.callback","javax.security.auth.login","javax.security.auth.x500","javax.security.cert","javax.sql","javax.xml","javax.xml.datatype","javax.xml.namespace","javax.xml.parsers","javax.xml.transform","javax.xml.transform.dom","javax.xml.transform.sax","javax.xml.transform.stream","javax.xml.validation","javax.xml.xpath","junit.framework","junit.runner","org.apache.http.conn","org.apache.http.conn.scheme","org.apache.http.conn.ssl","org.apache.http.params","org.json","org.w3c.dom","org.w3c.dom.ls","org.xml.sax","org.xml.sax.ext","org.xml.sax.helpers","org.xmlpull.v1","org.xmlpull.v1.sax2"]
    
    def __init__ (self, random_state=2020, run_id="default", data_path = '', working_path="./"):
        """
        
        Parameters
        ----------
        run_id : String
            String with run id to store results uniquely
        data_path : String
            Path to the directory containing the apk samples (dataset)
        working_path : String
            Path where files are stored

        Returns
        -------
        None.



        """

        if not working_path.endswith('/'):
            working_path += '/'
        if data_path == '':
            raise Exception('No data path provided')
        self._data_path = data_path
        self._working_path = working_path + "MaMaDroid_Data/"
        if not os.path.exists(self._working_path):
            os.makedirs(self._working_path)
        self._random_state = random_state
        self._encoder = FeatureEncoder()
        self._model_file = self._working_path + run_id + '_model.pkl'
        print('Trying to load:', self._model_file)
        # Load model if exists
        try:
            with open(self._model_file, 'rb') as f:
                mod = pickle.load(f)
                self._clf = mod[0]
                self._encoder = mod[1]
        except FileNotFoundError:
            pass
    
    
    def _abstractSequences(self, sequences):
        abstracted = {}
        for seq in sequences:
            new_seq = []
            for call in seq:
                splitcall = (call[1:].split(";"))[0].split('/')  # (Remove 'L' from init and split until ;) then split over the calls identifier [0]
                match = 0
                for n in range(len(splitcall)-1,-1,-1):
                    pack = ".".join(splitcall[0:n])
                    if pack in self.packages:
                        new_seq.append(pack)
                        match = 1
                        break
                    elif call=='ROOT':
                        new_seq.append(call)
                        match = 1
                        break
                if match == 0:  # Own method
                    obfuscated = 0
                    for split in splitcall:
                        if len(split)<3:
                            obfuscated += 1
                    if obfuscated >= len(splitcall)/2:
                        new_seq.append("obfuscated")
                    else:
                        new_seq.append("selfdefined")
            # Avoid repeated edges
            try:
                abstracted[tuple(new_seq)] += 1
            except:
                abstracted[tuple(new_seq)] = 1
        return abstracted
    
    
    def _abstractSequencesToClasses(self, sequences):
        abstracted_edges = []
        nodes = []
        for seq in sequences:
            new_seq = []
            for call in seq:
                class_name = (call[1:].split(";"))[0]  # (Remove 'L' from init and split until ;) then get first element to get class name
                if len(class_name)<(3*class_name.count('/')):  # If less than 3 chars for package and class names, it is obfuscated
                    new_seq.append("obfuscated")
                else:
                    new_seq.append(class_name)
                    if class_name not in nodes:
                        nodes.append(class_name)
            # Avoid repeated edges
            abstracted_edges.append(tuple(new_seq))
        return abstracted_edges, nodes
                    
    
    
    def _writeAPKfeatures (self, apk_list, parent_out_dir ):
        analyzer = APKFeatureExtractor()
        for idx in range(len(apk_list)):
            path = apk_list[idx]
            path = path.replace(self._data_path, '').replace('../', '')
            path = path.replace('/', '_')
            path = path[:-4]
            write_path = parent_out_dir + path + ".pkl"
            try:
                analyzer.processAPK(apk_list[idx])
                CG = analyzer.getCallGraph()
                data = self._abstractSequences(CG.edges())
            except:
                traceback.print_exc()
                print("Corrupted file: ", apk_list[idx])
                #os.remove(apk_list[idx])
                continue  # Omit that corrupted apk
            
            with open(write_path, 'wb') as f:
                pickle.dump(data, f)
    
    
    # Remove already parsed files from parsing
    def _filterInputList (self, apk_list, parent_out_dir):
        new_list = []
        for idx in range(len(apk_list)):
            path = apk_list[idx]
            path = path.replace(self._data_path, '').replace('../', '')
            path = path.replace('/', '_')
            path = path[:-4]
            write_path = parent_out_dir + path + ".pkl"
            if os.path.isfile(write_path): continue
            new_list.append(apk_list[idx])
        return new_list
    
    
    def parseAPKs (self, njobs = -1, overwrite=True):
        """
        This method extract features from every apk found inside path and 
        stores the results in the working_path 
        

        Parameters
        ----------
            njobs : integer
                -1 use all cpus
                1 use only one thread
            overwrite : boolean
                True to overwrite existing files

        Returns
        -------
        None.

        """
        t1 = time.time()

        files = glob.glob(self._data_path+'/**/*.apk', recursive=True)
        
        n_files = len(files)
        print("Found", n_files, " in directory")
        
        if not overwrite:
            files = self._filterInputList(files, self._working_path)
            n_files = len(files)
            print("Remaining", n_files)
        
        # Parallellized        
        processes = []
        if njobs<0:
            nproc = mp.cpu_count()-2
            part_size = n_files//nproc
        else:
            nproc = njobs
            part_size = n_files//nproc
        
        init_idx = 0
        fin_idx = 0
        for pn in range(nproc-1):
            init_idx = pn*part_size
            fin_idx = (pn+1)*part_size
            processes.append(mp.Process(target=self._writeAPKfeatures, args=(files[init_idx:fin_idx], self._working_path) ))
            processes[pn].start()
            
        # This process also works! Do the rest
        self._writeAPKfeatures(files[fin_idx:], self._working_path)
        
        # Wait for the rest
        for proc in processes:
            proc.join()
        
        t2 = time.time()
        print("Finished in: ", t2-t1, ", avg time: ", (t2-t1)/(1+n_files), "s/apk" )    
    
    
    # Compute the MC transition matrix based on the edges found
    def _transitionMatrix (self, abstracted_edges):
        node_dict = self._encoder.getDictionary()
        size_vectors = len(node_dict.keys())
        T = np.zeros((size_vectors, size_vectors))
        #print(abstracted_edges.items())
        for key,num_edges in abstracted_edges.items():
            from_node = key[0]
            to_node = key[1]
            # Calculate position in feature vector
            from_pos = node_dict[from_node]
            to_pos = node_dict[to_node]
            # Update position
            if type(num_edges)!=int: print(type(num_edges), num_edges)
            T[from_pos,to_pos] += num_edges
        # Compute transition probabilities
        totals = np.sum(T,axis=1)
        totals[totals==0] = 1  # Avoid division by 0
        T = ((1/totals)*T.T).T
        return T.reshape((-1))
    
    
    def _computeFeatureSet (self, APKs_train, Y_train):
        init = time.time()
        
        self._encoder = FeatureEncoder()
        self._encoder.addList(self.packages + ["obfuscated","selfdefined"])
        
        node_dict = self._encoder.getDictionary()
        
        n_features = self._encoder.getLength()**2
        
        n_apks = len(APKs_train)
        
        X_train = np.zeros((n_apks, n_features))
        print(n_features)
        i = 0
        while i<n_apks:
            path = APKs_train[i]
            path = path.replace(self._data_path, '').replace('../', '')
            path = path.replace('/', '_')
            path = path[:-4]
            filename = self._working_path + path + ".pkl"
            with open(filename, 'rb') as f:
                data = pickle.load(f)
            
            # Compute transition counts
            X_train[i] = self._transitionMatrix(data)  # Compute transition probabilities of that apk
            i += 1        
        
        self._column_names = [element for element in itertools.product(list(node_dict.keys()),list(node_dict.keys()))]
        self._column_names.append("Class")
        # Store data into csv files
        
        fin = time.time()
        print("Processed: ", X_train.shape," in ", fin-init, " seconds")
        return X_train, Y_train
        
    
    def _extractFeaturesFor ( self, APKs, Y=None):
        # Get testing features and Encode testing set
        node_dict = self._encoder.getDictionary()
        size_vectors = self._encoder.getLength()
        
        n_features = self._encoder.getLength()**2
        n_apks = len(APKs)
        
        X = np.zeros((n_apks, n_features))
        
        i = 0
        while i<n_apks:
            path = APKs[i]
            path = path.replace(self._data_path, '').replace('../', '')
            path = path.replace('/', '_')
            path = path[:-4]
            filename = self._working_path + path + ".pkl"
            with open(filename, 'rb') as f:
                data = pickle.load(f)
            
            X[i] = self._transitionMatrix(data)  # Compute transition probabilities of that apk
            i += 1        
        
        if type(Y)==np.ndarray:
            return X, Y
        return X
    
    
    def _GridSearchCV (self, X, Y, estimator, param_grid, kf_cv, scorer):
        results = []
        print("Finding best hiperparameters for the model")
        X = np.array(X)
        num_cvs = kf_cv.get_n_splits()
        for params in list(ParameterGrid(param_grid)):
            t1 = time.time()
            n_features = 0
            score = 0
            for train_index, test_index in kf_cv.split(X, Y):
                # Fit to k-1 folds
                X_train, Y_train = self._computeFeatureSet(X[train_index], Y[train_index])
                n_features += X_train.shape[1]
                model = estimator(**params)
                model.fit(X_train, Y_train)
                # Predict k fold
                X_test, Y_test = self._extractFeaturesFor(X[test_index], Y[test_index])
                Y_pred = model.predict(X_test)
                score += scorer(Y_test, Y_pred)
            results.append((params,score/num_cvs))
            t2 = time.time()
            print("Grid Search iteration", t2-t1, "seconds with", n_features/num_cvs, "mean features" )
        # Sort restuls and return the best
        results = sorted(results, key=lambda item: item[1], reverse=True)
        return results[0]
    
    
    
    def fit (self, X, Y, kfold_splits=5, n_repeats=1, time_dependent_folds=False):
        """
        Computes feature set of X and trains the Random Forest classifier. Returns the validation metrics.

        Parameters
        ----------
        X : List of Strings 
            List containing the apk names used for training
        Y : np.array
            Array containing the class labels of the apks in X
        kfold_splits : int, optional
            Number of cross validation splits used in training. The default is 5.
        n_repeats : int, optional
            Number of repetitions of cross validation in training. The default is 1.
        time_dependent_folds : bool, optional
            Use time-dependent-aware k-folds (sklearn TimeSeriesSplit). If True, n_repeats is
            omited. The default is False.

        Returns
        -------
        (TPR, FPR, precision, F1, AUC, Kappa)
            TPR: average of the True Possitive Rate of the classifier in the cross-validation set
            FPR: average of the False Possitive Rate of the classifier in the cross-validation set
            precision: ratio of correct positive predictions
            F1: average of the F1 score of the classifier in the cross-validation set
            AUC: average Area Under the ROC Curve of the classifier in the cross-validation set
            Kappa: Cohen Kappa

        """
        if time_dependent_folds:
            kf = TimeSeriesSplit(max_train_size=None, n_splits=kfold_splits)
        else:
            kf = RepeatedStratifiedKFold(n_splits = kfold_splits, n_repeats=n_repeats, random_state=self._random_state)
        
        param_grid = {'n_estimators' : list(range(100,301,100)),
                      'criterion': ['gini', 'entropy'],
                      'min_samples_split': list(range(2,11,3)),
                      'min_samples_leaf': list(range(1,11,3)),
                      'max_features': [.1, .25, .5],
                      'max_samples': [.25, .5, .75, 1] }
        
        kappa_scorer = cohen_kappa_score
        
        rf = RandomForestClassifier
        
        best = self._GridSearchCV(X, Y, rf, param_grid, kf_cv=kf, scorer=kappa_scorer)
        
        self._best_params = best[0]
        print('Selected', self._best_params, 'with CV score', best[1])
        
        X, Y = self._computeFeatureSet(X, Y)
        
        self._clf = RandomForestClassifier(**self._best_params)
        self._clf.fit(X, Y)
        
        y_pred = self._clf.predict(X)
        
        auc = metrics.roc_auc_score(Y, y_pred)
        f1 = metrics.f1_score(Y, y_pred)
        tn, fp, fn, tp = metrics.confusion_matrix(Y, y_pred).ravel()
        fpr = fp/(fp+tn)
        tpr = tp/(tp+fn)
        precision = tp/(tp+fp)
        kappa = cohen_kappa_score(Y, y_pred)
        
        print("On train set (tpr, fpr, precision, f1, auc, kappa):", tpr, fpr, precision, f1, auc, kappa )
        
        print("Saving model to ", self._model_file)
        with open(self._model_file, 'wb') as f:
            pickle.dump([self._clf, self._encoder], f)
        
        return tpr, fpr, precision, f1, auc, kappa
    
        
    
    def evaluate (self, X, Y):
        """
        Evaluates the Classifier and returns the metrics computed.

        Parameters
        ----------
        X : List of Strings
            List with the APK names in the testing set.
        Y : np.array
            Array containing the class labels of the apks in X

        Returns
        -------
        (TPR, FPR, precision, F1, AUC, Kappa)
            TPR: True Possitive Rate of the classifier
            FPR: False Possitive Rate of the classifier
            precision: ratio of correct positive predictions
            F1: F1 score of the classifier
            AUC: Area Under the ROC Curve of the classifier
            Kappa: Cohen Kappa of the classifier

        """
        X, Y = self._extractFeaturesFor(X, Y)
        
        y_pred = self._clf.predict(X)
        auc = metrics.roc_auc_score(Y, y_pred)
        f1 = metrics.f1_score(Y, y_pred)
        tn, fp, fn, tp = metrics.confusion_matrix(Y, y_pred).ravel()
        fpr = fp/(fp+tn)
        tpr = tp/(tp+fn)
        precision = tp/(tp+fp)
        kappa = cohen_kappa_score(Y, y_pred)
        print("On test set (tpr, fpr, precision, f1, auc, kappa):", tpr, fpr, precision, f1, auc, kappa )
        return tpr, fpr, precision, f1, auc, kappa
    
    
    def predict (self, APKs):
        """
        Gets the prediction of a list of APKs

        Parameters
        ----------
        APKs : List of Strings
            List with the APK names

        Returns
        -------
        y_pred : np.array
            Predictions returned by the classifier

        """
        to_be_parsed = self._filterInputList(APKs, self._working_path)
        if len(to_be_parsed)>0:
            self._writeAPKfeatures(to_be_parsed, self._working_path)
        X = self._extractFeaturesFor(APKs)
        y_pred = self._clf.predict(X)
        return y_pred
        
