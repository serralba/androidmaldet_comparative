
# Towards a Fair Comparison and Realistic Evaluation Framework of Android Malware Detectors based on Static Analysis and Machine Learning


## Description
Project with the code and dataset used for the paper entitled "Towards a Fair Comparison and Realistic Evaluation Framework of Android Malware Detectors based on Static Analysis and Machine Learning". The structure of this project is as follows:

* [dataset](../dataset)


This directory contains a file with the SHA hashes of the APKs that comprise each of the three classes (goodware, malware and greyware). All these APKs were originally downloaded from [AndroZoo](https://androzoo.uni.lu/). To download the APKs in our dataset, you can use the [AZ tool](https://github.com/ArtemKushnerov/az).

A class implementing each of the following methods (METHODNAME.py) is included:

* [AndroDialysis](https://www.sciencedirect.com/science/article/pii/S0167404816301602)
* [BasicBlocks](https://link.springer.com/article/10.1007/s10664-014-9352-6)
* [Drebin](https://www.ndss-symposium.org/ndss2014/programme/drebin-effective-and-explainable-detection-android-malware-your-pocket/)
* [DroidDet](https://www.sciencedirect.com/science/article/pii/S0925231217312870)
* [DroidDetector](https://ieeexplore.ieee.org/document/7399288)
* [HMMDetector](https://www.sciencedirect.com/science/article/pii/S0167404816300499)
* [ICCDetector](https://ieeexplore.ieee.org/document/7398053)
* [MaMaDroid](https://dl.acm.org/doi/10.1145/3313391)
* [PermPair](https://ieeexplore.ieee.org/document/8886364)




The project has some dependencies that you need to install before using the code. These are:

* numpy
* pomegranate
* networkx
* sklearn
* androguard
* rotation_forest
* lief
* python-magic


## Usage
For any method usage. 

Instantiate the method:

```python3
from Drebin import Drebin
dre = Drebin(run_id = YOUR_RUN_ID, data_path = DATASET_PATH, working_path= OUTPUT_DIR )
```
Statically analyze APKs and store their components (static analysis features) in files. A file (.pkl extension) is created inside the working path for each APK:

```python3
dre.parseAPKs()
```
Once APKs in DATASET\_PATH have been parsed, you can fit a model. To do so, pass the list of training APKs (files with .apk extension as in DATASET_PATH), and their labels, to the fit the model:

```python3
scores = dre.fit(X_train, Y_train)
```
The previous call returns the TPR, FPR, precision, F1 score, AUC and kappa metrics of the fitted model with the training set. The model (Drebin_YOUR_RUN_ID_model.pkl) is by default saved in the OUTPUT\_DIR:

You can evaluate the model passing a list of APK files (as in DATASET_PATH) and their labels to the evaluate method:

```python3
scores = dre.evaluate(X_test, Y_test)
```

The previous call returns the TPR, FPR, precision, F1 score, AUC and kappa metrics of the predictions made for the test set by the fitted model.

You can also predict new samples with:

```python3
preds = dre.predict(APKs)
```

## Authors and acknowledgment
Please cite:

```
@article{molinacoronado2022towards,
    title = {Towards a Fair Comparison and Realistic Evaluation Framework of Android Malware Detectors based on Static Analysis and Machine Learning},
    author = {Borja Molina-Coronado and Usue Mori and Alexander Mendiburu and Jose Miguel-Alonso},
    journal = {Computers & Security},
    pages = {102996},
    year = {2022},
    issn = {0167-4048},
    doi = {https://doi.org/10.1016/j.cose.2022.102996},
    url = {https://www.sciencedirect.com/science/article/pii/S0167404822003881}
}
```

## License
This work is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License. For more information check the link below:

http://creativecommons.org/licenses/by-nc/4.0/


## Contributing
Feel free to make any pull request


