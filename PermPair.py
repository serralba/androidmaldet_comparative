#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 11:22:15 2020

@author: Borja Molina Coronado
@email: borja.molina@ehu.eus
"""

from analysis.APKFeatureExtractor import APKFeatureExtractor
import numpy as np
from sklearn import metrics
import multiprocessing as mp
import math
import networkx as nx
import traceback
import glob
import pickle
import time
import os


class PermPair:
    """
    Class implementing the method by Arora et al. in 
    "PermPair: Android Malware Detection using Permission Pairs"
    
    APK Information:
        Manifest -> permissions
        
    Features:
        2 graphs being each node a permission and edges the presence of two permissions
        in the same app (the weight of the edge is the frequency of apparition):
            
            Gm : graph with permissions of malware apps
            Gn : graph with permissions of normal apps
            
    Model:
        
        Malware if the sum of weights of the presence of the permissions of an 
        application in Gm is higher than the weight using Gn. Else is normal
        
        
        The graphs are prunned to remove edges which do not affect the TPR nor TNR.
        
        To do so an iterative process adds into a list of "candidates for removal"
        those edges that do not vary TPR nor TNR when removed.
        With every edge, its the difference of its weight in Gm in Gn is added.
        
        Beta = sum(all the weights of candidates)
        
        Then, a subset of edges is tested to be removed. To do so:
            
            - Two thresholds are set a1=0 and a2=Beta
            - edges whose weight is less than a2 are removed.
            
        If TNR and TPR decrease. a2 = a2/2.
        Else a1=a2 and a2=a2*2.
        
        If a1==a2: end. 
        Else repeat and return prunned graphs
        
        
    """
    

    _working_path = None
    _data_path = None

    _column_names = None
    
    _Gn = None
    _Gm = None

    _model_file = None
    
    
    def __init__ (self, random_state=2020, run_id="default", data_path = '', working_path="./"):
        """
        

        Parameters
        ----------
        run_id : String
            String with run id to store results uniquely
        data_path : String
            Path to the directory containing the apk samples (dataset)
        working_path : String
            Path where files are stored

        Returns
        -------
        None.


        """
        
        if not working_path.endswith('/'):
            working_path += '/'
        if data_path == '':
            raise Exception('No data path provided')
        self._data_path = data_path
        self._working_path = working_path + "PermPair_Data/"
        if not os.path.exists(self._working_path):
            os.makedirs(self._working_path)
        self._random_state = random_state
        self._model_file = self._working_path + run_id + '_model.pkl'
        # Load model if exists
        try:
            with open(self._model_file, 'rb') as f:
                mod = pickle.load(f)
                self._Gn = mod[0]
                self._Gm = mod[1]
        except FileNotFoundError:
            pass
        
        
    def _writeAPKfeatures (self, apk_list, parent_out_dir ):
        analyzer = APKFeatureExtractor()
        for idx in range(len(apk_list)):
            path = apk_list[idx]
            path = path.replace(self._data_path, '').replace('../', '')
            path = path.replace('/', '_')
            path = path[:-4]
            write_path = parent_out_dir + path + ".pkl"
            try:
                analyzer.processAPK(apk_list[idx])
                perms = analyzer.getDeclaredPermissions()
                data = np.array(perms)
            except:
                traceback.print_exc()
                print("Corrupted file: ", apk_list[idx])
                #os.remove(apk_list[idx])
                continue  # Omit that corrupted apk
            
            with open(write_path, 'wb') as f:
                pickle.dump(data, f)
    
    
    # Remove already parsed files from parsing
    def _filterInputList (self, apk_list, parent_out_dir):
        new_list = []
        for idx in range(len(apk_list)):
            path = apk_list[idx]
            path = path.replace(self._data_path, '').replace('../', '')
            path = path.replace('/', '_')
            path = path[:-4]
            write_path = parent_out_dir + path + ".pkl"
            if os.path.isfile(write_path): continue
            new_list.append(apk_list[idx])
        return new_list
    
    
    def parseAPKs (self, njobs = -1, overwrite=True):
        """
        This method extract features from every apk found inside path and 
        stores the results in the working_path 
        

        Parameters
        ----------
            njobs : integer
                -1 use all cpus
                1 use only one thread
            overwrite : boolean
                True to overwrite existing files

        Returns
        -------
        None.

        """
        t1 = time.time()
        
        files = glob.glob(self._data_path+'/**/*.apk', recursive=True)
        n_files = len(files)
        print("Found", n_files, " in directory")
        
        if not overwrite:
            files = self._filterInputList(files, self._working_path)
            n_files = len(files)
            print("Remaining", n_files)
        
        # Parallellized        
        processes = []
        if njobs<0:
            nproc = mp.cpu_count()-2
            part_size = n_files//nproc
        else:
            nproc = njobs
            part_size = n_files//nproc
        
        init_idx = 0
        fin_idx = 0
        for pn in range(nproc-1):
            init_idx = pn*part_size
            fin_idx = (pn+1)*part_size
            processes.append(mp.Process(target=self._writeAPKfeatures, args=(files[init_idx:fin_idx], self._working_path) ))
            processes[pn].start()
            
        # This process also works! Do the rest
        self._writeAPKfeatures(files[fin_idx:], self._working_path)
        
        # Wait for the rest
        for proc in processes:
            proc.join()
        
        t2 = time.time()
        print("Finished in: ", t2-t1, ", avg time: ", (t2-t1)/(1+n_files), "s/apk" )


    def _computeFeatureSet (self, APKs_train, Y_train):
        
        # Read training apks to get the global feature set
        n_files = len(APKs_train)
        n = 0
               
        D = []
        while n<n_files:
            path = APKs_train[n]
            path = path.replace(self._data_path, '').replace('../', '')
            path = path.replace('/', '_')
            path = path[:-4]
            name = self._working_path + path + ".pkl"
            with open(name, 'rb') as f:
                data = pickle.load(f)
            
            # Create matrix of app permissions
            
            D.append([data,Y_train[n]])
            
            n += 1
        return D
            
        
    def _predict (self, Gn, Gm, D, omit_edges = []):
        Ypred = np.zeros(len(D), dtype=np.int8)
        n = 0
        for d in D:
            data = d[0]
            normal_score = 0
            malicious_score = 0
            if len(data)>1:
                for t in range(len(data)-1):
                    for t1 in range(t+1,len(data)):
                        if (data[t], data[t1]) in omit_edges or (data[t1],data[t]) in omit_edges:  # Undirected
                            continue
                        if Gn.has_edge(data[t], data[t1]):
                            normal_score += Gn[data[t]][data[t1]]['weight']
                        if Gm.has_edge(data[t], data[t1]):
                            malicious_score += Gm[data[t]][data[t1]]['weight']
                if malicious_score>normal_score:
                    Ypred[n] = 1
                else:
                    Ypred[n] = 0
            n += 1
        return Ypred
    
    
    def _computeDetectionPerformance (self, Gn, Gm, D, Y, omit_edges = []):
        Ypred = self._predict(Gn, Gm, D, omit_edges)
        tn, fp, fn, tp = metrics.confusion_matrix(Y, Ypred).ravel()
        return (tn/(tn+fp), tp/(tp+fn))
    
    
    def _computeGraphs (self, D):
        Gm = nx.Graph()
        nm = 0
        Gn = nx.Graph()
        nn = 0
        for d in D:  # For each sample
            data = d[0]  # Get perms
            Y = d[1]  # Label
            if len(data)>1:
                if Y==1:
                    G = Gm
                    nm += 1
                else:
                    G = Gn
                    nn += 1
                
                for t in range(len(data)-1):  # Get perm pairs
                    if t == 0 and data[t] not in G:
                        G.add_node(data[t])
                    for t1 in range(t+1,len(data)):
                        if data[t1] not in G:
                            G.add_node(data[t1])
                        if G.has_edge(data[t], data[t1]):
                            G[data[t]][data[t1]]['weight'] += 1
                        else:
                            G.add_edge(data[t], data[t1], weight= 1)
                            
        for (u,v,d) in Gm.edges(data='weight'):
            if d>0:
                Gm[u][v]['weight'] = d/nm
        
        for (u,v,d) in Gn.edges(data='weight'):
            if d>0:
                Gn[u][v]['weight'] = d/nn
                
        return Gn, Gm
    
    def _reweight_M_graph (self, Gm, Gn):
        for (u,v,wm) in Gm.edges(data='weight'):
            if Gn.has_edge(u, v):
                wn = Gn[u][v]['weight']
                a1 = wm/(wn+wm)
                a2 = 1 - a1
                
                w = a1*(wm-wn)*wm+a2*(wn-wm)*wn
                Gm[u][v]['weight'] = w
        return Gm
    
    
    def _reduceGraphs (self, Gn, Gm, D, Y):
        TNR, TPR = self._computeDetectionPerformance(Gn, Gm, D, Y)
        edges = list(set(list(Gm.edges)+list(Gn.edges)))
        
        candidates = []
        for edge in edges:
            sTNR, sTPR = self._computeDetectionPerformance(Gn, Gm, D, Y, edge)
            if sTNR<TNR or sTPR<TPR:  # If performance does not decrease, cadidates for removal
                mweight = 0
                nweight = 0
                try:
                    mweight = Gm[edge[0]][edge[1]]['weight']
                except KeyError:
                    pass
                try:
                    nweight = Gn[edge[0]][edge[1]]['weight']
                except KeyError:
                    pass
                candidates.append((edge, (mweight - nweight)))
        
        if len(candidates)>0:
            candidates = sorted(candidates, key=lambda a: a[1])
            beta = math.fsum(np.array(candidates)[:,1])
            
            a1 = 0
            a2 = beta
            max_pos = len(candidates)-1
            # Test what to remove
            while a1!=a2:
                to_remove = candidates[0:max_pos,0]
                sTNR, sTPR = self._computeDetectionPerformance(Gn, Gm, D, Y, to_remove)
                if sTNR<TNR or sTPR<TPR:  # Performance decreases
                    a2 = a2/2
                else:
                    a1 = a2
                    a2 = a2*2
                    if a2>beta: a2=beta
                
                # Select new limit
                previous = 0
                new_max_pos = max_pos - 1
                while new_max_pos<len(candidates):
                    suma = math.fsum(candidates[0:new_max_pos,1])
                    if suma==a2:
                        break
                    if suma<a2:  # Select the appropriate max pos
                        new_max_pos += 1
                        if previous==-1:  # a +1 after a -1 is that we are near the sum
                            break
                        previous = 1
                    else:
                        new_max_pos -= 1
                        previous = -1
                max_pos = new_max_pos
            
            # Remove edges   
            Gn.remove_edges_from(candidates[:max_pos,0])
            Gm.remove_edges_from(candidates[:max_pos,0])
        
        return Gn, Gm
        
        
    
    
    def _extractFeaturesFor (self, APKs, Y=None):
        # Get testing permissions and Encode testing set
        n_apks = len(APKs)
        n = 0
        D = []
        while n<n_apks:
            path = APKs[n]
            path = path.replace(self._data_path, '').replace('../', '')
            path = path.replace('/', '_')
            path = path[:-4]
            name = self._working_path + path + ".pkl"
            with open(name, 'rb') as f:
                data = pickle.load(f)
            
            # Create matrix of app permissions
            D.append([data])
            
            n += 1
        
        if type(Y)==np.ndarray:
            return D, Y
        else:
            return D
    
    

    
    def fit (self, X, Y, kfold_splits=5, n_repeats=1, time_dependent_folds=False):
        """
        Computes feature set of X and trains the Random Forest classifier. Returns the validation metrics.

        Parameters
        ----------
        X : List of Strings 
            List containing the apk names used for training
        Y : np.array
            Array containing the class labels of the apks in X
        kfold_splits : int, optional
            Number of cross validation splits used in training. The default is 5.

        Returns
        -------
        (TPR, FPR, F1, AUC)
            TPR: average of the True Possitive Rate of the classifier in the cross-validation set
            FPR: average of the False Possitive Rate of the classifier in the cross-validation set
            F1: average of the F1 score of the classifier in the cross-validation set
            AUC: average Area Under the ROC Curve of the classifier in the cross-validation set
            
        """
        D = self._computeFeatureSet(X, Y)
        
        Gn, Gm = self._computeGraphs(D)
        
        Gm = self._reweight_M_graph(Gm, Gn)
        
        Gn, Gm = self._reduceGraphs(Gn, Gm, D, Y)
        
        self._Gn = Gn
        self._Gm = Gm
        
        y_pred = self._predict(Gn, Gm, D)
        
        print("Saving model to ", self._model_file)
        with open(self._model_file, 'wb') as f:
            pickle.dump([self._Gn, self._Gm], f)
        
        auc = metrics.roc_auc_score(Y, y_pred)
        f1 = metrics.f1_score(Y, y_pred)
        tn, fp, fn, tp = metrics.confusion_matrix(Y, y_pred).ravel()
        fpr = fp/(fp+tn)
        tpr = tp/(tp+fn)
        precision = tp/(tp+fp)
        kappa = metrics.cohen_kappa_score(Y, y_pred)
        print("On train set (tpr, fpr, precision, f1, auc, kappa):", tpr, fpr, precision, f1, auc, kappa )
        return tpr, fpr, precision, f1, auc, kappa
        
    
    def evaluate (self, X, Y):
        """
        Evaluates the Classifier and returns the metrics computed.

        Parameters
        ----------
        X : List of Strings
            List with the APK names in the testing set.
        Y : np.array
            Array containing the class labels of the apks in X

        Returns
        -------
        (TPR, FPR, F1, AUC)
            TPR: True Possitive Rate of the classifier
            FPR: False Possitive Rate of the classifier
            F1: F1 score of the classifier
            AUC: Area Under the ROC Curve of the classifier

        """
        X, Y = self._extractFeaturesFor(X, Y)
        y_pred = self._predict(self._Gn, self._Gm, X)
        auc = metrics.roc_auc_score(Y, y_pred)
        f1 = metrics.f1_score(Y, y_pred)
        tn, fp, fn, tp = metrics.confusion_matrix(Y, y_pred).ravel()
        fpr = fp/(fp+tn)
        tpr = tp/(tp+fn)
        precision = tp/(tp+fp)
        kappa = metrics.cohen_kappa_score(Y, y_pred)
        print("On test set (tpr, fpr, precision, f1, auc, kappa):", tpr, fpr, precision, f1, auc, kappa )
        return tpr, fpr, precision, f1, auc, kappa
    
    
    def predict (self, APKs):
        """
        

        Parameters
        ----------
        APKs : List of Strings
            List with the APK names

        Returns
        -------
        y_pred : np.array
            Predictions returned by the classifier

        """
        to_be_parsed = self._filterInputList(APKs, self._working_path)
        if len(to_be_parsed)>0:
            self._writeAPKfeatures(to_be_parsed, self._working_path)
        X = self._extractFeaturesFor(APKs)
        y_pred = self._predict(self._Gn, self._Gm, X)
        return y_pred
        